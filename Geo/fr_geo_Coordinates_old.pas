unit fr_geo_Coordinates_old;

interface

uses
  SysUtils,  StdCtrls, ExtCtrls, Variants,Classes, Controls, Forms, cxSpinEdit,
  Mask, cxControls, cxContainer, cxEdit, cxTextEdit,

  u_func,
  u_Geo,
  u_geo_convert, cxMaskEdit, rxToolEdit, rxCurrEdit;

type
  Tframe_geo_Coordinates = class(TForm)
    rg_CoordSystem: TRadioGroup;
    lb_Lat: TLabel;
    lb_Lon: TLabel;
    Panel1: TPanel;
    ed_B_h: TcxSpinEdit;
    ed_B_m: TcxSpinEdit;
    ed_B_s: TcxSpinEdit;
    ed_LAt: TcxSpinEdit;
    Panel4: TPanel;
    ed_L_h: TcxSpinEdit;
    ed_L_m: TcxSpinEdit;
    ed_L_s: TcxSpinEdit;
    ed_Lon: TcxSpinEdit;
    MaskEdit1: TMaskEdit;
    cxSpinEdit1: TcxSpinEdit;
    cb_East: TComboBox;
    cb_North: TComboBox;
    Panel2: TPanel;
    CurrencyEdit11: TCurrencyEdit;
    CurrencyEdit12: TCurrencyEdit;
    CurrencyEdit13: TCurrencyEdit;
    CurrencyEdit14: TCurrencyEdit;
    CurrencyEdit21: TCurrencyEdit;
    CurrencyEdit22: TCurrencyEdit;
    CurrencyEdit23: TCurrencyEdit;
    CurrencyEdit24: TCurrencyEdit;
    procedure FormCreate(Sender: TObject);
    procedure ed_BL_Change(Sender: TObject);
    procedure ed_B_hChange(Sender: TObject);
    procedure ed_B_hEditing(Sender: TObject; var CanEdit: Boolean);
    procedure ed_LAtEditing(Sender: TObject; var CanEdit: Boolean);
    procedure rg_CoordSystemClick(Sender: TObject);
    procedure ed_B_sPropertiesEditValueChanged(Sender: TObject);
    procedure ed_LAtPropertiesEditValueChanged(Sender: TObject);
  private
    FBLPoint_Pulkovo: TBLPoint;
    FDisplayBLPoint: TBLPoint;

    FDisplayCoordSys: integer;

    FIsEdited: Boolean;
    FOnChange: TnotifyEvent;

    procedure BLPointToEdit(aIndex: integer);
    procedure EditToBLPoint(aIndex: Integer);
    function GetBLPoint_pulkovo: TBLPoint;
    function GetDisplayCoordSys: Integer;
    procedure SetDisplayCoordSys(aValue: Integer);
    procedure UpdateDisplayBLPoint;
  private
    procedure SetBLPoint_pulkovo(const Value: TBLPoint);

  public
    property OnChange: TnotifyEvent read FOnChange write FOnChange;

    /// read: Pulkovo
    property BLPoint_Pulkovo: TBLPoint read GetBLPoint_pulkovo write SetBLPoint_pulkovo;

    property DisplayCoordSys: Integer read GetDisplayCoordSys write SetDisplayCoordSys;

//EK_KRASOVSKY42, EK_WGS84);

  end;

const
  DEF_frame_geo_Coordinates_HEIGHT = 90;




implementation
{$R *.dfm}


//--------------------------------------------------------------
procedure Tframe_geo_Coordinates.FormCreate(Sender: TObject);
//--------------------------------------------------------------
begin
//  ed_Lon.ValueType:=vtFloat;
 // ed_Lat.ValueType:=vtFloat;

  lb_Lat.Caption := '������';
  lb_Lon.Caption := '�������';


  rg_CoordSystem.Items.Clear;
  rg_CoordSystem.Items.AddObject(DEF_GEO_STR_CK42,  Pointer(EK_CK_42));
  rg_CoordSystem.Items.AddObject(DEF_GEO_STR_CK95,  Pointer(EK_CK_95));
  rg_CoordSystem.Items.AddObject(DEF_GEO_STR_WGS84, Pointer(EK_WGS84));

  rg_CoordSystem.OnClick := nil;
  rg_CoordSystem.ItemIndex :=0;
  rg_CoordSystem.OnClick := rg_CoordSystemClick;


  if Owner is TControl then
//    TControl(Owner).Height:=115;
    TControl(Owner).Height:=85;  //85
end;

//----------------------------------------------
procedure Tframe_geo_Coordinates.BLPointToEdit(aIndex: integer);
//----------------------------------------------
var rAngle1,rAngle2: TAngle;
  d: integer;
begin
  FIsEdited := True;

  case aIndex of
    1: begin
        rAngle1:=geo_DecodeDegree (FDisplayBLPoint.B);
        rAngle2:=geo_DecodeDegree (FDisplayBLPoint.L);

        d:=Trunc((rAngle1.sec - Trunc(rAngle1.sec)) * 100);

{
        MaskEdit1.Text:= Format('%3d� %d'' %d,%d"',
              [rAngle1.deg, rAngle1.min, Trunc(rAngle1.sec), d ]);

}

        ed_B_h.Value:=rAngle1.deg;
        ed_B_m.Value:=rAngle1.min;
        ed_B_s.Value:=TruncFloat (Round(rAngle1.sec*100000000)/100000000, 2);

        ed_L_h.Value:=rAngle2.deg;
        ed_L_m.Value:=rAngle2.min;
        ed_L_s.Value:=TruncFloat (Round(rAngle2.sec*100000000)/100000000, 2)
       end;

    2: begin
        ed_Lat.Value:=FDisplayBLPoint.B;
        ed_Lon.Value:=FDisplayBLPoint.L;
       end;
  end;

  FIsEdited := False;
end;


//----------------------------------------------
procedure Tframe_geo_Coordinates.EditToBLPoint(aIndex: Integer);
//----------------------------------------------
var rAngle1,rAngle2: TAngle;
  iCoordSys: integer;
begin
  case aIndex of
    1: begin
         rAngle1.deg :=Round(ed_B_h.Value);
         rAngle1.min :=Round(ed_B_m.Value);
         rAngle1.sec :=ed_B_s.Value;

         rAngle2.deg :=Round(ed_L_h.Value);
         rAngle2.min :=Round(ed_L_m.Value);
         rAngle2.sec :=ed_L_s.Value;

         FDisplayBLPoint.B := geo_EncodeDegreeRec (rAngle1);
         FDisplayBLPoint.L := geo_EncodeDegreeRec (rAngle2);
       end;
    2: begin
         FDisplayBLPoint.B := ed_Lat.Value;
         FDisplayBLPoint.L := ed_Lon.Value;
       end;
  end;

  //------------------------------------------------------
  iCoordSys :=GetDisplayCoordSys;

  case iCoordSys of
    EK_CK_42  : FBLPoint_Pulkovo := FDisplayBLPoint;
   // EK_CK_95  : FBLPoint_Pulkovo := FDisplayBLPoint;
    EK_CK_95  : FBLPoint_Pulkovo := geo_BL_to_BL_95(FDisplayBLPoint, EK_CK_95, EK_CK_42);
    EK_WGS84  : FBLPoint_Pulkovo := geo_BL_to_BL(FDisplayBLPoint, EK_WGS84, EK_CK_42);
  end;
end;


procedure Tframe_geo_Coordinates.ed_BL_Change(Sender: TObject);
begin
  if FIsEdited then
    Exit;

  EditToBLPoint(2);
  BLPointToEdit(1);

  if Assigned(FOnChange) then
    FOnChange(Self);
end;


procedure Tframe_geo_Coordinates.ed_B_hChange(Sender: TObject);
begin
{ if FIsEdited then
    Exit;

  EditToBLPoint(1);
  BLPointToEdit(2);

  if Assigned(FOnChange) then
    FOnChange(Self);
}
end;

procedure Tframe_geo_Coordinates.ed_B_hEditing(Sender: TObject; var CanEdit:
    Boolean);
begin
{ if FIsEdited then
    Exit;

  EditToBLPoint(1);
  BLPointToEdit(2);

  if Assigned(FOnChange) then
    FOnChange(Self);
}
end;

procedure Tframe_geo_Coordinates.ed_LAtEditing(Sender: TObject; var CanEdit:  Boolean);
begin
 if FIsEdited then
    Exit;

  EditToBLPoint(2);
  BLPointToEdit(1);

  if Assigned(FOnChange) then
    FOnChange(Self);
end;


function Tframe_geo_Coordinates.GetBLPoint_pulkovo: TBLPoint;
begin
  //��������� ��������� �����
 // EditToBLPoint(1);
  Result:=FBLPoint_Pulkovo;

{  case rg_CoordSystem.ItemIndex of
    0: Result := FDisplayBLPoint;
    1: Result := geo_BL_to_BL(FDisplayBLPoint, EK_WGS84, EK_KRASOVSKY42);
  end;
}
end;


// ---------------------------------------------------------------
function Tframe_geo_Coordinates.GetDisplayCoordSys: Integer;
// ---------------------------------------------------------------
begin
 // Result := IIF(rg_CoordSystem.ItemIndex=0, EK_KRASOVSKY42, EK_WGS84);

//  ifv Result then
  if rg_CoordSystem.ItemIndex>=0 then
    Result := Integer( rg_CoordSystem.Items.Objects[rg_CoordSystem.ItemIndex])
  else
  //  Result :=-1;

    raise Exception.Create('function Tframe_geo_Coordinates.GetDisplayCoordSys: Integer;');


(*  ind := rg_CoordSystem.Items.IndexOfObject(Pointer(aCoordSys));
  if ind>=0 then
    rg_CoordSystem.ItemIndex :=ind;
*)
end;


procedure Tframe_geo_Coordinates.rg_CoordSystemClick(Sender: TObject);
begin
  UpdateDisplayBLPoint();
end;


procedure Tframe_geo_Coordinates.SetBLPoint_pulkovo(const Value: TBLPoint);
begin
  FBLPoint_Pulkovo:=Value;
  UpdateDisplayBLPoint();
end;


procedure Tframe_geo_Coordinates.SetDisplayCoordSys(aValue: Integer);
var
  ind: integer;
begin
  FDisplayCoordSys:=aValue;
 // rg_CoordSystem.ItemIndex:=IIF(aValue=EK_KRASOVSKY42, 0, 1);

  ind := rg_CoordSystem.Items.IndexOfObject(Pointer(aValue));
  if ind>=0 then
    rg_CoordSystem.ItemIndex :=ind;
end;


procedure Tframe_geo_Coordinates.UpdateDisplayBLPoint;
var
  iCoordSys: integer;
begin
  iCoordSys :=GetDisplayCoordSys;

  case iCoordSys of
    EK_CK_42 : FDisplayBLPoint:=FBLPoint_Pulkovo;
    EK_CK_95 : FDisplayBLPoint:=geo_BL_to_BL_95(FBLPoint_Pulkovo, EK_CK_42, EK_CK_95);
    EK_WGS84 : FDisplayBLPoint:=geo_BL_to_BL(FBLPoint_Pulkovo, EK_CK_42, EK_WGS84);
//  else
//    raise Exception.Create('');
  end;
//  if FDisplayCoordSys=EK_KRASOVSKY42 then
  //  Result := geo_BL_to_BL(FBLPoint, EK_KRASOVSKY42, EK_WGS84);

  BLPointToEdit(1);
  BLPointToEdit(2);

  cb_North.ItemIndex := IIF(FDisplayBLPoint.B>0, 0,1 );
  cb_East.ItemIndex  := IIF(FDisplayBLPoint.L>0, 0,1 );

end;


procedure Tframe_geo_Coordinates.ed_B_sPropertiesEditValueChanged(
  Sender: TObject);
begin
 if FIsEdited then
    Exit;

  EditToBLPoint(1);
  BLPointToEdit(2);

  if Assigned(FOnChange) then
    FOnChange(Self);

end;

procedure Tframe_geo_Coordinates.ed_LAtPropertiesEditValueChanged(
  Sender: TObject);
begin
 if FIsEdited then
    Exit;

  EditToBLPoint(2);
  BLPointToEdit(1);

  if Assigned(FOnChange) then
    FOnChange(Self);


end;

end.


