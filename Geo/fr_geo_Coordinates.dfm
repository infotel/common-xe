object frame_geo_Coordinates: Tframe_geo_Coordinates
  Left = 821
  Top = 387
  BorderWidth = 1
  Caption = 'fr_geo_Coordinates'
  ClientHeight = 198
  ClientWidth = 478
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PopupMenu = PopupMenu1
  Scaled = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object rg_CoordSystem: TRadioGroup
    Left = 363
    Top = 0
    Width = 77
    Height = 67
    ItemIndex = 0
    Items.Strings = (
      'CK42'
      'CK95'
      'WGS 84')
    PopupMenu = PopupMenu1
    TabOrder = 0
    OnClick = rg_CoordSystemClick
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 217
    Height = 68
    Hint = #1064#1080#1088#1086#1090#1072
    TabOrder = 1
    object lb_Lat: TLabel
      Left = 8
      Top = 19
      Width = 38
      Height = 13
      Caption = #1064#1080#1088#1086#1090#1072
    end
    object lb_Lon: TLabel
      Left = 8
      Top = 42
      Width = 43
      Height = 13
      Caption = #1044#1086#1083#1075#1086#1090#1072
    end
    object Panel1: TPanel
      Left = 64
      Top = 15
      Width = 151
      Height = 51
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object CurrencyEdit11: TCurrencyEdit
        Left = 0
        Top = 0
        Width = 30
        Height = 21
        Margins.Left = 1
        Margins.Top = 1
        Alignment = taLeftJustify
        DecimalPlaces = 0
        DisplayFormat = ',0'
        MaxValue = 90.000000000000000000
        MinValue = -90.000000000000000000
        TabOrder = 0
        Value = -78.000000000000000000
        OnChange = CurrencyEdit11Change
      end
      object CurrencyEdit12: TCurrencyEdit
        Left = 34
        Top = 0
        Width = 25
        Height = 21
        Hint = #1064#1080#1088#1086#1090#1072
        Margins.Left = 1
        Margins.Top = 1
        Alignment = taLeftJustify
        DecimalPlaces = 0
        DisplayFormat = ',0'
        MaxValue = 60.000000000000000000
        TabOrder = 1
        Value = 45.000000000000000000
        OnChange = CurrencyEdit11Change
      end
      object CurrencyEdit13: TCurrencyEdit
        Left = 62
        Top = 0
        Width = 40
        Height = 21
        Hint = #1064#1080#1088#1086#1090#1072
        Margins.Left = 1
        Margins.Top = 1
        Alignment = taLeftJustify
        DisplayFormat = ',0.00'
        MaxValue = 60.000000000000000000
        TabOrder = 2
        Value = 45.550000000000000000
        OnChange = CurrencyEdit11Change
      end
      object cb_North: TComboBox
        Left = 105
        Top = 0
        Width = 43
        Height = 21
        Hint = 'N-north'#13#10'S-sourth'
        Style = csDropDownList
        ItemIndex = 0
        TabOrder = 3
        Text = 'N'
        Items.Strings = (
          'N'
          'S')
      end
      object cb_East: TComboBox
        Left = 105
        Top = 24
        Width = 43
        Height = 21
        Hint = 'E - '#1074#1086#1089#1090#1086#1082#13#10'W - '#1079#1072#1087#1072#1076' '
        Style = csDropDownList
        ItemIndex = 0
        ParentShowHint = False
        ShowHint = True
        TabOrder = 7
        Text = 'E'
        Items.Strings = (
          'E'
          'W')
      end
      object CurrencyEdit23: TCurrencyEdit
        Left = 62
        Top = 24
        Width = 40
        Height = 21
        Hint = #1044#1086#1083#1075#1086#1090#1072
        Margins.Left = 1
        Margins.Top = 1
        Alignment = taLeftJustify
        DisplayFormat = ',0.00'
        MaxValue = 60.000000000000000000
        TabOrder = 6
        Value = 45.550000000000000000
        OnChange = CurrencyEdit11Change
      end
      object CurrencyEdit22: TCurrencyEdit
        Left = 34
        Top = 24
        Width = 25
        Height = 21
        Hint = #1044#1086#1083#1075#1086#1090#1072
        Margins.Left = 1
        Margins.Top = 1
        Alignment = taLeftJustify
        DisplayFormat = ',0'
        MaxValue = 60.000000000000000000
        TabOrder = 5
        Value = 45.000000000000000000
        OnChange = CurrencyEdit11Change
      end
      object CurrencyEdit21: TCurrencyEdit
        Left = 0
        Top = 24
        Width = 30
        Height = 21
        Hint = #1044#1086#1083#1075#1086#1090#1072
        Margins.Left = 1
        Margins.Top = 1
        Alignment = taLeftJustify
        DecimalPlaces = 0
        DisplayFormat = ',0'
        MaxValue = 180.000000000000000000
        MinValue = -180.000000000000000000
        TabOrder = 4
        Value = -78.000000000000000000
        OnChange = CurrencyEdit11Change
      end
    end
  end
  object GroupBox2: TGroupBox
    Left = 223
    Top = 0
    Width = 135
    Height = 67
    TabOrder = 2
    object CurrencyEdit24: TCurrencyEdit
      Left = 8
      Top = 40
      Width = 120
      Height = 21
      Margins.Left = 1
      Margins.Top = 1
      Alignment = taLeftJustify
      DecimalPlaces = 8
      DisplayFormat = ',0.000000000'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Value = 45.342342000000000000
      OnChange = CurrencyEdit14Change
    end
    object CurrencyEdit14: TCurrencyEdit
      Left = 8
      Top = 14
      Width = 120
      Height = 21
      Margins.Left = 1
      Margins.Top = 1
      Alignment = taLeftJustify
      DecimalPlaces = 8
      DisplayFormat = ',0.000000000'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Value = 45.342342000000000000
      OnChange = CurrencyEdit14Change
    end
  end
  object Edit1: TEdit
    Left = 32
    Top = 104
    Width = 121
    Height = 21
    TabOrder = 3
    Text = 'Edit1'
    Visible = False
  end
  object Edit2: TEdit
    Left = 32
    Top = 128
    Width = 121
    Height = 21
    TabOrder = 4
    Text = 'Edit1'
    Visible = False
  end
  object Button1: TButton
    Left = 184
    Top = 112
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 5
    Visible = False
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 280
    Top = 112
    Width = 75
    Height = 25
    Caption = 'Button2'
    TabOrder = 6
    Visible = False
    OnClick = Button2Click
  end
  object PopupMenu1: TPopupMenu
    Left = 400
    Top = 88
    object N11: TMenuItem
      Caption = '1'
      OnClick = N11Click
    end
    object N21: TMenuItem
      Caption = '2'
      OnClick = N21Click
    end
  end
end
