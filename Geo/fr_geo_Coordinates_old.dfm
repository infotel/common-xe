object frame_geo_Coordinates: Tframe_geo_Coordinates
  Left = 465
  Top = 315
  Width = 548
  Height = 303
  Caption = 'fr_geo_Coordinates'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lb_Lat: TLabel
    Left = 5
    Top = 7
    Width = 15
    Height = 13
    Caption = 'Lat'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object lb_Lon: TLabel
    Left = 5
    Top = 32
    Width = 18
    Height = 13
    Caption = 'Lon'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Panel4: TPanel
    Left = 59
    Top = 28
    Width = 278
    Height = 21
    BevelOuter = bvNone
    TabOrder = 1
    object ed_L_h: TcxSpinEdit
      Left = 0
      Top = 0
      Width = 47
      Height = 21
      Hint = #1044#1086#1083#1075#1086#1090#1072' - '#1043#1088#1072#1076#1091#1089
      ParentShowHint = False
      Properties.OnChange = ed_B_sPropertiesEditValueChanged
      ShowHint = True
      Style.BorderStyle = ebsFlat
      StyleFocused.BorderStyle = ebsFlat
      StyleHot.BorderStyle = ebsFlat
      TabOrder = 0
      OnEditing = ed_B_hEditing
    end
    object ed_L_m: TcxSpinEdit
      Left = 48
      Top = 0
      Width = 40
      Height = 21
      Hint = #1044#1086#1083#1075#1086#1090#1072' - '#1052#1080#1085#1091#1090#1072
      ParentShowHint = False
      Properties.OnChange = ed_B_sPropertiesEditValueChanged
      ShowHint = True
      Style.BorderStyle = ebsFlat
      StyleFocused.BorderStyle = ebsFlat
      StyleHot.BorderStyle = ebsFlat
      TabOrder = 1
      OnEditing = ed_B_hEditing
    end
    object ed_L_s: TcxSpinEdit
      Left = 89
      Top = 0
      Width = 55
      Height = 21
      Hint = #1044#1086#1083#1075#1086#1090#1072' - '#1057#1077#1082#1091#1085#1076#1072
      ParentShowHint = False
      Properties.ValueType = vtFloat
      Properties.OnChange = ed_B_sPropertiesEditValueChanged
      ShowHint = True
      Style.BorderStyle = ebsFlat
      StyleFocused.BorderStyle = ebsFlat
      StyleHot.BorderStyle = ebsFlat
      TabOrder = 2
      OnEditing = ed_B_hEditing
    end
    object ed_Lon: TcxSpinEdit
      Left = 148
      Top = 0
      Width = 122
      Height = 21
      Hint = #1044#1086#1083#1075#1086#1090#1072
      ParentShowHint = False
      Properties.ValueType = vtFloat
      Properties.OnChange = ed_LAtPropertiesEditValueChanged
      ShowHint = True
      Style.BorderStyle = ebsFlat
      StyleFocused.BorderStyle = ebsFlat
      StyleHot.BorderStyle = ebsFlat
      TabOrder = 3
      OnEditing = ed_LAtEditing
    end
  end
  object Panel1: TPanel
    Left = 59
    Top = 4
    Width = 278
    Height = 21
    BevelOuter = bvNone
    TabOrder = 0
    object ed_B_h: TcxSpinEdit
      Left = 0
      Top = 0
      Width = 47
      Height = 21
      Hint = #1064#1080#1088#1086#1090#1072' - '#1043#1088#1072#1076#1091#1089
      ParentShowHint = False
      Properties.OnChange = ed_B_sPropertiesEditValueChanged
      ShowHint = True
      Style.BorderStyle = ebsFlat
      Style.ButtonStyle = btsDefault
      Style.Shadow = False
      Style.TransparentBorder = True
      StyleFocused.BorderStyle = ebsFlat
      StyleFocused.ButtonStyle = btsDefault
      StyleHot.BorderStyle = ebsFlat
      TabOrder = 0
      OnEditing = ed_B_hEditing
    end
    object ed_B_m: TcxSpinEdit
      Left = 48
      Top = 0
      Width = 40
      Height = 21
      Hint = #1064#1080#1088#1086#1090#1072' - '#1052#1080#1085#1091#1090#1072
      ParentShowHint = False
      Properties.OnChange = ed_B_sPropertiesEditValueChanged
      ShowHint = True
      Style.BorderStyle = ebsFlat
      StyleFocused.BorderStyle = ebsFlat
      StyleHot.BorderStyle = ebsFlat
      TabOrder = 1
      OnEditing = ed_B_hEditing
    end
    object ed_B_s: TcxSpinEdit
      Left = 89
      Top = 0
      Width = 55
      Height = 21
      Hint = #1064#1080#1088#1086#1090#1072' - '#1057#1077#1082#1091#1085#1076#1072
      ParentShowHint = False
      Properties.ValueType = vtFloat
      Properties.OnChange = ed_B_sPropertiesEditValueChanged
      ShowHint = True
      Style.BorderStyle = ebsFlat
      StyleFocused.BorderStyle = ebsFlat
      StyleHot.BorderStyle = ebsFlat
      TabOrder = 2
      OnEditing = ed_B_hEditing
    end
    object ed_LAt: TcxSpinEdit
      Left = 148
      Top = 0
      Width = 122
      Height = 21
      Hint = #1064#1080#1088#1086#1090#1072
      ParentShowHint = False
      Properties.ValueType = vtFloat
      Properties.OnChange = ed_LAtPropertiesEditValueChanged
      ShowHint = True
      Style.BorderStyle = ebsFlat
      StyleFocused.BorderStyle = ebsFlat
      StyleHot.BorderStyle = ebsFlat
      TabOrder = 3
      OnEditing = ed_LAtEditing
    end
  end
  object rg_CoordSystem: TRadioGroup
    Left = 373
    Top = -5
    Width = 76
    Height = 60
    ItemIndex = 0
    Items.Strings = (
      'CK42'
      'CK95'
      'WGS 84')
    TabOrder = 2
    OnClick = rg_CoordSystemClick
  end
  object MaskEdit1: TMaskEdit
    Left = 20
    Top = 220
    Width = 98
    Height = 21
    EditMask = '900\'#176' 00\'#39' 00\,0\";1;_'
    MaxLength = 14
    TabOrder = 3
    Text = '023'#176' 23'#39' 45,6"'
    Visible = False
  end
  object cxSpinEdit1: TcxSpinEdit
    Left = 145
    Top = 216
    Width = 55
    Height = 21
    Hint = #1064#1080#1088#1086#1090#1072' - '#1057#1077#1082#1091#1085#1076#1072
    ParentShowHint = False
    Properties.ValueType = vtFloat
    Properties.OnChange = ed_B_sPropertiesEditValueChanged
    ShowHint = True
    Style.BorderStyle = ebsFlat
    StyleFocused.BorderStyle = ebsFlat
    StyleHot.BorderStyle = ebsFlat
    TabOrder = 4
    Visible = False
    OnEditing = ed_B_hEditing
  end
  object cb_East: TComboBox
    Left = 332
    Top = 27
    Width = 39
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 5
    Text = 'W'
    Items.Strings = (
      'W'
      'E')
  end
  object cb_North: TComboBox
    Left = 332
    Top = 3
    Width = 39
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 6
    Text = 'N'
    Items.Strings = (
      'N'
      'S')
  end
  object Panel2: TPanel
    Left = 8
    Top = 144
    Width = 369
    Height = 57
    Caption = 'Panel2'
    TabOrder = 7
    Visible = False
  end
  object CurrencyEdit11: TCurrencyEdit
    Left = 58
    Top = 64
    Width = 33
    Height = 23
    Alignment = taLeftJustify
    DisplayFormat = ',0'
    TabOrder = 8
    Value = 45.000000000000000000
    Visible = False
  end
  object CurrencyEdit12: TCurrencyEdit
    Left = 96
    Top = 64
    Width = 25
    Height = 23
    Alignment = taLeftJustify
    DisplayFormat = ',0'
    TabOrder = 9
    Value = 45.000000000000000000
    Visible = False
  end
  object CurrencyEdit13: TCurrencyEdit
    Left = 124
    Top = 64
    Width = 40
    Height = 23
    Alignment = taLeftJustify
    DisplayFormat = ',0.00'
    TabOrder = 10
    Value = 45.550000000000000000
    Visible = False
  end
  object CurrencyEdit14: TCurrencyEdit
    Left = 176
    Top = 64
    Width = 81
    Height = 23
    Alignment = taLeftJustify
    DisplayFormat = ',0.00000000'
    TabOrder = 11
    Value = 45.342342000000000000
    Visible = False
  end
  object CurrencyEdit21: TCurrencyEdit
    Left = 58
    Top = 96
    Width = 33
    Height = 23
    Alignment = taLeftJustify
    DisplayFormat = ',0'
    TabOrder = 12
    Value = 45.000000000000000000
    Visible = False
  end
  object CurrencyEdit22: TCurrencyEdit
    Left = 96
    Top = 96
    Width = 25
    Height = 23
    Alignment = taLeftJustify
    DisplayFormat = ',0'
    TabOrder = 13
    Value = 45.000000000000000000
    Visible = False
  end
  object CurrencyEdit23: TCurrencyEdit
    Left = 124
    Top = 96
    Width = 40
    Height = 23
    Alignment = taLeftJustify
    DisplayFormat = ',0.00'
    TabOrder = 14
    Value = 45.550000000000000000
    Visible = False
  end
  object CurrencyEdit24: TCurrencyEdit
    Left = 176
    Top = 96
    Width = 81
    Height = 23
    Alignment = taLeftJustify
    DisplayFormat = ',0.00000000'
    TabOrder = 15
    Value = 45.342342000000000000
    Visible = False
  end
end
