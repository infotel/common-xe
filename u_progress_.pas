unit u_progress_;

interface

uses
  Forms, Windows, Messages;


const
  WM_PROGRESS = WM_USER + 1000;
 

type
  TProgressMessageKind = (msgProgress1,msgProgress2,msgProgressMsg1, msgClear) ;

  
function ProgressPostMessage(aMsg: TProgressMessageKind; aWParam: WPARAM;
    aLParam: LPARAM): longbool;
                                                                                 

implementation

function ProgressPostMessage(aMsg: TProgressMessageKind; aWParam: WPARAM;
    aLParam: LPARAM): longbool;
begin
//  Result:=
  Result:=PostMessage(Application.Handle, WM_PROGRESS + Integer(aMsg), aWParam,aLParam );
  
//  Result:=PostMessage(Handle, WM_PROGRESS + Integer(aMsg), WParam,LParam );
  // TODO -cMM: ProgressPostMessage default body inserted
end;

end.

(*
{$IFNDEF UNICODE}
function SendTextMessage(Handle: HWND; Msg: UINT; WParam: WPARAM; LParam: AnsiString): LRESULT;
begin
  Result := SendMessage(Handle, Msg, WParam, Winapi.Windows.LPARAM(PAnsiChar(LParam)));
end;
{$ENDIF}


}
