unit f_neva_DM_Preview;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  OleCtrls, xdmx_TLB, StdCtrls, ToolWin, ComCtrls, Placemnt;

type
  Tfrm_DM_Map_preview = class(TForm)
    DmxActiveX1: TDmxActiveX;
    ToolBar1: TToolBar;
    cb_Maps: TComboBox;
    FormPlacement1: TFormPlacement;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    procedure BeginUpdate;
    procedure EndUpdate;

    procedure Add (aFileName: string);
  public

    class procedure CreateView (aFileName: string); overload;
    class procedure CreateView (aFileNameList: TStrings); overload;
  end;


//======================================================
implementation {$R *.DFM}
//======================================================


//-------------------------------------------------------------------
class procedure Tfrm_DM_Map_preview.CreateView (aFileNameList: TStrings);
//-------------------------------------------------------------------
var
  i: integer;
begin
  with Tfrm_DM_Map_preview.Create(Application) do
  begin
    cb_Maps.Items.Clear;

    BeginUpdate;
    for i:=0 to aFileNameList.Count-1 do
      Add (aFileNameList[i]);
    EndUpdate;

    Show;
  end;
end;


//-------------------------------------------------------------------
class procedure Tfrm_DM_Map_preview.CreateView (aFileName: string);
//-------------------------------------------------------------------
begin
  with Tfrm_DM_Map_preview.Create(Application) do
  begin
    cb_Maps.Items.Clear;

    BeginUpdate;
    Add (aFileName);
    EndUpdate;

    Show;
  end;
end;

//-------------------------------------------------------------------
procedure Tfrm_DM_Map_preview.Add (aFileName: string);
//-------------------------------------------------------------------
var
  sExt: string;
begin
  sExt:=LowerCase(ExtractFileExt (aFileName));
  if sExt='.dm' then
    DmxActiveX1.Insert_Map (aFileName) else

  if (sExt='.bmp') or
     (sExt='.gif') or
     (sExt='.jpg')
  then
    DmxActiveX1.Image:=aFileName;


  with cb_Maps do begin
    Items.Add (aFileName);
    if ItemIndex<0 then ItemIndex:=0; 
  end;

end;


procedure Tfrm_DM_Map_preview.FormCreate(Sender: TObject);
begin
  Caption:='�����';
  DmxActiveX1.Align:=alClient;
end;


procedure Tfrm_DM_Map_preview.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
end;


procedure Tfrm_DM_Map_preview.BeginUpdate;
begin
  Screen.Cursor:=crHourGlass;
  DmxActiveX1.isMoving:=True;
end;


procedure Tfrm_DM_Map_preview.EndUpdate;
begin
  DmxActiveX1.isMoving:=False;
  DmxActiveX1.Show_Window(1,1,0,0);
  Screen.Cursor:=crDefault;

end;


end.
