object frm_DM_Map_preview: Tfrm_DM_Map_preview
  Left = 314
  Top = 178
  Width = 427
  Height = 376
  Caption = 'Map'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDefault
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object DmxActiveX1: TDmxActiveX
    Left = 0
    Top = 29
    Width = 419
    Height = 292
    ParentColor = False
    ParentFont = False
    Align = alTop
    TabOrder = 0
    ControlData = {
      545046300B54446D78416374697665580A446D7841637469766558044C656674
      020003546F70021D05576964746803A301064865696768740324010D4178426F
      726465725374796C6507076166624E6F6E650743617074696F6E060A446D7841
      63746976655805436F6C6F720709636C42746E466163650C466F6E742E436861
      72736574070F44454641554C545F434841525345540A466F6E742E436F6C6F72
      070C636C57696E646F77546578740B466F6E742E48656967687402F509466F6E
      742E4E616D65060D4D532053616E732053657269660A466F6E742E5374796C65
      0B000A4B657950726576696577090E4F6C644372656174654F7264657208094F
      6E4B6579446F776E0711416374697665466F726D4B6579446F776E0D50697865
      6C73506572496E636802600A54657874486569676874020D0000}
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 419
    Height = 29
    ButtonHeight = 21
    Caption = 'ToolBar1'
    TabOrder = 1
    object cb_Maps: TComboBox
      Left = 0
      Top = 2
      Width = 293
      Height = 21
      Style = csDropDownList
      Anchors = [akLeft, akTop, akRight]
      ItemHeight = 13
      TabOrder = 0
    end
  end
  object FormPlacement1: TFormPlacement
    Left = 24
    Top = 42
  end
end
