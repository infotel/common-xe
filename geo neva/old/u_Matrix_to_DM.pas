unit u_Matrix_to_DM;

interface
uses SysUtils,Windows,
     u_func,
     u_func_img,
     u_files,

     u_Matrix,
     u_classes,
     u_GEO,
     u_Geo_classes,

     u_progress,

     u_MatrixDecomposer

     //u_Neva_Func
     ;

type
  //------------------------------------------------------
  // ������������ ����� DM �� 2D ��������� �������
  //------------------------------------------------------
  TMatrixToDM = class (TProgress)
  //------------------------------------------------------
  private
    FMatrix : TOrdinalMatrix;

    FMatrixDecomposer : TMatrixDecomposer; // ������� � �������
    FColorMatrix      : TIntMatrix;

    procedure DoOnMapNewShape (Sender: TObject;
                               aBLPoints: TBLPointList;
                             //  aShapeLine: TMatrixPolyline;
                               aFillValue:integer);

    procedure ProcessMatrix (aMatrix: TOrdinalMatrix; aMapFileName: string);
    function  GetColorIndexByValue (aValue: integer): integer;
//    procedure InitColors();
    function GetGradientColor (aValue: integer): integer;

  public
    Params : record
       DestMapFileName : string;
     //  ObjFileName     : string;
       ObjCode         : integer;

       SrcMatrix       : TOrdinalMatrix;

       ColorFillType: (cftGradient,cftGrid);

       Gradient : record
          MinColor : integer;  // ����. �������� : -20
          MaxColor : integer;  // ���. ��������  : -200
          MinValue : integer;
          MaxValue : integer;
       end;

       ColorValues : array of record
          MinValue : integer; // �������� � �������
          MaxValue : integer; // �������� � �������
          Color    : integer; // ���� �� �����
          IsTransparent: boolean;
       end;

    end;

    constructor Create;
    destructor Destroy; override;

    procedure ExecuteProc; override;
  end;


//======================================================
implementation
//======================================================

const
   MCODE_REGION = 20000010;  //��� ������ ������������


//---------------------------------------------------------
function MakeGradientColor (aValue,aMinValue,aMaxValue : integer;
                            aBeginColor,aEndColor      : integer ): integer;
//---------------------------------------------------------
var r1,g1,b1,r2,g2,b2: byte;
begin
  if (aValue<aMinValue) or
     (aValue>aMaxValue) then begin Result:=0; Exit; end;

  aMaxValue:=aMaxValue-aMinValue;
  aValue   :=aValue-aMinValue;

  ColorToRGB(aBeginColor, R1,G1,B1);
  ColorToRGB (aEndColor,   R2,G2,B2);

  if aMaxValue <> 0 then
    Result:=RGB(Byte(Trunc(R1 + (R2-R1) * aValue / aMaxValue)),
                Byte(Trunc(G1 + (G2-G1) * aValue / aMaxValue)),
                Byte(Trunc(B1 + (B2-B1) * aValue / aMaxValue)))
  else Result:=0;
end;


constructor TMatrixToDM.Create;
begin
  inherited;
  FMatrixDecomposer:=TMatrixDecomposer.Create;
  FMatrixDecomposer.OnProgress:=OnProgress; // DoOnProgressEvent;
  FMatrixDecomposer.OnNewBLShape:=DoOnMapNewShape;
  FColorMatrix:=TIntMatrix.Create;

end;


destructor TMatrixToDM.Destroy;
begin
  FMatrixDecomposer.Free;
  FColorMatrix.Free;
  inherited;
end;

//---------------------------------------------------------
function TMatrixToDM.GetGradientColor (aValue: integer): integer;
//---------------------------------------------------------
begin
  Result:=MakeGradientColor (aValue,
                             Params.Gradient.MinValue, Params.Gradient.MaxValue,
                             Params.Gradient.MinColor, Params.Gradient.MaxColor);
end;


//-----------------------------------------------------
function TMatrixToDM.GetColorIndexByValue (aValue: integer): integer;
//-----------------------------------------------------
var i: integer;
begin
  case Params.ColorFillType of
    cftGrid:     for i:=0 to High(Params.ColorValues) do
                  if (Params.ColorValues[i].MinValue=aValue) then
                  begin
                    Result:=i; Exit;
                  end;

    cftGradient: for i:=0 to High(Params.ColorValues) do
                  if (Params.ColorValues[i].MinValue<=aValue) and
                     (Params.ColorValues[i].MaxValue>aValue)
                  then begin
                    Result:=i; Exit;
                  end;
  end;
  Result:=-1;
end;

//------------------------------------------------------
procedure TMatrixToDM.DoOnMapNewShape (Sender: TObject;
                         aBLPoints: TBLPointList;
                         aFillValue: integer);
//------------------------------------------------------
var neva_cl,iBrush,cl,i: integer; blPoints: TBLPointArray;
    // in the matrix - to draw the map; min and max values
begin
  SetLength (blPoints, aBLPoints.Count);

  for i:=0 to aBLPoints.Count-1 do begin
    blPoints[i].B := aBLPoints[i].Value.B; //X;
    blPoints[i].L := aBLPoints[i].Value.L; //Y;
  end;

  iBrush:=NEVA_PATTERN_SOLID;

  if Params.ColorFillType=cftGradient then begin
    cl:=aFillValue;
  end else begin
    if aFillValue <> -1 then begin
                           cl:=Params.ColorValues[aFillValue].Color;
                           if Params.ColorValues[aFillValue].IsTransparent then
                             iBrush:=NEVA_PATTERN_DOTS;
                        end else cl:=0;
  end;

  // don't show black color
  if cl<>0 then begin
//    neva_cl:=neva_Make_Color (cl, iBrush);
    neva_Insert_BLPolygone (MCODE_REGION, blPoints, cl, iBrush, false);
  end;
end;


//------------------------------------------------------
procedure TMatrixToDM.ExecuteProc();
//------------------------------------------------------
var tmpMatrix : TOrdinalMatrix;
    r,c,iColor,i,iValue,iMinValue: integer;
    blBounds: TBLRect;
begin
   tmpMatrix:=Params.SrcMatrix;

   if (tmpMatrix.RowCount=0) or (tmpMatrix.ColCount=0) then  begin
       DoLog ('���������� ����������� ������� ��� ����� DM.');
       if not Assigned(Params.SrcMatrix) then
         tmpMatrix.Free;
       Exit;
   end;

   blBounds:=tmpMatrix.GetBLBounds();

   ForceDirByFileName (Params.DestMapFileName);

   neva_Create_Map (Params.DestMapFileName, '', blBounds);

   //��������� ������� ������

   FColorMatrix.AssignHeader (tmpMatrix);
   FColorMatrix.FillBlank;

   with tmpMatrix do
   for r:=0 to RowCount-1 do
   for C:=0 to ColCount-1 do
     if not IsNull(r,c) then
   begin
     iValue:=Items[r,c];
     if iValue>0 then
       iValue:=iValue;

     if Params.ColorFillType=cftGradient
       then iColor:=GetGradientColor (iValue)
       else iColor:=GetColorIndexByValue (iValue);

     FColorMatrix[r,c]:=iCOlor;

     if c=0 then
       DoProgress (r, RowCount);
   end;

   ProcessMatrix (FColorMatrix, Params.DestMapFileName);

   if not Assigned(Params.SrcMatrix) then
     tmpMatrix.Free;

//  Result:=True;
end;

//------------------------------------------------------
procedure TMatrixToDM.ProcessMatrix (aMatrix:TOrdinalMatrix; aMapFileName: string);
//------------------------------------------------------
var blPoints: TBLPointArray;
begin
  FMatrix:=aMatrix;

  if not neva_Open_Map(aMapFileName,fmOpenWrite) then begin
    DoLog('������ ��� �������� �����: '+aMapFileName); Exit;
  end;

//  Log('�������� ������� ��� ��������...');
//  CutPolygon();
  DoLog ('������ ������������ �����: '+ TimeToStr(Now));

  neva_Goto_Down();

//  blPoints:=geo_XYRectToBLPoints (aMatrix.GetXYBounds(), 0);
 // neva_Insert_BLPolygone (MCODE_REGION, blPoints, 0, False);

  FMatrixDecomposer.Decompose(aMatrix);
  DoLog ('����� ������������ �����: ' + TimeToStr(Now));
  DoLog (Format('����� / c����������� ����������: %d / %d', [aMatrix.RowCount*aMatrix.ColCount, FMatrixDecomposer.ShapesTotal]));
  neva_Close_Map;
end;



end.



