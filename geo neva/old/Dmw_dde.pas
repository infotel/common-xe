unit Dmw_dde; interface

uses
  Windows,SysUtils,oTypes,dmw_use;

function dmw_Open(path: pChar; edit: boolean): Integer;
procedure dmw_Done;

function dmw_Server_Exist: boolean; stdcall;
procedure dmw_dde_Abort; stdcall;

function dmw_PickMessage(s: pChar): boolean; stdcall;
function dmw_PickCaption(s: pChar): boolean; stdcall;

function dmw_PickPoint(var ix,iy: longint; var gx,gy: double): boolean;
stdcall;
{����� ����� -> <ix,iy> , <gx,gy> - ���������� �����}

function dmw_PickGauss(locator: Boolean;
                       var ix,iy: longint;
                       var gx,gy,gz: double): boolean; stdcall;

function dmw_PickRect(var x1,y1,x2,y2: longint): boolean;
stdcall;
{����� �������� -> <x1 y1 x2 y2>}

function dmw_PickPort(w,h: longint; var x1,y1,x2,y2: longint): boolean;
stdcall;
{����� �������� � ��������� ������� <w> � ������� <h> -> <x1 y1 x2 y2>}

function dmw_PickPoly(lock: boolean; lp: pLLine; max: word): boolean;
stdcall;
{����� ���������/������� -> <lp> - ��������� }
{ <max> - ������������ ���������� �����      }
{ <lock> =0 - ���������; =1 - �������        }

function dmw_PickVector(var x1,y1,x2,y2: longint): boolean;
stdcall; {����� ������ -> <x1 y1 x2 y2>}

function dmw_PickRing(var x,y,r: longint): boolean;
stdcall; {����� ���������� -> <x,y,r>}

function dmw_PickObject(var offs,Code: longint; var Tag: byte;
                        var x1,y1,x2,y2: longint;
                        name: pChar; len: word): boolean;
stdcall;
{����� ������ -> <offs Code Tag x1 y1 x2 y2 name>}

function dmw_PickImage(var x1,y1,x2,y2: longint): boolean;
stdcall;

function dmw_PickGeoid(w,h: longint; var x1,y1,x2,y2: longint): boolean;
stdcall;

function dmw_PickMove(w,h: longint; var x1,y1,x2,y2: longint): boolean;
stdcall;

function dmw_ChooseObject(iCode: longint; iTag: byte;
                          var Code: longint; var Tag: byte;
                          name: pChar; len: word): boolean;
stdcall;
{������� ������ � ���� -> <Code Tag Name>}

function dmw_GetColor(Index: Integer): Integer; stdcall;

function dmw_ChooseColor(iIndex: byte; var Index: byte;
                         var Color: longint): boolean;
stdcall;
{������� ���� � ������� -> Index ColorRef}

function dmw_DialogInfo(p: longint): boolean; stdcall;

function dmw_HideMap: boolean; stdcall;
{������� �������� ����� ��� ��������������}

function dmw_BackMap: boolean; stdcall;
{������� �������� ����� ��� ��������������}

function dmw_SetFocus: boolean; stdcall;
{���������� ����� ����� �� ������� ����}

function dmw_HideWindow: boolean;
stdcall; {������� ���� "��������"}

function dmw_BackWindow: boolean;
stdcall; {������� ���� "��������"}

function dmw_ShowWindow(x1,y1,x2,y2: longint): boolean;
stdcall; {���������� �������� ����� <x1,y1,x2,y2> � ������� ����}

function dmw_ShowPoint(gx,gy,scale: double): boolean; stdcall;
{���������� ��� �������� ���� ����� <gx,gy> � ������� <scale> }
{gx,gy - �����; scale - 1:xxxxx                               }

function dmw_ShowObject(offs: longint; scale: double): boolean;
stdcall;
{���������� ������ <offs> �� �������� <scale> }
{offs - ��������� �� ������                   }

function dmw_SwapMap(fn: pChar): boolean; stdcall;
{���������� ����� � ������ <fn> ��������}

function dmi_ShowLink(id: longint; scale: double): boolean;
stdcall;
{���������� ������ <id> �� �������� <scale> }
{id - ����� �������                         }

function dmw_FreeObject: boolean; stdcall;
{���������� ��������� � ������� ���� ������}

function dmw_DrawObject(offs: longint): boolean; stdcall;
{���������� ������ <offs>, ��� offs - ��������� �� ������}

function dmw_HideObject(offs: longint): boolean; stdcall;
{������� ������ <offs>, ��� offs - ��������� �� ������}

function dmw_StackPush(gx,gy,scale: double; msg: pChar): boolean;
stdcall; {�������� �������� � ����}

function dmw_StackClear: boolean; stdcall;
{�������� ����}

function dmw_InsertMap(fn: pChar): boolean; stdcall;
{�������� ����� � ������ <fn> � ������}

function dmw_DeleteMap(fn: pChar): boolean; stdcall;
{������� ����� � ������ <fn> �� �������}

function dmw_OpenProject(fn: pChar): boolean; stdcall;
{������� ������ <fn>}

function dmw_ChangeProject(fn: pChar): boolean; stdcall;
{������� ������ <fn>}

function dmw_PcxPath(fn: pChar; len: Integer): pChar; stdcall;
{���������� ��� ������}

function dmw_PcxOpen(fn: pChar): boolean; stdcall;
{������� ����� <fn>}

function dmw_DrawPoly(lp: pLLine; Tag: byte; cl: longint): boolean;
stdcall;
{���������� ��������� (Tag=2) / ������� (Tag = 3), cl - �������}

function dmw_DrawPgm(x,y: longint; i: word; pgm: pChar): boolean;
stdcall;
{���������� ��������� ���� � �����  <x,y> }
{i - ����� �����; pgm - ��� ����������    }

function dmw_DrawVgm(x1,y1,x2,y2: longint; i: word; pgm: pChar): boolean;
stdcall;
{���������� ��������� ���� � �����  <x1,y1> }
{i - ����� �����; vgm - ��� ����������      }

procedure dmw_DrawText(x1,y1,x2,y2, Code,Up: Integer; s: pChar);
stdcall;

function dmw_InsObject(Code: longint; Tag: byte; txt: pChar; lp: pLLine): boolean;
stdcall;
{�������� ������, ��� Code - ���, Tag - ���, txt - �����; lp - ����������}

function dmw_MoveObject(offs, dx,dy: longint): boolean;
stdcall;
{����������� ������, ��� offs - ���������, <dx,dy> - ��������}

function dmw_MoveSign(offs, x1,y1,x2,y2: longint): boolean;
stdcall;
{����������� ����,��� offs - ���������, }
{<x1,y1> - �����, <x2,y2> - ����������� }

function info_SetInt(offs: longint; nn: word; equ: longint): boolean;
stdcall;
{��������� ������� ����� ��������������, ��� offs - ���������, }
{<nn,equ> - ����� � �������� ��������������                    }

function info_SetReal(offs: longint; nn: word; equ: float): boolean;
stdcall;
{��������� ������� ��������� ��������������, ��� offs - ���������, }
{<nn,equ> - ����� � �������� ��������������                        }

function info_SetStr(offs: longint; nn: word; equ: pChar): boolean;
stdcall;
{��������� ������� ��������� ��������������, ��� offs - ���������, }
{<nn,equ> - ����� � �������� ��������������                        }

function dmw_ListAppend(offs: longint): boolean;
stdcall;

function dmw_ListContains(offs: longint): boolean;
stdcall;

function dmw_ListClear: boolean; stdcall;
function dmw_ListDelete: boolean; stdcall;
function dmw_ListCode(Code: longint; Loc: byte): boolean; stdcall;

function dmw_DrawMsg(Wnd: hWnd; Msg: word): boolean; stdcall;
function dmw_MoveMsg(Wnd: hWnd; Msg: word): boolean; stdcall;

function win_ChildShow(Wnd: hWnd; Msg: word): boolean; stdcall;
function win_ChildHide: boolean; stdcall;

function dmw_MainMinimize: boolean; stdcall;
function dmw_MainNormal: boolean; stdcall;
function dmw_MainClose: boolean; stdcall;

function dmw_GetRoot: longint; stdcall;
{���������� ��������� �� ������ �����}

function dmw_AltProject(fn: pChar; len: word): pChar;
stdcall;
{���������� ��� ����� �� ������� ���� � �������}

function dmw_AllProject(fn: pChar; len: word): pChar;
stdcall;
{���������� ��� ����� � ������ ������� ���� � �������}

function dmw_ActiveMap(fn: pChar; len: word): pChar;
stdcall; {���������� ��� �������� �����}

function dmw_MapsCount: Integer; stdcall;
{���������� ���������� ���� � �������}

function dmw_ProjectMap(i: word; fn: pChar; len: word): pChar;
stdcall;
{���������� ��� ����� � �������, ��� i - ���������� �����}

function dmw_MapContains(x,y: longint; fn: pChar; len: word): SmallInt;
stdcall;
{���������� ���������� ����� � ��� ����� <fn> � �������, }
{������� �������� ����� � ������������ <x,y>             }
{<len> - ������������ ����� ������ <fn>                  }

function dmw_GetWindow(var x1,y1,x2,y2: longint): boolean;
stdcall;
{���������� <x1,y1,x2,y2> - ���������� �������� ����}

function dmw_GetCentre(var gx,gy,sc: double): boolean;
stdcall;
{���������� <gx,gy,sc> - ����� � ������� �������� ����}

function dmw_Offs_Id(offs: longint; var id: longint): boolean;
stdcall;
{���������� ����� �������,��� offs - ���������, id - �����}

function dmw_Id_Offs(id: longint; var offs: longint): boolean;
stdcall;
{���������� ��������� �� ������,��� id - �����, offs - ���������}

function dmw_IntObject(Code: longint; Tag: byte; nn: word; equ: longint): longint;
stdcall;
{���������� ��������� �� ������,��� <Code,Tag> - ��� � ��� ������� }
{<nn,equ> - ����� � �������� ����� ��������������                  }

function dmw_StrObject(Code: longint; Tag: byte; nn: word; equ: pChar): longint;
stdcall;
{���������� ��������� �� ������,��� <Code,Tag> - ��� � ��� ������� }
{<nn,equ> - ����� � �������� ��������� ��������������              }

function info_GetInt(offs: longint; nn: word; var equ: longint): boolean;
stdcall;
{���������� ��� ������� <offs> �������� <equ>}
{����� �������������� � ������� <nn>         }

function info_GetReal(offs: longint; nn: word; var equ: double): boolean;
stdcall;
{���������� ��� ������� <offs> �������� <equ>}
{��������� �������������� � ������� <nn>     }

function info_GetStr(offs: longint; nn: word; equ: pChar; len: word): pChar;
stdcall;
{���������� ��� ������� <offs> �������� <equ>}
{��������� �������������� � ������� <nn>     }

function dmw_MenuObject(var Code: longint; var Tag: byte;
                        name: pChar; len: word): pChar;
stdcall;
{���������� ��� ���������� � ���� �������}
{��� <Code>, ��� <Tag>, �������� <p>     }

function dmw_OffsObject(var offs,Code: longint; var Tag: byte;
                        var x1,y1,x2,y2: longint;
                        name: pChar; len: word): pChar;
stdcall;
{���������� ��� ���������� � ������� ���� �������}
{��������� <offs>, ��� <Code>, ��� <Tag>,        }
{���������� ����� <x1,y1,x2,y2>, �������� <p>    }

function dmi_LinkObject(var id,Code: longint; var Tag: byte): boolean;
stdcall;
{���������� ��� ���������� � ������� ���� �������}
{����� <id>, ��� <Code>, ��� <Tag>               }

function dmi_PasteObject(id: longint; fn: pChar): longint;
stdcall;

function dmw_ListRect( var x1,y1,x2,y2: longint): boolean;
stdcall;

function dmw_L_to_G(ix,iy: longint; var gx,gy: double): boolean; stdcall;
function dmw_G_to_L(gx,gy: double; var ix,iy: longint): boolean; stdcall;

function dmw_L_to_B(ix,iy: longint; var ox,oy: longint): boolean; stdcall;
function dmw_B_to_L(ix,iy: longint; var ox,oy: longint): boolean; stdcall;

procedure dmw_lp_View(v: Integer); stdcall;

function sau_Track(fl: byte): boolean; stdcall;
function sau_Ctrl: boolean; stdcall;

function dmw_English: boolean; stdcall;

procedure dmw_dlg_Options; stdcall;
procedure dmw_dlg_Layers; stdcall;
procedure dmw_dlg_Ground; stdcall;
procedure dmw_dlg_Object; stdcall;
procedure dmw_Restore_Graphics; stdcall;

function dmw_win_Objects_Count: Integer; stdcall;
function dmw_win_Objects(I: Integer): Integer; stdcall;

function dmw_sel_Objects_Count: Integer; stdcall;
function dmw_sel_Objects(I: Integer): Integer; stdcall;

const
  pen_Code    = 21;
  pen_View    = 22;
  pen_Info    = 23;
  pen_Text    = 24;
  pen_First   = 25;
  pen_Color   = 26;
  pen_Move    = 27;
  pen_Copy    = 28;
  pen_Rotate  = 29;
  pen_Mirror  = 30;
  pen_Delete  = 31;
  pen_Access  = 32;
  pen_Calc    = 33;
  pen_Attr    = 34;

  pen_pmov    = 41;
  pen_pdel    = 42;
  pen_lmov    = 43;
  pen_ladd    = 44;
  pen_ldel    = 45;
  pen_lock    = 46;
  pen_swap    = 47;
  pen_ldiv    = 48;
  pen_Join    = 49;
  pen_pcls    = 50;
  pen_pline   = 51;
  pen_prect   = 52;
  pen_Scale   = 53;
  pen_Run     = 54;
  pen_Curve   = 55;

procedure dmw_Change_Tool(pen: Integer); stdcall;

procedure dmw_Show_Gauss(lt_x,lt_y,rb_x,rb_y: Double; pps: Integer);
stdcall;

function dmw_Get_Gauss(out lt_x,lt_y,rb_x,rb_y: Double): Integer;
stdcall;

implementation

const
  dll_dde = 'dll_dde.dll';

function dmw_Open(path: pChar; edit: boolean): Integer;
begin
  Result:=0; if dmw_HideMap then
  Result:=dm_Open(path,edit)
end;

procedure dmw_Done;
begin
  dm_Done; dmw_BackMap
end;

function dmw_Server_Exist: boolean; external dll_dde;
procedure dmw_dde_Abort; external dll_dde;

function dmw_PickMessage(s: pChar): boolean; external dll_dde;
function dmw_PickCaption(s: pChar): boolean; external dll_dde;

function dmw_PickPoint(var ix,iy: longint; var gx,gy: double): boolean;
external dll_dde;

function dmw_PickGauss(locator: Boolean;
                       var ix,iy: longint;
                       var gx,gy,gz: double): boolean;
external dll_dde;

function dmw_PickRect(var x1,y1,x2,y2: longint): boolean;
external 'dll_dde.dll';

function dmw_PickPort(w,h: longint; var x1,y1,x2,y2: longint): boolean;
external 'dll_dde.dll';

function dmw_PickPoly(lock: boolean; lp: pLLine; max: word): boolean;
external 'dll_dde.dll';

function dmw_PickVector(var x1,y1,x2,y2: longint): boolean;
external 'dll_dde.dll';

function dmw_PickRing(var x,y,r: longint): boolean;
external 'dll_dde.dll';

function dmw_PickObject(var offs,Code: longint; var Tag: byte;
                        var x1,y1,x2,y2: longint;
                        name: pChar; len: word): boolean;
external dll_dde;

function dmw_PickImage(var x1,y1,x2,y2: longint): boolean;
external dll_dde;

function dmw_PickGeoid(w,h: longint; var x1,y1,x2,y2: longint): boolean;
external dll_dde;

function dmw_PickMove(w,h: longint; var x1,y1,x2,y2: longint): boolean;
external dll_dde;

function dmw_ChooseObject(iCode: longint; iTag: byte;
                          var Code: longint; var Tag: byte;
                          name: pChar; len: word): boolean;
external dll_dde;

function dmw_ChooseColor(iIndex: byte; var Index: byte;
                         var Color: longint): boolean;
external dll_dde;

function dmw_GetColor(Index: Integer): Integer;
external dll_dde;

function dmw_DialogInfo(p: longint): boolean;
external 'dll_dde.dll';

function dmw_HideMap: boolean;
external 'dll_dde.dll';

function dmw_BackMap: boolean;
external 'dll_dde.dll';

function dmw_SetFocus: boolean;
external 'dll_dde.dll';

function dmw_CopyWindow(x1,y1,x2,y2: longint; fn: pChar): boolean;
external 'dll_dde.dll';

function dmw_HideWindow: boolean; external 'dll_dde.dll';
function dmw_BackWindow: boolean; external 'dll_dde.dll';

function dmw_ShowWindow(x1,y1,x2,y2: longint): boolean;
external 'dll_dde.dll';

function dmw_ShowPoint(gx,gy,scale: double): boolean;
external 'dll_dde.dll';

function dmw_ShowObject(offs: longint; scale: double): boolean;
external 'dll_dde.dll';

function dmw_SwapMap(fn: pChar): boolean;
external 'dll_dde.dll';

function dmi_ShowLink(id: longint; scale: double): boolean;
external 'dll_dde.dll';

function dmw_FreeObject: boolean;
external 'dll_dde.dll';

function dmw_DrawObject(offs: longint): boolean;
external 'dll_dde.dll';

function dmw_HideObject(offs: longint): boolean;
external 'dll_dde.dll';

function dmw_StackPush(gx,gy,scale: double; msg: pChar): boolean;
external 'dll_dde.dll';

function dmw_StackClear: boolean;
external 'dll_dde.dll';

function dmw_InsertMap(fn: pChar): boolean;
external 'dll_dde.dll';

function dmw_DeleteMap(fn: pChar): boolean;
external 'dll_dde.dll';

function dmw_OpenProject(fn: pChar): boolean; external dll_dde;
function dmw_ChangeProject(fn: pChar): boolean; external dll_dde;

function dmw_PcxPath(fn: pChar; len: Integer): pChar;
external 'dll_dde.dll';

function dmw_PcxOpen(fn: pChar): boolean;
external 'dll_dde.dll';

function dmw_DrawPoly(lp: pLLine; Tag: byte; cl: longint): boolean;
external 'dll_dde.dll';

function dmw_DrawPgm(x,y: longint; i: word; pgm: pChar): boolean;
external 'dll_dde.dll';

function dmw_DrawVgm(x1,y1,x2,y2: longint; i: word; pgm: pChar): boolean;
external 'dll_dde.dll';

procedure dmw_DrawText(x1,y1,x2,y2, Code,Up: Integer; s: pChar);
external 'dll_dde.dll';

function dmw_InsObject(Code: longint; Tag: byte; txt: pChar; lp: pLLine): boolean;
external 'dll_dde.dll';

function dmw_MoveObject(offs, dx,dy: longint): boolean;
external 'dll_dde.dll';

function dmw_MoveSign(offs, x1,y1,x2,y2: longint): boolean;
external 'dll_dde.dll';

function info_SetInt(offs: longint; nn: word; equ: longint): boolean;
external 'dll_dde.dll';

function info_SetReal(offs: longint; nn: word; equ: float): boolean;
external 'dll_dde.dll';

function info_SetStr(offs: longint; nn: word; equ: pChar): boolean;
external 'dll_dde.dll';

function dmw_ListAppend(offs: longint): boolean;
external 'dll_dde.dll';

function dmw_ListContains(offs: longint): boolean;
external 'dll_dde.dll';

function dmw_ListClear: boolean; external 'dll_dde.dll';
function dmw_ListDelete: boolean; external 'dll_dde.dll';
function dmw_ListCode(Code: longint; Loc: byte): boolean;
external 'dll_dde.dll';

function dmw_DrawMsg(Wnd: hWnd; Msg: word): boolean; external 'dll_dde.dll';
function dmw_MoveMsg(Wnd: hWnd; Msg: word): boolean; external 'dll_dde.dll';

function win_ChildShow(Wnd: hWnd; Msg: word): boolean; external 'dll_dde.dll';
function win_ChildHide: boolean; external 'dll_dde.dll';

function dmw_MainMinimize: boolean; external 'dll_dde.dll';
function dmw_MainNormal: boolean; external 'dll_dde.dll';
function dmw_MainClose: boolean; external 'dll_dde.dll';

function dmw_GetRoot: longint;
external 'dll_dde.dll';

function dmw_AltProject(fn: pChar; len: word): pChar;
external 'dll_dde.dll';

function dmw_AllProject(fn: pChar; len: word): pChar;
external 'dll_dde.dll';

function dmw_ActiveMap(fn: pChar; len: word): pChar;
external 'dll_dde.dll';

function dmw_MapsCount: Integer;
external 'dll_dde.dll';

function dmw_ProjectMap(i: word; fn: pChar; len: word): pChar;
external 'dll_dde.dll';

function dmw_MapContains(x,y: longint; fn: pChar; len: word): SmallInt;
external 'dll_dde.dll';

function dmw_GetWindow(var x1,y1,x2,y2: longint): boolean;
external 'dll_dde.dll';

function dmw_GetCentre(var gx,gy,sc: double): boolean;
external 'dll_dde.dll';

function dmw_Offs_Id(offs: longint; var id: longint): boolean;
external 'dll_dde.dll';

function dmw_Id_Offs(id: longint; var offs: longint): boolean;
external 'dll_dde.dll';

function dmw_IntObject(Code: longint; Tag: byte; nn: word; equ: longint): longint;
external 'dll_dde.dll';

function dmw_StrObject(Code: longint; Tag: byte; nn: word; equ: pChar): longint;
external 'dll_dde.dll';

function info_GetInt(offs: longint; nn: word; var equ: longint): boolean;
external 'dll_dde.dll';

function info_GetReal(offs: longint; nn: word; var equ: double): boolean;
external 'dll_dde.dll';

function info_GetStr(offs: longint; nn: word; equ: pChar; len: word): pChar;
external 'dll_dde.dll';

function dmw_MenuObject(var Code: longint; var Tag: byte;
                        name: pChar; len: word): pChar;
external 'dll_dde.dll';

function dmw_OffsObject(var offs,Code: longint; var Tag: byte;
                        var x1,y1,x2,y2: longint;
                        name: pChar; len: word): pChar;
external 'dll_dde.dll';

function dmi_LinkObject(var id,Code: longint; var Tag: byte): boolean;
external 'dll_dde.dll';

function dmi_PasteObject(id: longint; fn: pChar): longint;
external 'dll_dde.dll';

function dmw_ListRect(var x1,y1,x2,y2: longint): boolean;
external 'dll_dde.dll';

function dmw_L_to_G(ix,iy: longint; var gx,gy: double): boolean;
external 'dll_dde.dll';

function dmw_G_to_L(gx,gy: double; var ix,iy: longint): boolean;
external 'dll_dde.dll';

function dmw_L_to_B(ix,iy: longint; var ox,oy: longint): boolean;
external 'dll_dde.dll';

function dmw_B_to_L(ix,iy: longint; var ox,oy: longint): boolean;
external 'dll_dde.dll';

procedure dmw_lp_View(v: Integer); external 'dll_dde.dll';

function sau_Track(fl: byte): boolean; external 'dll_dde.dll';
function sau_Ctrl: boolean; external 'dll_dde.dll';

function dmw_English: boolean; external 'dll_dde.dll';

procedure dmw_dlg_Options; external dll_dde;
procedure dmw_dlg_Layers; external dll_dde;
procedure dmw_dlg_Ground; external dll_dde;
procedure dmw_dlg_Object; external dll_dde;
procedure dmw_Restore_Graphics; external dll_dde;

function dmw_win_Objects_Count: Integer; external dll_dde;
function dmw_win_Objects(I: Integer): Integer; external dll_dde;

function dmw_sel_Objects_Count: Integer; external dll_dde;
function dmw_sel_Objects(I: Integer): Integer; external dll_dde;

procedure dmw_Change_Tool(pen: Integer); external dll_dde;

procedure dmw_Show_Gauss(lt_x,lt_y,rb_x,rb_y: Double; pps: Integer);
external dll_dde;

function dmw_Get_Gauss(out lt_x,lt_y,rb_x,rb_y: Double): Integer;
external dll_dde;

end.
