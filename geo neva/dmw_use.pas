unit dmw_Use; 

interface

uses
  Windows,Math,OTypes;

const
  fl_del  = 1;
  fl_cl   = 2;
  fl_mf   = 4;
  fl_hf   = 8;
  fl_draw = 16;
  fl_skip = 64;

  fl_xyz  = 128;

  ctrl_init  = 0;
  ctrl_start = 1;
  ctrl_quest = 2;
  ctrl_exec  = 3;
  ctrl_setup = 4;
  ctrl_dial  = 5;
  ctrl_about = 6;
  ctrl_reset = 7;

  opt_load   = 8;
  opt_save   = 9;

  ctrl_auto  = 10;
  ctrl_call  = 11;

  ini_load   = 12;
  ini_save   = 13;

  ini_lock   = 14;
  dbf_lock   = 15;

  ctrl_pair  = 16;
  ctrl_up    = 17; 

  ctrl_close = 18;
  ctrl_Batch = 19;
  pair_Batch = 20;

  mode_quest = 64;
  mode_auto  = 128;
  mode_pkg   = 256;

  mr_exec   = 1;
  mr_hide   = 2;
  mr_cancel = 4;
  mr_setup  = 8;
  mr_pair   = 16;

  mr_move  = 1;
  mr_del   = 2;
  mr_swap  = 4;

  undo_mov = 0;
  undo_add = 1;
  undo_del = 2;

type
  PPool = ^TPool;
  TPool = record Len: word; Buf: tbytes end;

  dm_Hdr = record
    ver,pps,scale,elp,sys,prj,rus,fot: byte;
    y_get,y_upd,y_mag,y_pab,dm_scale: Integer;
    b1,b2,l1: double; nom,idt,obj: string;
    loc,st_loc,own,access: string;
  end;

  dm_Rec = record
    Code,mind,hind: longint; Tag,View: byte;
    Color: word; ox1,oy1,ox2,oy2: longint
  end;

  Id_Tag = (_byte,_word,_int,_long,_time,
            _date,_float,_real,_angle,_string,
            _dBase,_enum,_bool,_link,_double,
            _unicode,_list,_text,_label);

  Id_Set = set of Id_Tag;

  TOnObject = function(p: Integer;
                       const dRec: dm_Rec): Boolean of object;

  TStepItProc = function(IsBreak: Boolean): Boolean; stdcall;

  TBatchProc = function(cp: PIntegers; Count: Integer;
                        StepIt: TStepItProc): Integer; stdcall;

  TBatchPair = function(cp1,cp2: PIntegers; Count: Integer;
                        StepIt: TStepItProc): longint; stdcall;
                        
  pcn_rec = ^tcn_rec;
  tcn_rec = record
    rcid: Cardinal;
    rver: Word;
    rcnm: Byte;
    bits: Byte;

    fe: Integer;

    case Integer of
  0:  (Pos: LPoint;
       zpos: Integer);

  1:  (VPos: VPoint);

  2:  (vc1: Integer;
       vc2: Integer;
       sgp: Integer);

  3:  (OBJL: Cardinal;
       mfp: Cardinal;
       hfp: Cardinal);
  end;

const
  Id_Int: Id_Set = [_byte,_word,_int,_long,_time,_date,
                    _dBase,_enum,_bool,_link];

  Id_Real: Id_Set = [_float,_real,_angle,_double];

// 911,914 - ��������� ���������
// 912 - ��������� ����������
// 913 - ������� ���������

function dm_Open(path: pChar; edit: boolean): word; stdcall;
procedure dm_Done; stdcall;
procedure dm_File; stdcall;

function alt_Open(Path: PChar; rw: Boolean): Boolean; stdcall;
procedure alt_Done; stdcall;
procedure alt_Swap; stdcall;

function dmx_Get_Ellipsoid: Integer; stdcall;
function dmx_Get_Projection: Integer; stdcall;
function dmx_Get_Bound(G: PGPoly): Integer; stdcall;

procedure dm_Get_dm_Hdr(var Hdr: dm_Hdr); stdcall;
procedure dm_Put_dm_Hdr(var Hdr: dm_Hdr); stdcall;

function dm_Objects_Path(Path,Ext: pChar): PChar; stdcall;

procedure dm_Execute(Tool: TOnObject); stdcall;

procedure dm_GetRecord(var dRec: dm_Rec); stdcall;
procedure dm_PutRecord(var dRec: dm_Rec); stdcall;

function dm_Object: longint; stdcall;
function dm_Goto_Root: longint; stdcall;

function dm_Goto_left: boolean; stdcall;
function dm_Goto_right: boolean; stdcall;
function dm_Goto_upper: boolean; stdcall;
function dm_Goto_down: boolean; stdcall;

function dm_Goto_home: longint; stdcall;
function dm_Goto_last: longint; stdcall;
function dm_Goto_node(p: longint): boolean; stdcall;
procedure dm_Jump_node(p: longint); stdcall;

function dm_Jump_id(id: longint): longint; stdcall;

procedure dm_Get_Position(out pos: LPoint); stdcall;
procedure dm_Set_Position(const pos: LPoint); stdcall;

function dm_Is_Childs(p: longint): Integer; stdcall;
function dm_Mov_Childs(p1,p2: longint): Integer; stdcall;

function dm_Is_Hole(run,par: longint): Boolean; stdcall;

function dm_Get_Holes(par: longint;
                      Holes: PIntegers;
                      MaxCount: Integer): Integer; stdcall;

function dm_Get_Word(n,bln: word; var w: word): boolean; stdcall;
function dm_Get_Int(n,bln: word; var i: SmallInt): boolean; stdcall;
function dm_Get_Long(n,bln: word; var i: longint): boolean; stdcall;
function dm_Get_Real(n,bln: word; var r: float): boolean; stdcall;
function dm_Get_Angle(n,bln: word; var r: float): boolean; stdcall;
function dm_Get_String(n,len: word; var str): boolean; stdcall;

procedure dm_Put_Byte(n: word; b: byte); stdcall;
procedure dm_Put_Word(n: word; w: word); stdcall;
procedure dm_Put_Enum(n: word; w: word); stdcall;
procedure dm_Put_dBase(n: word; w: word); stdcall;
procedure dm_Put_Int(n: word; i: SmallInt); stdcall;
procedure dm_Put_Long(n: word; i: longint); stdcall;
procedure dm_Put_Real(n: word; r: float); stdcall;
procedure dm_Put_Angle(n: word; r: float); stdcall;
procedure dm_Put_Time(n: word; t: longint); stdcall;
procedure dm_Put_Date(n: word; d: longint); stdcall;
procedure dm_Put_String(n: word; s: pChar); stdcall;
procedure dm_Put_Rus(n: Integer; s: PChar); stdcall;

function dm_Assign_Index(p: longint): longint; stdcall;

procedure dm_Del_hf(n: word; id: Id_Tag); stdcall;

function dm_Get_hf_Pool(lp: pPool; max: word): word; stdcall;
procedure dm_Put_hf_Pool(lp: pPool); stdcall;

function dm_hf_Pool_Get(lp: pPool; ind: word;
  var nn: word; var id: Id_Tag; info: PBytes): word; stdcall;

function dm_hf_Pool_Put(lp: pPool; size: word;
  nn: word; id: Id_Tag; info: PBytes): word; stdcall;

procedure dm_hf_Pool_Delete(hf: PPool; nn: word; id: Id_Tag);
stdcall;

function dm_Find_Frst_Code(code: longint; loc: byte): longint; stdcall;
function dm_Find_Next_Code(code: longint; loc: byte): longint; stdcall;
function dm_Find_Next_Object(Code: longint; Loc: byte): longint; stdcall;

function dmx_Find_Frst_Code(Code,Loc: Integer): Integer; stdcall;
function dmx_Find_Next_Code(Code,Loc: Integer): Integer; stdcall;

procedure dm_Find_Char_Is_Words(Value: Boolean); stdcall;

function dm_Find_Frst_Char(nn: word; id: Id_Tag; i: longint;
                           f: float; s: pChar): longint; stdcall;

function dm_Find_Next_Char(nn: word; id: Id_Tag; i: longint;
                           f: float; s: pChar): longint; stdcall;

function dm_Find_Frst_Link_Point: longint; stdcall;
function dm_Find_Next_Link_Point: longint; stdcall;

procedure dm_Get_Bound(var a,b: lpoint); stdcall;
procedure dm_Set_Bound(a,b: lpoint); stdcall;

function dm_Port_Contains_Bound(x1,y1,x2,y2: longint): boolean;
stdcall;

function dm_Get_Poly_Count: SmallInt; stdcall;
function dm_Get_Poly_Buf(lp: pLLine; max: word): SmallInt; stdcall;
function dm_Set_Poly_Buf(lp: pLLine): SmallInt; stdcall;

function dm_z_res: Integer; stdcall;

function dm_Get_Poly_xyz(lp: PLLine; hp: PIntegers;
                         lp_Max: Integer): Integer; stdcall;

function dm_Set_xyz(lp: pLLine; hp: PIntegers): Integer; stdcall;

function dm_Height_Object(p: Integer): double; stdcall;
function dm_Get_Height(p: Integer; out h: double): Boolean; stdcall;
procedure dm_Put_Height(p: Integer; h: double); stdcall;
function dm_Delete_Height(p: Integer): Boolean; stdcall;
function dm_Trunc_z_axe(p: Integer): Boolean; stdcall;
function dm_Up_z_axe(p: Integer): Boolean; stdcall;

function dm_Get_Color: word; stdcall;
procedure dm_Set_Color(cl: word); stdcall;

function dm_Get_Code: longint; stdcall;
procedure dm_Set_Code(code: longint); stdcall;

function dm_Get_Local: byte; stdcall;
procedure dm_Set_Local(loc: byte); stdcall;

function dm_Get_Tag: byte; stdcall;
procedure dm_Set_Tag(t: byte); stdcall;

procedure dm_Set_Flag(fl: byte; up: boolean); stdcall;

function dm_Get_Flags: byte; stdcall;
procedure dm_Set_Flags(flags: byte); stdcall;

function dm_Get_Level: byte; stdcall;
procedure dm_Set_Level(lev: byte); stdcall;

function dm_Add_Layer(code: longint; v: word): longint;
stdcall;

function dm_Add_Sign(c: longint; a,b: lpoint; v: word;
                     down: boolean): longint;
stdcall;

function dm_Add_Poly(c: longint; loc: byte; v: word; lp: pLLine;
                     down: boolean): longint;
stdcall;

function dm_Add_Text(c: longint; loc: byte; v: word; lp: pLLine;
                     s: pChar; down: boolean): longint;
stdcall;

function dm_Add_Rect(c: longint; a,b: lpoint;
                     v: word; down: boolean): longint;
stdcall;

function dm_Move_Object(prd,mov: longint; down: boolean): longint;
stdcall;

function dm_Del_Object(prd,run: longint): longint;
stdcall;

function dm_Delete_Object(prd,run: longint): longint;
stdcall;

function dm_Next_Object(run: longint): longint; stdcall;
function dm_Pred_Object(prd,run: longint): longint; stdcall;
function dm_Dup_Object(run: longint): longint; stdcall;

function dm_Id_Object(run: longint): longint; stdcall;
function dm_Id_Offset(id: longint): longint; stdcall;

procedure dm_Cls_Parent(par: longint); stdcall;
procedure dm_Up_Childs(par: longint); stdcall;
procedure dm_Delete_Childs(par: longint); stdcall;

procedure dm_L_to_G(lx,ly: longint; var gx,gy: double); stdcall;
procedure dm_G_to_L(gx,gy: double; var lx,ly: longint); stdcall;

procedure dm_to_sm(ix,iy: longint; var ox,oy: longint);

function dm_Get_Angle_Length(a,b: lpoint; var f: double): double;
stdcall;

procedure dm_Text_Bound(code: longint; txt: pChar;
                        lp,bp: pLLine; max,up: word); stdcall;

procedure dm_Sign_Bound(code: longint; a,b: lpoint;
                        bp: pLLine; max: word); stdcall;

function dm_This_Text_Bound(lp: pLLine; Max: Integer): Integer;
stdcall;

function dm_Get_Polygon(bp: pLLine; max: Integer): Integer; stdcall;

function dm_Ind_Blank: word; stdcall;
function dm_Get_Blank(i: word; lp: pWLine; max: word): word; stdcall;

function dm_Def_Blank(i: Integer; lp: PLLine; max: Integer): Integer;
stdcall;

function dm_Get_std_hf(bln: Integer; List: PIntegers;
                       Capacity: Integer): Integer; stdcall;

function dm_Get_dbf(bln,nnn: Integer; dbf: pChar): Integer; stdcall;

function dm_Get_enum(bln,nnn,val: Integer; str: PChar): PChar; stdcall;

function dm_Get_Unicode(n: word; s: pWideChar): boolean; stdcall;
procedure dm_Put_Unicode(n: word; s: pWideChar); stdcall;

function dm_Get_Double(n,bln: word; var r: double): boolean; stdcall;
procedure dm_Put_Double(n: word; r: double); stdcall;

function obj_Get_Count: Integer; stdcall;

function obj_Get_Object(i: Integer; var Code,Loc: Integer; ps: pChar): pChar;
stdcall;

function obj_Ind_Blank(Code: longint; Loc: byte): word; stdcall;
function obj_Get_Blank(Code: longint; Loc: byte; lp: PWLine; max: word): word;
stdcall;

function obj_Get_Name(Code: longint; Loc: byte; s: pChar): pChar;
stdcall;

function idx_Get_Name(nn: Integer; s: PChar): PChar; stdcall;

function id_Parent: longint; stdcall;

function dm_Get_Pen(var mm,tag,width,style: byte): byte; stdcall;
procedure dm_Set_Pen(mm,tag,width,style,pc: byte); stdcall;

function dm_Get_Brush(var fc,pc: byte): byte; stdcall;
procedure dm_Set_Brush(fc,pc,msk: byte); stdcall;

function dmw_Get_Brush(var r,g,b: byte): byte; stdcall;
procedure dmw_Set_Brush(r,g,b,msk: byte); stdcall;

function dm_Get_Graphics(Code: longint; Loc: byte): longint; stdcall;
procedure dm_Set_Graphics(cl: longint); stdcall;
procedure dm_Restore_Graphics; stdcall;

function dmw_Get_Color: Integer; stdcall;
procedure dmw_Set_Color(cl: Integer); stdcall;

function dm_Goto_by_Code(code: longint): longint; stdcall;

function dm_Get_Layer(code: longint): longint; stdcall;

function dm_New(path,obj: PChar;

                pps,prj,elp, dx,dy,dz: Integer;  //0-plan, 1 map; 1,9;  dx,...-
                b1,b2, lc: Double;        //0,0, ..medium meridian

                xmin,ymin,xmax,ymax: double;
                ed: Integer): Integer; stdcall;     //edinitsa-10 -metrov

function dm_Frame(path,obj: PChar; pps,ed: Integer;
                  xmin,ymin,xmax,ymax: double): Integer;
stdcall;

function dm_Make(path,obj: pChar; pps: byte;
                 xmin,ymin,xmax,ymax: double): word;
stdcall;

function bl_Make(path,obj,nom: pChar): word;
stdcall;

function avia_Make(path,obj,nom: pChar): word;
stdcall;

procedure dm_Cls; stdcall;

function dm_Copy(src,dst,obj: PChar): Boolean; stdcall;

function dm_lg_transit(l,g: PGPoly): Integer; stdcall;

procedure dm_L_to_R(x,y: longint; var b,l: double); stdcall;
procedure dm_R_to_L(b,l: double; var x,y: longint); stdcall;
procedure dm_X_to_L(x,y: double; bl: Integer; var lx,ly: longint); stdcall;

function lc_zone(lc: double): word; stdcall;
function dm_zone: longint; stdcall;
function dm_pps: byte; stdcall;
function dm_prj: byte; stdcall;
function dm_elp: byte; stdcall;

procedure dm_BL_XY(b,l: double; var x,y: double); stdcall;
procedure dm_XY_BL(x,y: double; var b,l: double); stdcall;

function dm_azimuth(const a,b: LPoint;
                    out fi: Double): double; stdcall;

function dm_Contains(x,y: Double; pps: Integer): boolean; stdcall;

function dm_Filter_region(Filter: pChar): byte; stdcall;

function dm_Filter(Filter,Dst: pChar; lp: pLLine; rgn: byte): pChar;
stdcall;

function dm_Objects_Count: longint; stdcall;

procedure dm_Undo_Push(run: longint; cmd: byte); stdcall;
function dm_Undo_Pop(ins: boolean): longint; stdcall;
function dm_Undo_Hide: longint; stdcall;

function dm_Undo_Object(out Code,Loc,x1,y1,x2,y2: Integer): longint; stdcall;

function Link_Open(path: pChar): Integer; stdcall;
procedure Link_Done; stdcall;

procedure Link_pair(i: Integer; var ix,iy, gx,gy: longint); stdcall;
procedure Link_add(ix,iy, gx,gy: longint); stdcall;
function Link_Count: Integer; stdcall;

procedure dm_to_bit(ix,iy: longint; var ox,oy: longint); stdcall;
procedure bit_to_dm(ix,iy: longint; var ox,oy: longint); stdcall;

procedure link_a_to_b(ix,iy: longint; var ox,oy: longint); stdcall;
procedure link_b_to_a(ix,iy: longint; var ox,oy: longint); stdcall;

procedure dm_Bin_Dir(dir: pChar); stdcall;
function dm_Handle: Integer; stdcall;
procedure dm_AutoSave(Count: Integer); stdcall;
function dm_Update: Integer; stdcall;
function dm_Scale: Integer; stdcall;

function dm_Object_Icon(Code,Loc: Integer): HBitMap; stdcall;

procedure dm_Polygon_Square(Value: Boolean); stdcall;

function dm_cn_to_dm(p: Integer): Boolean; stdcall;

function batch_cn_to_dm(cp: PIntegers; Count: Integer;
                        StepIt: TStepItProc): Integer; stdcall;

function dm_is_cn: Boolean; stdcall;
// true - ���� ���� ������������ �������-������� ���������

function dm_Get_vc_Count: Integer; stdcall;
// ������� �����

function dm_Get_vc_Id(I: Integer): Integer; stdcall;
// ������������� ����

function dm_Get_vc(Id: Cardinal; out v: tcn_rec): Boolean;
stdcall; // ����

function dm_Get_ve_Count: Integer; stdcall;
// ������� �����

function dm_Get_ve_Id(I: Integer): Integer; stdcall;
// ������������� �����

function dm_Get_ve(Id: Cardinal; out v: tcn_rec): Boolean;
stdcall; // �����

function dm_Get_ve_Poly(Id: Cardinal;
                        lp: PLLine; hp: PIntegers;
                        lp_Max: Integer): Integer; stdcall;
// ������� ����

function dm_Get_vc_ref(Id: Cardinal; vc: PIntegers;
                       vc_Max: Integer): Integer; stdcall;
// ������ ����� �� ����

function dm_Get_ve_ref(Id: Cardinal; ve: PIntegers;
                       ve_Max: Integer): Integer; stdcall;
// ������ �������� �� �����

function dm_Get_vi_ref(Id: Cardinal; vi: PIntegers;
                       vi_Max: Integer): Integer; stdcall;
// ������ �������� �� ����

function dm_add_vc(Id: Cardinal; X,Y,Z: Integer): Cardinal;
stdcall; // �������� ����

function dm_add_ve(Id, Vc1,Vc2: Cardinal; lp: PLLine;
                   hp: PIntegers): Cardinal; stdcall;
// �������� �����

function dm_Delete_vc(Id: Cardinal): Boolean; stdcall;
// ������� ����
function dm_Delete_ve(Id: Cardinal): Boolean; stdcall;
// ������� �����

procedure dm_Update_vc(Id: Cardinal; X,Y,Z: Integer); stdcall;
// �������� ����

procedure dm_Update_ve(Id, Vc1,Vc2: Cardinal;
                       lp: PLLine; hp: PIntegers); stdcall;
// �������� �����

function dm_Get_fc_Count: Integer; stdcall;
// ������� ���������

function dm_Get_fc_Id(I: Integer): Integer; stdcall;
// ������������� ���������

function dm_Get_fc(Id: Cardinal; out v: tcn_rec): Boolean;
stdcall; // ���������

function dm_fc_Get_code(Id: Cardinal): Integer; stdcall;
// �������� ��� ���������

function dm_fc_Get_list(Id: Cardinal;
                        lp: PLLine; lp_Max: Integer): Integer;
stdcall;
// �������� ������ ���������
// x - ������������� �������
// y - ��� �������������, ��� �����

procedure dm_fc_Update(Id, OBJL: Cardinal; lp: PLLine); stdcall;
// �������� ���������
// OBJL - ���
// lp - ������

function dm_fc_Get_hf(Id: Cardinal; lp: PPool;
                      BufSize: Integer): Integer; stdcall;
// �������� ��������� ���������

procedure dm_fc_Put_hf(Id: Cardinal; lp: PPool); stdcall;
// �������� ��������� ���������

function dm_lp_fill(Items: PInt64s;
                    Count: Integer;

                    lp: PLLine; lp_max: Integer;
                    const p, lt,rb: LPoint;

                    cut: Boolean): Integer; stdcall;

function dm_Size: tgauss;
function dm_Resolution: double;
function dm_dist(d: double; ed: Integer): double;
function dm_res_m: Integer;

function StrToCode(s: string): longint;
function CodeToStr(code: longint): string;

function cn_ptr(rcid,rcnm,ornt,usag,mask: Integer): TPoint;

procedure dm_DrawRect(DC: hDC; dc_w,dc_h: Integer;
                      dc_x1,dc_y1,dc_x2,dc_y2: Integer;
                      vx1,vy1,vx2,vy2: Integer; dmp: PChar);
stdcall;

implementation

uses
  Sysutils;

const
  dll_dm = 'dll_dm.dll';

function dm_Open(path: pChar; edit: boolean): word;
external dll_dm index 1;

procedure dm_Done; external dll_dm index 2;
procedure dm_File; external dll_dm index 3;

function alt_Open(Path: PChar; rw: Boolean): Boolean;
external dll_dm;

procedure alt_Done; external dll_dm;
procedure alt_Swap; external dll_dm;

function dmx_Get_Ellipsoid: Integer; external dll_dm;
function dmx_Get_Projection: Integer; external dll_dm;
function dmx_Get_Bound(G: PGPoly): Integer; external dll_dm;

procedure dm_Get_dm_Hdr(var Hdr: dm_Hdr); external dll_dm index 4;
procedure dm_Put_dm_Hdr(var Hdr: dm_Hdr); external dll_dm index 5;

function dm_Objects_Path(Path,Ext: pChar): pChar;
external dll_dm index 6;

procedure dm_Execute(Tool: TOnObject); external dll_dm index 7;

procedure dm_GetRecord(var dRec: dm_Rec); external dll_dm index 8;
procedure dm_PutRecord(var dRec: dm_Rec); external dll_dm index 9;

function dm_Object: longint; external dll_dm index 10;
function dm_Goto_Root: longint; external dll_dm index 11;
function dm_Goto_left: boolean;  external dll_dm index 12;
function dm_Goto_right: boolean; external dll_dm index 13;
function dm_Goto_upper: boolean; external dll_dm index 14;
function dm_Goto_down: boolean;  external dll_dm index 15;
function dm_Goto_home: longint;  external dll_dm index 16;
function dm_Goto_last: longint;  external dll_dm index 17;
function dm_Goto_node(p: longint): boolean; external dll_dm index 18;
procedure dm_Jump_node(p: longint); external dll_dm index 19;

function dm_Jump_id(id: longint): longint; external dll_dm;

procedure dm_Get_Position(out pos: LPoint); external dll_dm;
procedure dm_Set_Position(const pos: LPoint); external dll_dm;

function dm_Is_Childs(p: longint): Integer; external dll_dm;
function dm_Mov_Childs(p1,p2: longint): Integer; external dll_dm;

function dm_Is_Hole(run,par: longint): Boolean; external dll_dm;

function dm_Get_Holes(par: longint;
                      Holes: PIntegers;
                      MaxCount: Integer): Integer; external dll_dm;

function dm_Get_Word(n,bln: word; var w: word): boolean;
external dll_dm index 20;
function dm_Get_Int(n,bln: word; var i: SmallInt): boolean;
external dll_dm index 21;
function dm_Get_Long(n,bln: word; var i: longint): boolean;
external dll_dm index 22;
function dm_Get_Real(n,bln: word; var r: float): boolean;
external dll_dm index 23;
function dm_Get_Angle(n,bln: word; var r: float): boolean;
external dll_dm index 24;
function dm_Get_String(n,len: word; var str): boolean;
external dll_dm index 25;

procedure dm_Put_Byte(n: word; b: byte); external dll_dm index 26;
procedure dm_Put_Word(n: word; w: word); external dll_dm index 27;
procedure dm_Put_Enum(n: word; w: word); external dll_dm index 28;
procedure dm_Put_dBase(n: word; w: word); external dll_dm index 29;
procedure dm_Put_Int(n: word; i: SmallInt); external dll_dm index 30;
procedure dm_Put_Long(n: word; i: longint); external dll_dm index 31;
procedure dm_Put_Real(n: word; r: float); external dll_dm index 32;
procedure dm_Put_Angle(n: word; r: float); external dll_dm index 33;
procedure dm_Put_Time(n: word; t: longint); external dll_dm index 34;
procedure dm_Put_Date(n: word; d: longint); external dll_dm index 35;
procedure dm_Put_String(n: word; s: pChar); external dll_dm index 36;

procedure dm_Put_Rus(n: Integer; s: PChar); external dll_dm;

function dm_Assign_Index(p: longint): longint; external dll_dm;

procedure dm_Del_hf(n: word; id: Id_Tag); external dll_dm;

function dm_Get_hf_Pool(lp: pPool; max: word): word;
external dll_dm index 37;
procedure dm_Put_hf_Pool(lp: pPool);
external dll_dm index 38;

function dm_hf_Pool_Get(lp: pPool; ind: word;
                        var nn: word; var id: Id_Tag; info: pBytes): word;
external dll_dm index 39;

function dm_hf_Pool_Put(lp: pPool; size: word;
                        nn: word; id: Id_Tag; info: pBytes): word;
external dll_dm index 40;

procedure dm_hf_Pool_Delete(hf: PPool; nn: word; id: Id_Tag);
external dll_dm;

function dm_Find_Frst_Code(code: longint; loc: byte): longint;
external dll_dm index 41;
function dm_Find_Next_Code(code: longint; loc: byte): longint;
external dll_dm index 42;

function dm_Find_Next_Object(Code: longint; Loc: byte): longint;
external dll_dm;

function dmx_Find_Frst_Code(Code,Loc: Integer): Integer;
external dll_dm;

function dmx_Find_Next_Code(Code,Loc: Integer): Integer;
external dll_dm;

procedure dm_Find_Char_Is_Words(Value: Boolean);
external dll_dm;

function dm_Find_Frst_Char(nn: word; id: Id_Tag; i: longint;
                           f: float; s: pChar): longint;
external dll_dm index 43;

function dm_Find_Next_Char(nn: word; id: Id_Tag; i: longint;
                           f: float; s: pChar): longint;
external dll_dm index 44;

function dm_Find_Frst_Link_Point: longint; external dll_dm;
function dm_Find_Next_Link_Point: longint; external dll_dm;

procedure dm_Get_Bound(var a,b: lpoint); external dll_dm index 50;
procedure dm_Set_Bound(a,b: lpoint); external dll_dm index 51;

function dm_Port_Contains_Bound(x1,y1,x2,y2: longint): boolean;
external dll_dm;

function dm_Get_Poly_Count: SmallInt;
external dll_dm index 52;
function dm_Get_Poly_buf(lp: pLLine; max: word): SmallInt;
external dll_dm index 53;
function dm_Set_Poly_Buf(lp: pLLine): SmallInt;
external dll_dm index 54;

function dm_z_res: Integer; external dll_dm;

function dm_Get_Poly_xyz(lp: pLLine; hp: PIntegers;
                         lp_Max: Integer): Integer;
external dll_dm;

function dm_Set_xyz(lp: pLLine; hp: PIntegers): Integer;
external dll_dm;

function dm_Height_Object(p: Integer): double; external dll_dm;
function dm_Get_Height(p: Integer; out h: double): Boolean; external dll_dm;
procedure dm_Put_Height(p: Integer; h: double); external dll_dm;
function dm_Delete_Height(p: Integer): Boolean; external dll_dm;
function dm_Trunc_z_axe(p: Integer): Boolean; external dll_dm;
function dm_Up_z_axe(p: Integer): Boolean; external dll_dm;

function dm_Get_Color: word; external dll_dm index 55;
procedure dm_Set_Color(cl: word); external dll_dm index 56;

function dm_Get_Code: longint; external dll_dm index 57;
procedure dm_Set_Code(code: longint); external dll_dm index 58;

function dm_Get_Local: byte; external dll_dm index 59;
procedure dm_Set_Local(loc: byte); external dll_dm index 60;

function dm_Get_Tag: byte; external dll_dm index 61;
procedure dm_Set_Tag(t: byte); external dll_dm index 62;
procedure dm_Set_Flag(fl: byte; up: boolean); external dll_dm index 63;

function dm_Get_Flags: byte; external dll_dm;
procedure dm_Set_Flags(flags: byte); external dll_dm;

function dm_Get_Level: byte; external dll_dm;
procedure dm_Set_Level(lev: byte); external dll_dm;

function dm_Add_Layer(code: longint; v: word): longint;
external dll_dm index 71;

function dm_Add_Sign(c: longint; a,b: lpoint; v: word;
                     down: boolean): longint;
external dll_dm index 72;

function dm_Add_Poly(c: longint; loc: byte; v: word; lp: pLLine;
                     down: boolean): longint;
external dll_dm index 73;

function dm_Add_Text(c: longint; loc: byte; v: word; lp: pLLine;
                     s: pChar; down: boolean): longint;
external dll_dm index 74;

function dm_Add_Rect(c: longint; a,b: lpoint;
                     v: word; down: boolean): longint;
external dll_dm;

function dm_Move_Object(prd,mov: longint; down: boolean): longint;
external dll_dm index 75;

function dm_Del_Object(prd,run: longint): longint;
external dll_dm index 76;

function dm_Delete_Object(prd,run: longint): longint; external dll_dm;

function dm_Next_Object(run: longint): longint; external dll_dm;
function dm_Pred_Object(prd,run: longint): longint; external dll_dm;

function dm_Dup_Object(run: longint): longint; external dll_dm;

function dm_Id_Object(run: longint): longint; external dll_dm;
function dm_Id_Offset(id: longint): longint; external dll_dm;

procedure dm_Cls_Parent(par: longint); external dll_dm;
procedure dm_Up_Childs(par: longint); external dll_dm;
procedure dm_Delete_Childs(par: longint); external dll_dm;

procedure dm_L_to_G(lx,ly: longint; var gx,gy: double);
external dll_dm index 81;
procedure dm_G_to_L(gx,gy: double; var lx,ly: longint);
external dll_dm index 82;

procedure dm_to_sm(ix,iy: longint; var ox,oy: longint);
var
  g: tgauss;
begin
  dm_L_to_G(ix,iy,g.x,g.y);
  ox:=Round(g.x*100); oy:=Round(g.y*100)
end;

function dm_Get_Angle_Length(a,b: lpoint; var f: double): double;
external dll_dm index 83;

procedure dm_Text_Bound(code: longint; txt: pChar;
                        lp,bp: pLLine; max,up: word);
external dll_dm index 91;

procedure dm_Sign_Bound(code: longint; a,b: lpoint;
                        bp: pLLine; max: word);
external dll_dm index 92;

function dm_This_Text_Bound(lp: pLLine; Max: Integer): Integer;
external dll_dm;

function dm_Get_Polygon(bp: pLLine; max: Integer): Integer;
external dll_dm;

function dm_Ind_Blank: word;
external dll_dm index 93;

function dm_Get_Blank(i: word; lp: PWLine; max: word): word;
external dll_dm index 94;

function dm_Def_Blank(i: Integer; lp: PLLine; max: Integer): Integer;
external dll_dm;

function dm_Get_std_hf(bln: Integer; List: PIntegers;
                       Capacity: Integer): Integer;
external dll_dm;

function dm_Get_dbf(bln,nnn: Integer; dbf: pChar): Integer;
external dll_dm;

function dm_Get_enum(bln,nnn,val: Integer; str: PChar): PChar;
external dll_dm;

function dm_Get_Unicode(n: word; s: pWideChar): boolean;
external dll_dm;

procedure dm_Put_Unicode(n: word; s: pWideChar);
external dll_dm;

function dm_Get_Double(n,bln: word; var r: double): boolean;
external dll_dm;

procedure dm_Put_Double(n: word; r: double);
external dll_dm;

function obj_Get_Count: Integer;
external dll_dm;

function obj_Get_Object(i: Integer; var Code,Loc: Integer; ps: pChar): pChar;
external dll_dm;

function obj_Ind_Blank(Code: longint; Loc: byte): word;
external dll_dm;

function obj_Get_Blank(Code: longint; Loc: byte; lp: PWLine; max: word): word;
external dll_dm;

function obj_Get_Name(Code: longint; Loc: byte; s: pChar): pChar;
external dll_dm;

function idx_Get_Name(nn: Integer; s: PChar): PChar; 
external dll_dm;

function id_Parent: longint; external dll_dm;

function dm_Get_Pen(var mm,tag,width,style: byte): byte; external dll_dm;
procedure dm_Set_Pen(mm,tag,width,style,pc: byte); external dll_dm;

function dm_Get_Brush(var fc,pc: byte): byte; external dll_dm;
procedure dm_Set_Brush(fc,pc,msk: byte); external dll_dm;

function dmw_Get_Brush(var r,g,b: byte): byte; external dll_dm;
procedure dmw_Set_Brush(r,g,b,msk: byte); external dll_dm;

function dm_Get_Graphics(Code: longint; Loc: byte): longint; external dll_dm;
procedure dm_Set_Graphics(cl: longint); external dll_dm;
procedure dm_Restore_Graphics; external dll_dm;

function dmw_Get_Color: Integer; external dll_dm;
procedure dmw_Set_Color(cl: Integer); external dll_dm;

function dm_Goto_by_Code(code: longint): longint; external dll_dm;

function dm_Get_Layer(code: longint): longint; external dll_dm;

function dm_New(path,obj: PChar;

                pps,prj,elp, dx,dy,dz: Integer;   // 0-plan, 1 map; 1,9;  dx,...-
                b1,b2, lc: Double;                // 0,0, ..medium meridian

                xmin,ymin,xmax,ymax: double;
                ed: Integer): Integer; external dll_dm;  // edinitsa-10 -metrov

function dm_Frame(path,obj: PChar; pps,ed: Integer;
                  xmin,ymin,xmax,ymax: double): Integer;
external dll_dm;

function dm_Make(path,obj: pChar; pps: byte;
                 xmin,ymin,xmax,ymax: double): word;
external dll_dm;

function bl_Make(path,obj,nom: pChar): word;
external dll_dm;

function avia_Make(path,obj,nom: PChar): word;
external dll_dm;

procedure dm_Cls; external dll_dm;

function dm_Copy(src,dst,obj: PChar): Boolean; external dll_dm;

function dm_lg_transit(l,g: PGPoly): Integer; external dll_dm;

procedure dm_L_to_R(x,y: longint; var b,l: double); external dll_dm;
procedure dm_R_to_L(b,l: double; var x,y: longint); external dll_dm;

procedure dm_X_to_L(x,y: double; bl: Integer; var lx,ly: longint);
external dll_dm;

function lc_zone(lc: double): word; external dll_dm;
function dm_zone: longint; external dll_dm;
function dm_pps: byte; external dll_dm;
function dm_prj: byte; external dll_dm;
function dm_elp: byte; external dll_dm;

procedure dm_BL_XY(b,l: double; var x,y: double);
external dll_dm;

procedure dm_XY_BL(x,y: double; var b,l: double);
external dll_dm;

function dm_azimuth(const a,b: LPoint;
                    out fi: Double): double;
external dll_dm;

function dm_Contains(x,y: Double; pps: Integer): boolean;
external dll_dm;

function dm_Filter_region(Filter: pChar): byte;
external dll_dm;

function dm_Filter(Filter,Dst: pChar; lp: pLLine; rgn: byte): pChar;
external dll_dm;

function dm_Objects_Count: longint;
external dll_dm;

procedure dm_Undo_Push(run: longint; cmd: byte); external dll_dm;
function dm_Undo_Pop(ins: boolean): longint; external dll_dm;
function dm_Undo_Hide: longint; external dll_dm;

function dm_Undo_Object(out Code,Loc,x1,y1,x2,y2: Integer): longint;
external dll_dm;

function Link_Open(path: pChar): Integer; external dll_dm;
procedure Link_Done; external dll_dm;

procedure Link_pair(i: Integer; var ix,iy, gx,gy: longint);
external dll_dm;
procedure Link_add(ix,iy, gx,gy: longint); external dll_dm;
function Link_Count: Integer; external dll_dm;

procedure dm_to_bit(ix,iy: longint; var ox,oy: longint); external dll_dm;
procedure bit_to_dm(ix,iy: longint; var ox,oy: longint); external dll_dm;

procedure link_a_to_b(ix,iy: longint; var ox,oy: longint); external dll_dm;
procedure link_b_to_a(ix,iy: longint; var ox,oy: longint); external dll_dm;

procedure dm_Bin_Dir(dir: pChar); external dll_dm;
function dm_Handle: Integer; external dll_dm;
procedure dm_AutoSave(Count: Integer); external dll_dm;
function dm_Update: Integer; external dll_dm;

function dm_Scale: Integer; external dll_dm;

function dm_Object_Icon(Code,Loc: Integer): HBitMap; external dll_dm;

procedure dm_Polygon_Square(Value: Boolean); external dll_dm;

function dm_cn_to_dm(p: Integer): Boolean; external dll_dm;

function batch_cn_to_dm(cp: PIntegers; Count: Integer;
                        StepIt: TStepItProc): Integer;
external dll_dm;

function dm_is_cn: Boolean; external dll_dm;

function dm_Get_vc_Count: Integer; external dll_dm;
function dm_Get_vc_Id(I: Integer): Integer; external dll_dm;
function dm_Get_vc(Id: Cardinal; out v: tcn_rec): Boolean;
external dll_dm;

function dm_Get_ve_Count: Integer; external dll_dm;
function dm_Get_ve_Id(I: Integer): Integer; external dll_dm;
function dm_Get_ve(Id: Cardinal; out v: tcn_rec): Boolean;
external dll_dm;

function dm_Get_ve_Poly(Id: Cardinal;
                        lp: PLLine; hp: PIntegers;
                        lp_Max: Integer): Integer;
external dll_dm;

function dm_Get_vc_ref(Id: Cardinal; vc: PIntegers;
                       vc_Max: Integer): Integer;
external dll_dm;

function dm_Get_ve_ref(Id: Cardinal; ve: PIntegers;
                       ve_Max: Integer): Integer;
external dll_dm;

function dm_Get_vi_ref(Id: Cardinal; vi: PIntegers;
                       vi_Max: Integer): Integer;
external dll_dm;

function dm_add_vc(Id: Cardinal; X,Y,Z: Integer): Cardinal;
external dll_dm;

function dm_add_ve(Id, Vc1,Vc2: Cardinal; lp: PLLine;
                   hp: PIntegers): Cardinal;
external dll_dm;

function dm_Delete_vc(Id: Cardinal): Boolean; external dll_dm;
function dm_Delete_ve(Id: Cardinal): Boolean; external dll_dm;

procedure dm_Update_vc(Id: Cardinal; X,Y,Z: Integer);
external dll_dm;

procedure dm_Update_ve(Id, Vc1,Vc2: Cardinal;
                       lp: PLLine; hp: PIntegers);
external dll_dm;

function dm_Get_fc_Count: Integer; external dll_dm;
function dm_Get_fc_Id(I: Integer): Integer; external dll_dm;
function dm_Get_fc(Id: Cardinal; out v: tcn_rec): Boolean;
external dll_dm;

function dm_fc_Get_code(Id: Cardinal): Integer; external dll_dm;

function dm_fc_Get_list(Id: Cardinal;
                        lp: PLLine; lp_Max: Integer): Integer;
external dll_dm;

procedure dm_fc_Update(Id, OBJL: Cardinal; lp: PLLine);
external dll_dm;

function dm_fc_Get_hf(Id: Cardinal; lp: PPool;
                      BufSize: Integer): Integer; external dll_dm;

procedure dm_fc_Put_hf(Id: Cardinal; lp: PPool); external dll_dm;

function dm_lp_fill(Items: PInt64s;
                    Count: Integer;

                    lp: PLLine; lp_max: Integer;
                    const p, lt,rb: LPoint;

                    cut: Boolean): Integer;
external dll_dm;



function dm_Size: tgauss;
var
  p1,p2: lpoint; gx1,gy1,gx2,gy2: double;
begin
  dm_Goto_Root;
  dm_Get_Bound(p1,p2);

  dm_L_to_G(p1.x,p1.y, gx1,gy1);
  dm_L_to_G(p2.x,p1.y, gx2,gy2);

  Result.x:=Hypot(gx2-gx1,gy2-gy1);

  dm_L_to_G(p1.x,p1.y, gx1,gy1);
  dm_L_to_G(p1.x,p2.y, gx2,gy2);

  Result.y:=Hypot(gx2-gx1,gy2-gy1);
end;

function dm_Resolution: double;
var
  p1,p2: lpoint; gx1,gy1,gx2,gy2,dist: double;
begin
  Result:=1;

  dm_Goto_Root;
  dm_Get_Bound(p1,p2);

  dm_L_to_G(p1.x,p1.y, gx1,gy1);
  dm_L_to_G(p2.x,p2.y, gx2,gy2);

  dist:=Hypot(p2.x-p1.x,p2.y-p1.y);

  if dist > 1 then
  Result:=Hypot(gx2-gx1,gy2-gy1)/dist;
end;

function dm_res_m: Integer;
var
  res: Double;
begin
  Result:=0; res:=dm_Resolution;
  while (Result < 3) and (res < 1) do begin
    Inc(Result); res:=res * 10
  end
end;

function dm_dist(d: double; ed: Integer): double;
var
  res: Double;
begin
  if ed = 1 then d:=d*dm_Scale/1000;
  Result:=d; res:=dm_Resolution;
  if res > 0 then Result:=d/res
end;

function AtoI(const fa; Len: Integer): Integer;
var
  a: array[0..31] of char absolute fa;
  i: Integer; sign: Boolean;
begin
  i:=0; Result:=0; sign:=false;

  if Len > 0 then begin
    while (i < Len) and (a[i] = ' ') do Inc(i);

    if a[i] = '+' then Inc(i) else
    if a[i] = '-' then begin Inc(i); sign:=true end;

    while i < Len do begin
      Result:=(Result*10)+ord(a[i])-ord('0'); Inc(i)
    end;

    if sign then Result:=-Result
  end
end;

function StrToCode(s: string): longint;
begin
  while length(s) < 8 do s:=s+'0';
  Result:=AtoI(s[1],length(s))
end;

function CodeToStr(code: longint): string;
var
  i: Integer; a: alfa;
begin
  if code < 0 then
    Result:=IntToStr(code)
  else begin
    for i:=8 downto 2 do begin
      a[i]:=Chr(code mod 10 + ord('0'));
      code:=code div 10
    end;

    code:=code + ord('0'); if code > ord('Z') then
    code:=ord('0'); a[1]:=chr(code); Result:=a
  end
end;

function cn_ptr(rcid,rcnm,ornt,usag,mask: Integer): TPoint;
begin
  Result.x:=rcid;
  Result.y:=Long_bytes(rcnm,ornt,usag,mask)
end;

procedure dm_DrawRect(DC: hDC; dc_w,dc_h: Integer;
                      dc_x1,dc_y1,dc_x2,dc_y2: Integer;
                      vx1,vy1,vx2,vy2: Integer; dmp: pChar);
external dll_dm;

//..var
//..  t: integer;
//initialization
//  t:=1;
end.


