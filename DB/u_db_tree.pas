unit u_db_tree;

//============================================================================//
interface
//============================================================================//
uses DB, SysUtils, Variants,
//     u_db_lib1;
    
     u_DB;

{
const
  FLD_CHECKED          = 'CHECKED';
  FLD_DXTREE_ID        = 'DXTREE_ID';
  FLD_DXTREE_PARENT_ID = 'DXTREE_PARENT_ID';

  FLD_SORT = 'SORT';
}

  procedure db_DeleteChildRecords (aDataset: TDataset;
                                   aParentID: integer;
                                   aKeyFieldName   : String = FLD_DXTREE_ID;
                                   aParentFieldName: String = FLD_DXTREE_PARENT_ID);

  procedure db_CheckChildRecords (aDataset: TDataset;
                                  aParentID: integer;
                                  aCheckedFieldName : String = FLD_CHECKED;
                                  aKeyFieldName     : String = FLD_DXTREE_ID;
                                  aParentFieldName  : String = FLD_DXTREE_PARENT_ID);

  procedure db_CheckDBTreeNode   (aDataset: TDataset;
                                  aID: integer;
                                  aChecked: Boolean;
                                  aCheckedFieldName : String = FLD_CHECKED;
                                  aKeyFieldName     : String = FLD_DXTREE_ID;
                                  aParentFieldName  : String = FLD_DXTREE_PARENT_ID);

  procedure db_DXTree_Move (aDataset: TDataset;
                            aTreeID: integer; aAtTop: boolean);



exports
  db_DeleteChildRecords,
  db_CheckChildRecords,
  db_DXTree_Move
  ;

  

//============================================================================//
implementation
//============================================================================//


//-------------------------------------------------------------------
procedure db_DeleteChildRecords(aDataset: TDataset;
                                aParentID: integer;
                                aKeyFieldName   : string = FLD_DXTREE_ID;
                                aParentFieldName: string = FLD_DXTREE_PARENT_ID);
//-------------------------------------------------------------------
var key_id: integer;
begin
  with aDataset do
     while Locate (aParentFieldName, aParentID, []) do
     begin
       key_id:=FieldByName(aKeyFieldName).AsInteger;
       db_DeleteChildRecords (aDataset, key_id,
                              aKeyFieldName,aParentFieldName);

       if Locate (aKeyFieldName, key_id, []) then
         Delete;
     end;
end;


//-------------------------------------------------------------------
procedure db_CheckChildRecords (aDataset: TDataset;
                                aParentID: integer;
                                aCheckedFieldName : string = FLD_CHECKED;
                                aKeyFieldName     : string = FLD_DXTREE_ID;
                                aParentFieldName  : string = FLD_DXTREE_PARENT_ID);
//-------------------------------------------------------------------
var iID: integer;
begin
  Assert(Assigned(aDataset.FindField(aCheckedFieldName)));
  Assert(Assigned(aDataset.FindField(aKeyFieldName)));
  Assert(Assigned(aDataset.FindField(aParentFieldName)));



  with aDataset do
  begin
//    DisableControls;

    while Locate (Format('%s;%s', [aParentFieldName,aCheckedFieldName]),
                  VarArrayOf([aParentID, False]), []) do
    begin
      iID:=FieldByName(aKeyFieldName).AsInteger;

      db_CheckChildRecords (aDataset,
                            iID,
                            aCheckedFieldName,
                            aKeyFieldName,
                            aParentFieldName);

      if Locate (aKeyFieldName, iID, []) then
        db_UpdateRecord (aDataset, [db_Par(FLD_CHECKED, True)]);
    end;

    if Locate (aKeyFieldName, aParentID, []) then
      db_UpdateRecord (aDataset, [db_Par(FLD_CHECKED, True)]);

//    EnableControls;
  end;
end;


//-------------------------------------------------------------------
procedure db_CheckDBTreeNode (aDataset: TDataset;
                              aID: integer;
                              aChecked: Boolean;
                              
                              aCheckedFieldName : string = FLD_CHECKED;
                              aKeyFieldName     : string = FLD_DXTREE_ID;
                              aParentFieldName  : string = FLD_DXTREE_PARENT_ID);
//-------------------------------------------------------------------

    //-------------------------------------------------------------------
    procedure DoProcessChildren (aParentID: integer);
    //-------------------------------------------------------------------
    var
      iID: integer;
    begin
        with aDataset do
        begin
      //    DisableControls;

          while Locate (Format('%s;%s', [aParentFieldName,aCheckedFieldName]),
                        VarArrayOf([aParentID, (not aChecked)]), []) do
          begin
            iID:=FieldByName(aKeyFieldName).AsInteger;

            db_UpdateRecord (aDataset, [db_Par(FLD_CHECKED, aChecked)]);

            DoProcessChildren (iID);
            //aDataset,
            //                   ,
              //                 aCheckedFieldName,
                //               aKeyFieldName,
                  //             aParentFieldName);

//            if Locate (aKeyFieldName, iID, []) then
  //            db_UpdateRecord (aDataset, [db_Par(FLD_CHECKED, aChecked)]);

          end;

       //   if Locate (aKeyFieldName, aParentID, []) then
        //    db_UpdateRecord (aDataset, [db_Par(FLD_CHECKED, aChecked)]);

      //    EnableControls;
        end;


    end;    // 

    //-------------------------------------------------------------------
    procedure DoProcessParents (aID: integer);
    //-------------------------------------------------------------------
    var
      iID,iParentID: integer;
    begin
        with aDataset do
        begin
//            if Locate (aKeyFieldNameiParentID, (not aChecked)]), []) then

            iParentID:=FieldByName(aParentFieldName).AsInteger;

            if iParentID>0 then
              if Locate (Format('%s;%s', [aKeyFieldName,aCheckedFieldName]),
                        VarArrayOf([iParentID, (not aChecked)]), []) then
              begin

                db_UpdateRecord (aDataset, [db_Par(FLD_CHECKED, aChecked)]);

                iID:=FieldByName(aKeyFieldName).AsInteger;
                DoProcessParents (iParentID);
              end;


      //    DisableControls;

     {     while Locate (Format('%s;%s', [aKeyFieldName,aCheckedFieldName]),
                        VarArrayOf([aID, (not aChecked)]), []) do
          begin
            iID:=FieldByName(aKeyFieldName).AsInteger;

            db_UpdateRecord (aDataset, [db_Par(FLD_CHECKED, aChecked)]);

            DoProcessChildren (iID);
            //aDataset,
            //                   ,
              //                 aCheckedFieldName,
                //               aKeyFieldName,
                  //             aParentFieldName);

//            if Locate (aKeyFieldName, iID, []) then
  //            db_UpdateRecord (aDataset, [db_Par(FLD_CHECKED, aChecked)]);

          end;
}


       //   if Locate (aKeyFieldName, aParentID, []) then
        //    db_UpdateRecord (aDataset, [db_Par(FLD_CHECKED, aChecked)]);

      //    EnableControls;
        end;


    end;    //


var iID: integer;
begin
  Assert(Assigned(aDataset.FindField(aCheckedFieldName)));
  Assert(Assigned(aDataset.FindField(aKeyFieldName)));
  Assert(Assigned(aDataset.FindField(aParentFieldName)));


  if aDataset.Locate (aKeyFieldName, aID, []) then
    db_UpdateRecord (aDataset, [db_Par(FLD_CHECKED, aChecked)]);


  DoProcessChildren (aID);

  aDataset.Locate (aKeyFieldName, aID, []);

  if aChecked then
    DoProcessParents (aID);

  aDataset.Locate (aKeyFieldName, aID, []);

{
  with aDataset do
  begin
//    DisableControls;

    while Locate (Format('%s;%s', [aParentFieldName,aCheckedFieldName]),
                  VarArrayOf([aParentID, False]), []) do
    begin
      iID:=FieldByName(aKeyFieldName).AsInteger;

      db_CheckChildRecords (aDataset,
                            iID,
                            aCheckedFieldName,
                            aKeyFieldName,
                            aParentFieldName);

      if Locate (aKeyFieldName, iID, []) then
        db_UpdateRecord (aDataset, [db_Par(FLD_CHECKED, aChecked)]);
    end;

    if Locate (aKeyFieldName, aParentID, []) then
      db_UpdateRecord (aDataset, [db_Par(FLD_CHECKED, aChecked)]);

//    EnableControls;
  end;
}
end;


//-----------------------------------------------------------
procedure db_DXTree_Move (aDataset: TDataset;
                        aTreeID: integer; aAtTop: boolean);
//-----------------------------------------------------------
var iTreeParentID, iSortID, iStep: integer;
begin
  with aDataset do
   if Locate(FLD_DXTREE_ID, aTreeID, []) then
  begin
    DisableControls;

    iTreeParentID:= FieldValues[FLD_DXTREE_PARENT_ID];
    iSortID      := FieldValues[FLD_SORT];
    iStep:= IIF(aAtTop, -1, 1);

    if Locate(FLD_DXTREE_PARENT_ID+';'+FLD_SORT,
                            VarArrayOf([iTreeParentID, iSortID+iStep]), []) then
    begin
      db_UpdateRecord (aDataset, [db_par(FLD_SORT, iSortID)]);
      Locate(FLD_DXTREE_ID, aTreeID, []);
      db_UpdateRecord (aDataset, [db_par(FLD_SORT, iSortID+iStep)]);
    end;

    EnableControls;
  end;
end;


{
const
  FLD_SORT = 'sort';





//-----------------------------------------------------------


//-----------------------------------------------------------
procedure db_tree_DeleteRecord (aDataset: TDataset;
                                aTreeID: integer);
//-----------------------------------------------------------
begin
  aDataset.DisableControls;

  if aDataset.Locate (FLD_DXTREE_ID, aTreeID, []) then
     aDataset.Delete;

  db_tree_DeleteChildren (aDataset, aTreeID);
  aDataset.EnableControls;
end;

//-----------------------------------------------------------
procedure db_tree_DeleteChildren (aDataset: TDataset;
                                  aTreeParentID: integer);
//-----------------------------------------------------------
var iTreeID: integer;
begin
  with aDataset do
    while Locate (FLD_DXTREE_PARENT_ID, aTreeParentID, []) do
    begin
      iTreeID:= FieldValues[FLD_DXTREE_ID];
      Delete;
      db_tree_DeleteChildren (aDataset, iTreeID);
    end;
end;


//-----------------------------------------------------------
procedure db_tree_DeleteByFieldName (aDataset: TDataset;
                                     aFieldName: string; aFieldValue: Variant);
//-----------------------------------------------------------
begin
  with aDataset do
    while Locate (aFieldName, aFieldValue, []) do
      db_tree_DeleteRecord (aDataset, FieldValues[FLD_DXTREE_ID]);
end;


}


end.
