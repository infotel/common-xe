unit u_db_ini;

interface

uses
  IniFiles,DB, cxGridDBTableView, cxGridDBBandedTableView,
  cxGridTableView, StrUtils
  ;


  procedure db_LoadFromIni(DataSet: TDataSet; aIniFileName, aSection: string);
  procedure db_SaveToIni(DataSet: TDataSet; aIniFileName, aSection: string);

  procedure cx_SaveToIni(aView: TcxGridDBTableView; aIniFileName: string; aSection:
      string = '');  overload;

  procedure cx_SaveToIni(aView: TcxGridDBBandedTableView; aIniFileName: string; aSection:
      string = '');  overload;
 // procedure cx_SaveToIni(aView: TcxGridDBBandedTableView; aIniFileName, aSection: string);

type
  TIniFileStorage = class(TIniFile)
  //  ReadString(aSection, oField.Name, oField.DisplayLabel);
  public
    function ReadString1(aSection, aFieldName, aDefault: string): string;
  end;



implementation

(*const
  DEF_COMMON_SECTION = '~';
  DEF_COMMON_CHAR = '~';
*)

// ---------------------------------------------------------------
procedure db_LoadFromIni(DataSet: TDataSet; aIniFileName, aSection: string);
// ---------------------------------------------------------------
var
  I: Integer;
  oField: TField;
begin
  with TIniFileStorage.Create (aIniFileName) do
  begin
    for I := 0 to DataSet.Fields.Count - 1 do
    begin
      oField := DataSet.Fields[i];
      oField.DisplayLabel :=ReadString1(aSection, oField.Name, oField.DisplayLabel);
    end;

    Free;
  end;
end;

// ---------------------------------------------------------------
procedure db_SaveToIni(DataSet: TDataSet; aIniFileName, aSection: string);
// ---------------------------------------------------------------
var
  I: Integer;
  oField: TField;
begin
  with TIniFile.Create (aIniFileName) do
  begin
    for I := 0 to DataSet.Fields.Count - 1 do
    begin
      oField := DataSet.Fields[i];

      if oField.Name<>oField.DisplayLabel then

    //  oField.DisplayLabel :=
        WriteString(aSection, oField.Name, oField.DisplayLabel);
    end;

    Free;
  end;
end;


// ---------------------------------------------------------------
procedure cx_SaveToIni(aView: TcxGridDBBandedTableView; aIniFileName: string; aSection:
    string = '');
// ---------------------------------------------------------------
var
  I: Integer;
  oColumn: TcxGridDBBandedColumn;
begin
  if aSection='' then
    aSection := aView.Name;


  with TIniFile.Create (aIniFileName) do
  begin
    for I := 0 to aView.ColumnCount - 1 do
    begin
      oColumn := aView.Columns[i];

      if oColumn.DataBinding.FieldName<>'' then
       if oColumn.DataBinding.FieldName<>oColumn.Caption then
        WriteString(aSection, oColumn.DataBinding.FieldName, oColumn.Caption);
    end;

    Free;
  end;
end;



// ---------------------------------------------------------------
procedure cx_SaveToIni(aView: TcxGridDBTableView; aIniFileName: string; aSection:
    string = '');
// ---------------------------------------------------------------
var
  I: Integer;
  oColumn: TcxGridDBColumn;
begin
  if aSection='' then
    aSection := aView.Name;


  with TIniFile.Create (aIniFileName) do
  begin
    for I := 0 to aView.ColumnCount - 1 do
    begin
      oColumn := aView.Columns[i];

      if oColumn.DataBinding.FieldName<>'' then
        WriteString(aSection, oColumn.DataBinding.FieldName, oColumn.Caption);
    end;

    Free;
  end;
end;



function TIniFileStorage.ReadString1(aSection, aFieldName, aDefault: string): string;
const
  DEF_COMMON_SECTION = '~';
  DEF_COMMON_CHAR = '~';
var
  s: string;
begin
  s :=ReadString(aSection, aFieldName, '');

  if LeftStr(s,1)=DEF_COMMON_CHAR then
  begin
     s := Copy(s, 2, 100);
     s :=ReadString(DEF_COMMON_SECTION, s, '');
  end;

  if s='' then
    Result := aDefault
  else
    Result := s;
end;



end.
