﻿unit XmlSerializer;

interface

uses
  SysUtils, Forms, ActiveX,
  InvokeRegistry, OPToSOAPDomConv, XmlDoc, XmlIntf, XmlDom;

type
  TXmlSerializer_ = class
  strict private
    FObjConverter : IObjConverter;
    FXmlNamespace : string;
    FXmlRootNode  : string;
    FXmlObjectNode: string;
  protected
    function _Deserialize(DeserializedObjectType: TClass;
      const Xml: string; IsFileName: Boolean): TObject;
  public
    class procedure SerializeToFile(aObj: TObject; const aXmlFileName: string);


    function Serialize(Obj: TObject): string; overload;
    function Serialize(aObj: TObject; const aXmlFileName: string): string; overload;

    function Deserialize(DeserializedObjectType: TClass;  const XmlContent: string): TObject;
    function DeserializeFromFile(DeserializedObjectType: TClass;
      const XmlFileName: string): TObject;

    property Converter    : IObjConverter read FObjConverter  write FObjConverter;

    property XmlNamespace : string        read FXmlNamespace  write FXmlNamespace;
    property XmlRootNode  : string        read FXmlRootNode   write FXmlRootNode;
    property XmlObjectNode: string        read FXmlObjectNode write FXmlObjectNode;
  end;

implementation

//------------------------------------------------------------------------------
// létrehoz egy objkektum-példányt XML-ből
//
function TXmlSerializer_._Deserialize(DeserializedObjectType: TClass;
  const Xml: string; IsFileName: Boolean): TObject;
var
  XmlDoc  : TXMLDocument;
  Conv    : IObjConverter;
  RootNode: IXMLNode;
begin
  Result := nil;

  XmlDoc := TXMLDocument.Create(Application);
  try
    if IsFileName then
      XmlDoc.LoadFromFile(Xml)
    else
      XmlDoc.XML.Text := Xml;

    XmlDoc.Active := True;

    // XML ellenőrzése
    RootNode := XmlDoc.DocumentElement;
    if not Assigned(RootNode) or (RootNode.ChildNodes.Count = 0) then
      raise Exception.Create('Invalid XML format.');

    // konverter
    if Assigned(Converter) then
      Conv := Converter
    else
      Conv := TSOAPDomConv.Create(nil) as IObjConverter;

    RootNode := XmlDoc.DocumentElement.ChildNodes[0];

    // objektum létrehozása
    Result := DeserializedObjectType.Create;
    try
      Conv.InitObjectFromSOAP(Result, RootNode, RootNode);
    except
      FreeAndNil(Result);
      raise;
    end;
  finally
    XmlDoc.Free;
  end;
end;

//------------------------------------------------------------------------------
// létrehoz egy objkektum-példányt XML tartalomból
//
function TXmlSerializer_.Deserialize(DeserializedObjectType: TClass;
  const XmlContent: string): TObject;
begin
  Result := _Deserialize(DeserializedObjectType, XmlContent, False);
end;

//------------------------------------------------------------------------------
// létrehoz egy objkektum-példányt XML fájlból
//
function TXmlSerializer_.DeserializeFromFile(DeserializedObjectType: TClass;
  const XmlFileName: string): TObject;
begin
  Result := _Deserialize(DeserializedObjectType, XmlFileName, True);
end;

//------------------------------------------------------------------------------
// szerializálja egy objektum published tagjait egy XML fájlba
//
function TXmlSerializer_.Serialize(aObj: TObject; const aXmlFileName: string): string;
var
  RefId      : InvString;
  oXmlDoc     : TXMLDocument;
  RootNode   : IXMLNode;
  vConv       : IObjConverter;
  XNamespace : string;
  XRootNode  : string;
  XObjectNode: string;
begin
  Result := EmptyStr;

  oXmlDoc := TXMLDocument.Create(Application);
  try
    oXmlDoc.Active  := True;
    oXmlDoc.Options := oXmlDoc.Options + [doNodeAutoIndent];

    // konverter
    if Assigned(Converter) then
      vConv := Converter
    else
      vConv := TSOAPDomConv.Create(nil) as IObjConverter;

    // XML névtér
    if XmlNamespace = EmptyStr then
      XNamespace := SXMLNamespaceURI
    else
      XNamespace := XmlNamespace;

    // gyökérelem
    if XRootNode = EmptyStr then
      XRootNode := 'Root'
    else
      XRootNode := XmlRootNode;

    RootNode := oXmlDoc.AddChild(XRootNode);

    // az objektumot leíró csomópont az XML-ben
    if XmlObjectNode = EmptyStr then
      XObjectNode := aObj.ClassName
    else
      XObjectNode := XmlObjectNode;

    // szerializáció SOAP XML formátumba
    vConv.ObjInstanceToSOAP(aObj, RootNode, RootNode,
           XObjectNode, XNamespace, XNamespace, [], RefId);


//    function  ObjInstanceToSOAP(
//.    nstance: TObject; RootNode, ParentNode: IXMLNode;
//       const NodeName, NodeNamespace, ChildNamespace: InvString;
//       ObjConvOpts: TObjectConvertOptions;
//       out RefID: InvString): IXMLNode;


    Result := oXmlDoc.XML.Text;

    // mentés fájlba
    if aXmlFileName <> EmptyStr then
      oXmlDoc.SaveToFile(aXmlFileName);
  finally
    oXmlDoc.Free;
  end;
end;

//------------------------------------------------------------------------------
// szerializálja egy objektum published tagjait XML-be
//
function TXmlSerializer_.Serialize(Obj: TObject): string;
begin
  Result:= Serialize(Obj, EmptyStr);
end;


class procedure TXmlSerializer_.SerializeToFile(aObj: TObject; const
    aXmlFileName: string);
var
  oXmlSerializer: TXmlSerializer_;
begin
  oXmlSerializer:=TXmlSerializer_.Create;
  oXmlSerializer.SerializeToFile(aObj, aXmlFileName);

//  s:=oXmlSerializer.Serialize(Self);
  FreeAndNil(oXmlSerializer);

end;

end.
