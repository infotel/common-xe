unit u_web;

interface
uses
  Classes,
   System.NetEncoding,

  SHDocVw, System.SysUtils,   Winapi.ActiveX, Vcl.Graphics,
  System.Types, Forms, Vcl.Imaging.pngimage,  System.Variants,

  MSHTML_TLB;


function ExecuteScript(aWebbrowser: TWebbrowser; aScript: string): Boolean;

procedure WebBrowserScreenShot_PNG(const aWebBrowser: TWebBrowser; const aFileName: TFileName);

procedure ConvertFileToBase64_(const AInFileName, AOutFileName: string);

function FileToBase64String_(const AInFileName: string): string;
function FileToByteDynArray(const AInFileName: string): TByteDynArray;




//
//type
//  TWebBrowserFunc = record
//    function GetElementIdValue(aWebBrowser: TWebBrowser; aTagName, aTagId, aTagAttrib: String): String;
//  end;

{
var MyGUID : TGUID;
begin

 CreateGUID(MyGUID);
 ShowMessage(GUIDToString(MyGUID));
 }


implementation


procedure ConvertFileToBase64_(const AInFileName, AOutFileName: string);
var
  inStream: TStream;
  outStream: TStream;
begin
  inStream := TFileStream.Create(AInFileName, fmOpenRead);
  try
    outStream := TFileStream.Create(AOutFileName, fmCreate);
    try
      TNetEncoding.Base64.Encode(inStream, outStream);
    finally
      outStream.Free;
    end;
  finally
    inStream.Free;
  end;
end;


//-------------------------------------------------------------------
procedure WebBrowserScreenShot_PNG(const aWebBrowser: TWebBrowser; const
    aFileName: TFileName);
//-------------------------------------------------------------------
 var
   vIViewObject : IViewObject;
   r : TRect;
   bitmap : TBitmap;
   oPNGObject: TPNGObject;

 begin
   Assert (aWebBrowser.Document <> nil, 'aWebBrowser.Document <> nil,');

//   if aWebBrowser.Document <> nil then
//   begin
   aWebBrowser.Document.QueryInterface(IViewObject, vIViewObject) ;

   Assert (vIViewObject <> nil, 'vIViewObject <> nil)');

   if Assigned(vIViewObject) then
   try
     bitmap := TBitmap.Create;
     try
       r := Rect(0, 0, aWebBrowser.Width, aWebBrowser.Height) ;

       bitmap.Height := aWebBrowser.Height;
       bitmap.Width := aWebBrowser.Width;

       vIViewObject.Draw(DVASPECT_CONTENT, 1, nil, nil, Application.Handle, bitmap.Canvas.Handle, @r, nil, nil, 0) ;



       oPNGObject:=TPNGObject.Create ;
       try
         oPNGObject.Assign(bitmap) ;
         oPNGObject.SaveToFile(aFileName) ;
       finally
         oPNGObject.Free;
       end;
     finally
       FreeAndNil(bitmap);

     end;
   finally
//       vIViewObject._Release;

     vIViewObject:=nil;


   end;
 //  end;
 end;


//
//
////-----------------------------------------------------------------------
//function ExecuteScript(aWebbrowser: TWebbrowser; aScript: string): Boolean;
////-----------------------------------------------------------------------
//  { Calls JavaScript foo() function }
//var
//  Doc: IHTMLDocument2;      // current HTML document
//  HTMLWindow: IHTMLWindow2; // parent window of current HTML document
//  //JSFn: string;             // stores JavaScipt function call
//begin
//  // Get reference to current document
//  Doc := aWebbrowser.Document as IHTMLDocument2;
//  if not Assigned(Doc) then
//    Exit;
//  // Get parent window of current document
//  HTMLWindow := Doc.parentWindow;
//  if not Assigned(HTMLWindow) then
//    Exit;
//  // Run JavaScript
//  try
////    JSFn := Format('foo("%s",%d)', [S, I]);  // build function call
//    HTMLWindow.execScript(aScript, 'JavaScript'); // execute function
//  except
//    // handle exception in case JavaScript fails to run
//  end;
//end;

//-----------------------------------------------------------------------
function ExecuteScript(aWebbrowser: TWebbrowser; aScript: string): Boolean;
//-----------------------------------------------------------------------
var
  vIWin: IHTMLWindow2;
  Olelanguage: Olevariant;

  vIDoc: IHTMLDocument2;
//  v: OleVariant;
//  VScript, V: Variant;
begin
  try
     aWebbrowser.Document.QueryInterface(IHTMLDocument2, vIDoc);
    // vScript := vIDoc.Script;

  except

  end;

  Assert (vIDoc <> nil);

  if vIDoc <> nil then
  begin
    try
   //   v:=VScript.Test1();

      vIWin := vIDoc.parentWindow;
      if vIWin <> nil then
      begin
        try
        //  Olelanguage := 'JavaScript';
          vIWin.ExecScript(aScript, 'JavaScript');

//CodeSite.Send( v  );

//          v:=vIWin.ExecScript(aScript, Olelanguage);

//          vIWin.ExecScript(aScript, 'JavaScript');
        finally
//          vIWin := nil;
        end;
      end;
    finally
  //    vIDoc := nil;
    end;
  end;
end;

//-------------------------------------------------------------------
function FileToBase64String_(const AInFileName: string): string;
//-------------------------------------------------------------------
var
  inStream: TFileStream;
  outStream: TMemoryStream;
  oStrStream: TStringStream;

begin
  oStrStream:=TStringStream.Create;
  inStream := TFileStream.Create(AInFileName, fmOpenRead);

  try
    outStream := TMemoryStream.Create;
    try
      TNetEncoding.Base64.Encode(inStream, outStream);

   //   outStream.SaveToFile('d:\11111.txt');

      oStrStream.LoadFromStream(outStream);
      Result:=oStrStream.DataString;

    finally
      outStream.Free;
    end;
  finally

    FreeAndNil(inStream);
    FreeAndNil(oStrStream);

  end;
end;

//function FileToByteDynArray(const AInFileName: string): TByteDynArray;


//-------------------------------------------------------------------
function FileToByteDynArray(const AInFileName: string): TByteDynArray;
//-------------------------------------------------------------------
var
  inStream: TFileStream;

begin
//�������� Mode ���������� ����� ������ � ������. �� ������������ �� ������ ������ ��������:
//
// fmCreate � ���� ���������;
// fmOpenRead � ���� ����������� ��� ������;
// fmopenwrite � ���� ����������� ��� ������;
// fmOpenReadWrite � ���� ����������� ��� ������ � ������.
//� ������ ������ ����������� �������������:
//
// fmShareExciusive � ���� ���������� ��� �������� ������� ������������;
// fmShareDenyWrite � ������ ���������� ����� ������ ������ �� �����;
// fmShareDenyRead � ������ ���������� ����� ������ ������ � ����;
// fmShareDenyNone � ������ ���������� ����� ����������� � ������ ����� ��������.
//
//
  try
    inStream := TFileStream.Create(AInFileName, fmShareDenyNone ); //OpenRead


    SetLength(Result, inStream.Size);

    inStream.Read (Pointer(Result)^, Length(Result) );

  finally
    FreeAndNil(inStream);

  end;



end;


var
  arr: TByteDynArray;

begin
 //. FileToBase64String_ ('E:\__temp\B115CF42-4058-4228-987C-75EA39E84C0D\map.png');

 ///  arr:=FileToByteDynArray('E:\__temp\B115CF42-4058-4228-987C-75EA39E84C0D\map.png');

end.

{
function TWebBrowserFunc.GetElementIdValue(aWebBrowser: TWebBrowser; aTagName, aTagId,
    aTagAttrib: String): String;

//  ResultLabel.Caption := GetElementIdValue(WebBrowser1,'input','result','value'));

var
  vDocument: IHTMLDocument2;
  vBody: IHTMLElement2;
  vTags: IHTMLElementCollection;
  vTag: IHTMLElement;
  I: Integer;
begin
  Result:='';

  if not Supports(aWebBrowser.Document, IHTMLDocument2, vDocument) then
    raise Exception.Create('Invalid HTML document');

  if not Supports(vDocument.body, IHTMLElement2, vBody) then
    raise Exception.Create('Can''t find <body> element');

  vTags := vBody.getElementsByTagName(UpperCase(aTagName));

  for I := 0 to Pred(vTags.length) do begin
    vTag:=vTags.item(I, EmptyParam) as IHTMLElement;
    if vTag.id=aTagId then
      Result:=vTag.getAttribute(aTagAttrib,0);
  end;
end;
 }





  oRequest: CalculationResult2;
  oResult: Result2;

 // ResultStream: TMemoryStream;

  //_MyByteDynArray: TByteDynArray;      //      = array of Byte;

  oCalculatePoint : CalculatePoint;

  arrCalculatePoints : ArrayOfCalculatePoint;
  k: Integer;

begin

//  SetLength(arrCalculatePoints, 1);

//
//  ResultStream := TMemoryStream.Create;
//  ResultStream.LoadFromFile(FilenameEdit_profile.FileName);
//

  FileToBase64String_(FilenameEdit_profile.FileName);

///  SetLength(_MyByteDynArray, ResultStream.Size);


 /// k:=ResultStream.Read (Pointer(_MyByteDynArray)^, Length(_MyByteDynArray) );

//  ResultStream.SaveToFile('d:\11111111111.bin');

// fmShareDenyNone � ������ ���������� ����� ����������� � ������ ����� ��������.



//�������� Mode ���������� ����� ������ � ������. �� ������������ �� ������ ������ ��������:
//
// fmCreate � ���� ���������;
// fmOpenRead � ���� ����������� ��� ������;
// fmopenwrite � ���� ����������� ��� ������;
// fmOpenReadWrite � ���� ����������� ��� ������ � ������.
//� ������ ������ ����������� �������������:
//
// fmShareExciusive � ���� ���������� ��� �������� ������� ������������;
// fmShareDenyWrite � ������ ���������� ����� ������ ������ �� �����;
// fmShareDenyRead � ������ ���������� ����� ������ ������ � ����;
// fmShareDenyNone � ������ ���������� ����� ����������� � ������ ����� ��������.
//
//


