unit u_mitab;

interface
uses SysUtils,Classes,Forms,Math,Graphics,Dialogs, Variants,

    // u_mapx_lib,

     u_func,
     u_files,
     u_Geo,

     u_MapX,

     mitab;

type

{
  TmiFieldRec =  record
    Name  : string;
    Size  : integer;
    Type_ : integer;
//    TypeStr : string;
 //   Value : Variant;
//    IsIndexed : Boolean;
  end;
 }

  Tmitab_feature = mitab_feature;   //mitab_feature = Longword;

  //-----------------------------------------------------
  TmitabMap = class
  //-----------------------------------------------------
  private
    F_mitab_handle: integer;

    Fields: array of record
              Name: string;
              Index: integer;
            end;


    function Create_feature(feature_type: integer): Tmitab_feature;
    function GetFieldIndexByName(aName: string): integer;
    function get_field_as_double(aFeature: Tmitab_feature; field_index: integer):  double;
    function get_field_as_string(aFeature: Tmitab_feature; field_index: integer):  string;

    procedure WriteFeatureFieldValues(aFeature: Tmitab_feature; aValues: array of
        Variant);
    function Write_feature(aFeature: mitab_feature): integer;

    procedure LoadFieldNames;

  private
    function  GetFieldValue (aFeature: Tmitab_feature; aFieldName: string): Variant;

    function  Read_feature (feature_id: integer): Tmitab_feature;

    function  Next_feature_id (last_feature_id: integer): integer;


    function get_feature_count: Integer;
    function get_field_as_integer(aFeature: Tmitab_feature; field_name: string):     integer;

    procedure WriteRegionF(var aBLPoints: TBLPointArrayF; aStyle: TmiStyleRec;     aFieldValues: array of Variant);



  public
    Style: TmiStyleRec;

    destructor Destroy; override;

    function CreateFile(aFileName: AnsiString; aFields: array of TmiFieldRec):      Boolean;

    function OpenFile(aFileName: string): boolean;
    procedure CloseFile ();


//    procedure WriteBezier(var aBLPoints: TBLPointArrayF; aStyle: TmiStyleRec;   aParams: array of Variant);


    procedure WritePoint(aBLPoint: TBLPoint; aFieldValues: array of Variant);
    procedure WritePoint_LatLon(aLat,aLon: double; aFieldValues: array of Variant);

    procedure WriteLine(aBLPoints: TBLPointArray; aStyle: TmiStyleRec;   aFieldValues: array of Variant);

//    procedure WriteLineF(var aBLPoints: TBLPointArrayF; aStyle: TmiStyleRec;
//        aFieldValues: array of Variant);

    procedure WriteVector(aBLVector: TBLVector; aStyle: TmiStyleRec; aFieldValues: array of Variant);
    procedure WriteRegionRect(aBLRect: TBLRect; aStyle: TmiStyleRec; aFieldValues: array of Variant);
    procedure WriteRegion(aBLPoints: TBLPointArray; aStyle: TmiStyleRec;  aFieldValues: array of Variant);

//    procedure WriteRegionParts (var aParts: TmiPartsArr; aStyle: TmiStyle; aFieldValues:  array of TmiParam); overload;
 //   procedure WriteRegionParts (aParts: TmiRegion; aStyle: TmiStyle; aFieldValues:  array of TmiParam); overload;

  end;

  procedure mitab_CreateFile(aFileName: string; aFields: array of TmiFieldRec);

//procedure mitab_test;

const

  DEF_COORD_SYS = 'CoordSys Earth Projection 1, 1001';


//=====================================================
implementation
//=====================================================


const
  MITAB_MAX_POINT_COUNT = 5000;


const

  // feature type values
  TABFC_NoGeom      = 0;
  TABFC_Point       = 1;
  TABFC_FontPoint   = 2;
  TABFC_CustomPoint = 3;
  TABFC_Text        = 4;
  TABFC_Polyline    = 5;
  TABFC_Arc         = 6;
  TABFC_Region      = 7;
  TABFC_Rectangle   = 8;
  TABFC_Ellipse     = 9;
  TABFC_MultiPoint  = 10;


  MI_FONT_MAPINFO_SYMBOLS= 'MapInfo symbols';


  function mitab_MakeColor(aColor: integer): integer; forward;

//--------------------------------------------------------
procedure ColorTo_R_G_B(Value: TColor; var R,G,B: byte);
//--------------------------------------------------------
var cl: packed record case b:byte of
          0: (int: TColor);   1: (RGB: array[0..4] of byte);
        end;
begin
  cl.int:=Value;
  R:=cl.rgb[0];  G:=cl.rgb[1];  B:=cl.rgb[2];
end;


//--------------------------------------------------------
function Eq(aValue1, aValue2: string): boolean;
//--------------------------------------------------------
begin
  Result := AnsiCompareText(Trim(aValue1),Trim(aValue2))=0;
end;


function mitab_MakeColor(aColor: integer): integer;
var r,g,b: byte;
begin
//  Result := aColor;
//  Exit;

  ColorTo_R_G_B (aColor, r,g,b);
  Result:=(r*256*256) + (g * 256) + b;
//  Result:=(b*65536) + (g * 256) + r;

//  Result := rgb(blue, green, red);

{
   iPixelColor := ColorToRGB(clRed); //for some reason mitab apparently uses BGR not RGB as MapX (?)

   red  := getrvalue(iPixelColor);
   green := getgvalue(iPixelColor);
   blue := getbvalue(iPixelColor);

   iInvColor := rgb(blue, green, red);
}
end;


//------------------------------------------------------
procedure mapx_DeleteFile (aFileName: string);
//------------------------------------------------------
begin
  SysUtils.DeleteFile (ChangeFileExt (aFileName, '.dat'));
  SysUtils.DeleteFile (ChangeFileExt (aFileName, '.id'));
  SysUtils.DeleteFile (ChangeFileExt (aFileName, '.map'));
  SysUtils.DeleteFile (ChangeFileExt (aFileName, '.tab'));
end;


destructor TmitabMap.Destroy;
begin
  CloseFile();
  inherited;
end;


// ---------------------------------------------------------------
function TmitabMap.CreateFile(aFileName: AnsiString; aFields: array of
    TmiFieldRec): Boolean;
// ---------------------------------------------------------------
var
  pch: array[0..255] of AnsiChar;
  i,iWidth,iType: integer;
  k: Integer;

begin
  aFileName:=ChangeFileExt(aFileName, '.Tab');

  mapx_DeleteFile (aFileName);

  ForceDirByFileName (aFileName);

//  if aBLRect.TopLeft.B=0 then
//    mitab_handle:=mitab_c_create (PAnsiChar('e:\temp.tab'), PAnsiChar('tab'),
    F_mitab_handle:=mitab_c_create (PAnsiChar(aFileName), PAnsiChar('tab'),
                                  PAnsiChar('CoordSys Earth Projection 1, 0'),
//                                  45, 10, 30, 56);
                                  90, 0, 180, -180);
//  else
//    mitab_handle:=mitab_c_create (PChar(aFileName), 'tab',
//                                  'CoordSys Earth Projection 1, 1001',
//                                  aBLRect.TopLeft.B, aBLRect.BottomRight.B,
//                                  aBLRect.TopLeft.L, aBLRect.BottomRight.L);
//
//
//static void WriteFile( const char * pszDest, const char * pszMifOrTab )
//
//{
//    mitab_handle	dataset;
//    mitab_feature	feature;
//    mitab_feature	region, polyline, multipoint;
//    double		x[100], y[100];
//    int			field_index;
//
//    dataset = mitab_c_create( pszDest, pszMifOrTab,
//                              "CoordSys Earth Projection 1, 0",
//                              90, 0, 180, -180 );
//
//    if( dataset == NULL )
//    {
//        printf( "Failed to create %s file %s.\n%s\n",
//                pszMifOrTab, pszDest, mitab_c_getlasterrormsg() );
//        return;
//    }

  Result := F_mitab_handle>0;

  Assert (F_mitab_handle>0, 'procedure TmitabMap.CreateFile: '+aFileName);

   //mitab_c_set_charset(mitab_handle, 'WindowsLatin1');




  for i:=0 to High(aFields) do
  begin
    iWidth:=0;

    case aFields[i].Type_ of
      miTypeString : begin
                       iType:=TABFT_Char;
                       iWidth:=aFields[i].Size;
                     end;
      miTypeFloat    : begin iType:=TABFT_Float;   iWidth:=12; end;
      miTypeInt      : begin iType:=TABFT_Integer; iWidth:=8; end;
      miTypeSmallInt : iType:=TABFT_SmallInt;
      miTypeLogical  : iType:=TABFT_Logical;

    end;

//    StrPCopy(pch,aFields[i].Name);

    k:=mitab_c_add_field (F_mitab_handle, PAnsiChar(aFields[i].Name), iType, iWidth, 0,0,0);
//    k:=mitab_c_add_field (mitab_handle, pch, iType, iWidth, 0,0,0);

//    Assert(k<>0);
  end;

  LoadFieldNames();

end;



function TmitabMap.get_feature_count: Integer;
begin
  Result := mitab_c_get_feature_count(F_mitab_handle);
end;

// ---------------------------------------------------------------
function TmitabMap.OpenFile(aFileName: string): boolean;
// ---------------------------------------------------------------
begin
  F_mitab_handle:=mitab_c_open (PAnsiChar(aFileName));

  LoadFieldNames();
end;


procedure TmitabMap.CloseFile;
begin
  if F_mitab_handle > 0 then
  begin
    mitab_c_close (F_mitab_handle);
    F_mitab_handle:=0;
  end;
end;



function TmitabMap.GetFieldValue(aFeature: Tmitab_feature; aFieldName: string): Variant;
begin

end;


function TmitabMap.Create_feature(feature_type: integer): Tmitab_feature;
begin
  result := mitab_c_Create_feature (F_mitab_handle, feature_type);
end;

function TmitabMap.Next_feature_id(last_feature_id: integer): integer;
begin
  result := mitab_c_Next_feature_id (F_mitab_handle, last_feature_id);
end;


function TmitabMap.Read_feature(feature_id: integer): Tmitab_feature;
begin
  result := mitab_c_Read_feature (F_mitab_handle, feature_id);
end;


function TmitabMap.Write_feature(aFeature: mitab_feature): integer;
begin
  result := mitab_c_Write_feature (F_mitab_handle, aFeature);
end;


function TmitabMap.get_field_as_double(aFeature: Tmitab_feature; field_index:
    integer): double;
begin
  result := mitab_c_get_field_as_double (aFeature, field_index);
end;


function TmitabMap.get_field_as_string(aFeature: Tmitab_feature; field_index: integer): string;
var
  Pch: PAnsiChar;
begin
  Pch := mitab_c_get_field_as_string (aFeature, field_index);
  result := String(Pch);
end;


function TmitabMap.get_field_as_integer(aFeature: Tmitab_feature; field_name:
    string): integer;
var
  iIndex: integer;
begin
  iIndex:=GetFieldIndexByName(field_name);
////////  result := get_field_as_string (aFeature, iIndex);

end;

// ---------------------------------------------------------------
function TmitabMap.GetFieldIndexByName(aName: string): integer;
// ---------------------------------------------------------------
var
  i: Integer;
begin
  Result := -1;

  for I := 0 to Length(Fields)-1 do
    if Eq(Fields[i].Name, aName) then
    begin
      result := i;
    end;

end;

// ---------------------------------------------------------------
procedure TmitabMap.LoadFieldNames();
// ---------------------------------------------------------------
var
  I: Integer;
  iCount: integer;
  pCh: PAnsiChar;
begin
  iCount:=mitab_c_get_field_count (F_mitab_handle);

  SetLength (Fields, iCount);
  for I := 0 to iCount-1 do
  begin
    pCh:=mitab_c_get_field_name (F_mitab_handle, i);

    Fields[i].Name:=string(pCh);
    Fields[i].Index:=i;
  end;
end;


//------------------------------------------------------
procedure TmitabMap.WriteFeatureFieldValues(aFeature: Tmitab_feature; aValues: array of Variant);
//------------------------------------------------------
var iIndex,i: integer;
    s: string;
    arr: array[0..255] of AnsiChar; //  ShortString;
//    iFeature: Tmitab_feature;
begin
  for i := 0 to (Length(aValues) div 2) - 1  do
  begin
    iIndex:=GetFieldIndexByName (aValues[i*2]);
    if iIndex<0 then
      Continue;


    s:=  VarToStr(aValues[i*2+1]) ;

    StrPCopy(arr, s);

    mitab_c_set_field (aFeature, iIndex, arr );
  end;

end;


// ---------------------------------------------------------------
procedure TmitabMap.WriteVector(aBLVector: TBLVector; aStyle: TmiStyleRec;
    aFieldValues: array of Variant);
// ---------------------------------------------------------------
var
  BLPoints: TBLPointArray;
begin
  SetLength(BLPoints,2);
//   BLPoints.Count:=2;

  BLPoints[0]:=aBLVector.Point1;
  BLPoints[1]:=aBLVector.Point2;

  WriteLine (BLPoints, aStyle, aFieldValues);
end;



// ---------------------------------------------------------------
procedure TmitabMap.WriteLine(aBLPoints: TBLPointArray; aStyle:
    TmiStyleRec; aFieldValues: array of Variant);
// ---------------------------------------------------------------
var r,i: integer;
    iFeature: Tmitab_feature;
    x,y: array[0..MITAB_MAX_POINT_COUNT-1] of double;
begin
  iFeature:=mitab_c_create_feature (F_mitab_handle, TABFC_Polyline);
  Assert (iFeature<>0);

  Assert(Length(aBLPoints) < MITAB_MAX_POINT_COUNT);


  for I := 0 to Length(aBLPoints)-1 do
  begin
    y[i]:=aBLPoints[i].B;
    x[i]:=aBLPoints[i].L;
  end;

  mitab_c_set_points (iFeature, 0, Length(aBLPoints), x[0], y[0]);

  mitab_c_set_pen (iFeature, aStyle.LineWidth, aStyle.LineStyle, mitab_MakeColor(aStyle.LineColor));

  WriteFeatureFieldValues (iFeature, aFieldValues);


  r:=mitab_c_write_feature (F_mitab_handle, iFeature);
  Assert (r=1);

  mitab_c_destroy_feature (iFeature);


end;

// ---------------------------------------------------------------
procedure TmitabMap.WriteRegion(aBLPoints: TBLPointArray; aStyle: TmiStyleRec;
    aFieldValues: array of Variant);
// ---------------------------------------------------------------
var blPoints: TBLPointArrayF;
  I: Integer;
begin
  blPoints.Count:=Length(aBLPoints);

  for I := 0 to High(aBLPoints) do
    blPoints.Items[i]:=aBLPoints[i];

  WriteRegionF(BLPoints, aStyle, aFieldValues);

end;



// ---------------------------------------------------------------
procedure TmitabMap.WriteRegionF(var aBLPoints: TBLPointArrayF; aStyle:
    TmiStyleRec; aFieldValues: array of Variant);
// ---------------------------------------------------------------
var
  r,i,j: integer;
  iFeature: Tmitab_feature;

  x,y: array[0..MITAB_MAX_POINT_COUNT-1] of double;
begin
  iFeature:=mitab_c_create_feature (F_mitab_handle, TABFC_Region );
  Assert (iFeature<>0);

  with aStyle do
    mitab_c_set_pen  (iFeature,
                      RegionBorderWidth,
                      RegionBorderStyle,
                      mitab_MakeColor(RegionBorderColor));

//  with aStyle do
  mitab_c_set_brush(iFeature,
                      mitab_MakeColor(aStyle.RegionBorderColor),
                      mitab_MakeColor(aStyle.RegionBackColor),
                      aStyle.RegionPattern,
                      0);
//                      IIF(aStyle.RegionIsTransparent,1,0));

//                      procedure mitab_c_set_brush
//(feature: mitab_feature; fg_color, bg_color, pattern, transparent: longint);                                           stdcall; external DLL Name '_mitab_c_set_brush@20'            ;

  Assert(aBLPoints.Count<MITAB_MAX_POINT_COUNT);
       
  for i:=0 to aBLPoints.Count-1 do
  begin
    y[i]:=aBLPoints.Items[i].B;
    x[i]:=aBLPoints.Items[i].L;

  end;

  y[aBLPoints.Count]:=y[0];
  x[aBLPoints.Count]:=x[0];

  mitab_c_set_points (iFeature, j, aBLPoints.Count+1, x[0], y[0]);


///////////  WriteFeatureFieldValues (iFeature, aFieldValues);

  r:=mitab_c_write_feature (F_mitab_handle, iFeature);
  Assert (r=1);

  mitab_c_destroy_feature (iFeature);

end;


// ---------------------------------------------------------------
procedure TmitabMap.WriteRegionRect(aBLRect: TBLRect; aStyle: TmiStyleRec;
    aFieldValues: array of Variant);
// ---------------------------------------------------------------
var
  arPoints: TBLPointArrayF;
begin
  geo_BLRectToBLPointsF(aBLRect, arPoints);

  WriteRegionF (arPoints, aStyle, aFieldValues);
end;






//------------------------------------------------------
procedure TmitabMap.WritePoint(aBLPoint: TBLPoint; aFieldValues: array of
    Variant);
//------------------------------------------------------
//void MITAB_STDCALL mitab_c_set_symbol  (  mitab_feature  feature,
//int  symbol_no,
//int  symbol_size,
//int  symbol_color
//
///* -------------------------------------------------------------------- */
///*      Write a point.                                                  */
///* -------------------------------------------------------------------- */
//    feature = mitab_c_create_feature( dataset, TABFC_Point );
//
//    x[0] = 98;
//    y[0] = 50;
//
//    mitab_c_set_points( feature, 0, 1, x, y );
//    mitab_c_set_symbol( feature, 41, 15, 255*256 );
//    mitab_c_set_field( feature, 0, "100" );
//    mitab_c_set_field( feature, 1, "100.5" );
//    mitab_c_set_field( feature, 2, "12345678901234567890" );
//    mitab_c_write_feature( dataset, feature );
//    mitab_c_destroy_feature( feature );


var
  i2: Integer;
  i1: Integer;
  r,i: integer;
  iFeature: Tmitab_feature;
//  x,y: array[0..1] of double;

begin
  iFeature:=mitab_c_create_feature (F_mitab_handle, TABFC_Point);
//  iFeature:=mitab_c_create_feature (F_mitab_handle, TABFC_FontPoint);
  Assert (iFeature>0);

  //procedure mitab_c_set_symbol (feature: mitab_feature; symbol_no, symbol_size, symbol_color: longint);
  mitab_c_set_symbol( iFeature, 41, 15, 255*256 );

 // StrPCopy(pch, aStyle.FontName);

//  with aStyle do

{
  if aStyle.SymbolFontName<>'' then
    mitab_c_set_font  (iFeature, PAnsiChar(aStyle.SymbolFontName));

//  mitab_c_set_font  (iFeature, pch);// PChar(aStyle.FontName));
 //// mitab_c_set_symbol (iFeature, aStyle.SymbolNumber, aStyle.FontSize,  aStyle.FontColor);

  if aStyle.SymbolFontSize=0  then aStyle.SymbolFontSize:=10;
  if aStyle.SymbolCharacter=0 then aStyle.SymbolCharacter:=MI_SYMBOL_STAR;
 }

//  i1 := 256*256;
 // i2 := clRed;
  //i1 := i1 * i2;
  //with aStyle do
  {
  mitab_c_set_symbol(iFeature,
                     aStyle.SymbolCharacter,
                     aStyle.SymbolFontSize,
                     mitab_MakeColor(aStyle.SymbolFontColor));
   }

 // with aStyle do
  //  mitab_c_set_pen  (iFeature, RegionBorderWidth, RegionBorderStyle, RegionBorderColor);


//  mitab_c_set_symbol_angle (iFeature, aStyle.SymbolFontRotation );
//  mitab_c_set_pen  (iFeature, aStyle.LineWidth, aStyle.LineStyle, aStyle.LineColor);

{  with aStyle do
    mitab_c_set_pen  (iFeature, RegionBorderWidth, RegionBorderStyle, RegionBorderColor);

  with aStyle do
    mitab_c_set_brush  (iFeature, RegionBackColor, RegionForegroundColor, RegionPattern, IIF(RegionIsTransparent,1,0));
}

//  y[0]:=aBLPoint.B;
//  x[0]:=aBLPoint.L;

  mitab_c_set_points (iFeature, 0, 1, aBLPoint.L, aBLPoint.B);

//  mitab_c_set_field( iFeature, 0, '100' );

  WriteFeatureFieldValues (iFeature, aFieldValues);

  r:=mitab_c_write_feature (F_mitab_handle, iFeature);
  Assert (r=1);

  mitab_c_destroy_feature (iFeature);

end;

procedure TmitabMap.WritePoint_LatLon(aLat,aLon: double; aFieldValues: array of  Variant);
begin
  WritePoint(MakeBLPoint(aLat,aLon), aFieldValues);
end;



procedure mitab_CreateFile(aFileName: string; aFields: array of TmiFieldRec);
var
  obj: TmitabMap;
begin
  obj := TmitabMap.Create();

  obj.CreateFile (aFileName, aFields);
  obj.Free;
end;



begin

 //  mitab_test;


end.









{
//------------------------------------------------------
procedure TmitabMap.WriteRegionParts (aParts: TmiRegion; aStyle: TmiStyle;
    aFieldValues: array of TmiParam);
//------------------------------------------------------
var
  iPointCount: integer;
  r,i,iPart: integer;
  iFeature: Tmitab_feature;

  x,y: array[0..MITAB_MAX_POINT_COUNT-1] of double;
begin
  if aParts.Count=0 then
    Exit;


  iFeature:=mitab_c_create_feature (mitab_handle, TABFC_Region );
  Assert (iFeature<>0);

  with aParts.Style do
    mitab_c_set_pen  (iFeature,
                      RegionBorderWidth,
                      RegionBorderStyle,
                      MakeMapinfoColor(RegionBorderColor));

  with aStyle do
    mitab_c_set_brush(iFeature,
                      MakeMapinfoColor(RegionForegroundColor),
                      MakeMapinfoColor(RegionBackgroundColor),
                      RegionPattern,
                      IIF(RegionIsTransparent,1,0));


  for iPart:=0 to aParts.Count-1 do
  begin
    iPointCount:=aParts[iPart].Points.Count;

    Assert(iPointCount<>0);
    Assert(iPointCount<MITAB_MAX_POINT_COUNT);


    for I := 0 to aParts[iPart].Points.Count-1 do
    begin
      y[i]:=aParts[iPart].Points.Items[i].B;
      x[i]:=aParts[iPart].Points.Items[i].L;
    end;

    y[iPointCount]:=y[0];
    x[iPointCount]:=x[0];

    mitab_c_set_points (iFeature, -iPart, iPointCount+1, x[0], y[0]);

  end;


  WriteFeatureFieldValues (iFeature, aFieldValues);

  r:=mitab_c_write_feature (mitab_handle, iFeature);
  Assert (r=1);

  mitab_c_destroy_feature (iFeature);

end;
}


  {


//------------------------------------------------------
procedure TmitabMap.WriteBezier(var aBLPoints: TBLPointArrayF; aStyle:
    TmiStyleRec; aParams: array of Variant);
//------------------------------------------------------
const
  ARROW_LEN = 300;
var
  arrBLPointsOut: TBLPointArrayF;
  iCenter: integer;
  dAzimuth: double;
  blVector: TBLVector;
begin
  geo_MakeBezier (aBLPoints, arrBLPointsOut);

  WriteLineF(arrBLPointsOut, aStyle, aParams);

  iCenter:=arrBLPointsOut.Count div 2;

  //draw arrow on the centre

  dAzimuth:=geo_Azimuth(arrBLPointsOut.Items[iCenter], arrBLPointsOut.Items[iCenter-1]);
  blVector.Point1:= arrBLPointsOut.Items[iCenter];

  blVector.Point2:= geo_RotateByAzimuth (blVector.Point1, ARROW_LEN, geo_Azimuth_Plus_Azimuth(dAzimuth,30));
  WriteVector(blVector, aStyle, [] );
//  WriteVector(blVector, aStyle, [mapx_Par('objname','')] );

  blVector.Point2:=geo_RotateByAzimuth (blVector.Point1, ARROW_LEN, geo_Azimuth_Plus_Azimuth(dAzimuth,-30));
  WriteVector(blVector, aStyle, []);
//  WriteVector(blVector, aStyle, [mapx_Par('objname','')]);
end;

