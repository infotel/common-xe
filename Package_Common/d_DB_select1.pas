unit d_DB_select1;

interface

uses
  Variants, Classes, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, rxPlacemnt, ActnList, DB, ADODB, 
  DBGrids,

  u_DB, Grids
  ;

type
  Tdlg_DB_select1 = class(TForm)
    pn_Main: TPanel;
    DBGrid1: TDBGrid;
    DataSource1: TDataSource;
    ActionList1: TActionList;
    act_Ok: TAction;
    act_Cancel: TAction;
    FormStorage1: TFormStorage;
    pn_Buttons: TPanel;
    Bevel1: TBevel;
    btn_Ok: TButton;
    btn_Cancel: TButton;
    Button1: TButton;
    ADOQuery1: TADOQuery;
    procedure DBGrid1DblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
//    FDataset: TDataSet;
  public
    class function ExecDlg (aCaption: string;
                            aADOConnection: TADOConnection;
                            aTableName: string;
                            var aID: integer;
                            var aName: string): Boolean;
  end;

(*var
  dlg_DB_select1: Tdlg_DB_select1;
*)
implementation

{$R *.dfm}


//-------------------------------------------------------------------
class function Tdlg_DB_select1.ExecDlg(
                                        aCaption: string;
                                        aADOConnection: TADOConnection;
                                        aTableName: string;
                                        var aID: integer;
                                        var aName: string): Boolean;
//-------------------------------------------------------------------
begin
  with Tdlg_DB_select1.Create(Application) do
  try
    Caption:=aCaption;

    ADOQuery1.Connection:=aADOConnection;

    db_OpenQuery (ADOQuery1, 'SELECT id,name FROM '+ aTableName +' ORDER BY Name' );


    if not ADOQuery1.Active then
    begin
      ShowMessage('if not ADOQuery1.Active then');

      db_OpenQuery (ADOQuery1, 'SELECT id,name FROM '+ aTableName +' ORDER BY Name', [EMPTY_PARAM] );
    end;

    ADOQuery1.Locate (FLD_ID, aID, []);

   // ShowModal;
    result := ShowModal=mrOk;

  //  result := ModalResult=mrOk;

    if ModalResult=mrOk then
    begin
      aID   :=ADOQuery1.FieldValues['ID'];
      aName :=ADOQuery1.FieldValues['NAME'];
    end;// else
     // Result:=0;

  finally
    Free;
  end;
end;


procedure Tdlg_DB_select1.DBGrid1DblClick(Sender: TObject);
begin
  ModalResult:=mrOk;
end;

procedure Tdlg_DB_select1.FormCreate(Sender: TObject);
begin      
  pn_Main.Align:=alClient;

 // pn_Top_.Visible := False;

end;

end.
