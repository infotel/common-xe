unit u_zlib;

interface
uses
   SysUtils, Classes, Zlib;


  procedure zlib_CompressFile(aSrcFileName, aDestFilename: String);

  // some files -> one archive
  procedure zlib_CompressFileList(aFiles: TStrings; aFilename: String);
  procedure zlib_DecompressFileList(aFilename, aDestDirectory: String);


implementation



// -------------------------------------------------------------------
procedure zlib_CompressFileList(aFiles: TStrings; aFilename: String);
// -------------------------------------------------------------------
var
 infile, outfile, tmpFile : TFileStream;
 compr : TCompressionStream;
 i,l : Integer;
 s,sTempFileName : String;

begin
  sTempFileName:='tmp';

 if aFiles.Count > 0 then
 begin
   outFile := TFileStream.Create(aFilename,fmCreate);
   try
     { the number of aFiles }
     l := aFiles.Count;
     outfile.Write(l,SizeOf(l));
     for i := 0 to aFiles.Count-1 do
     begin
       infile := TFileStream.Create(aFiles[i], fmShareDenyWrite			);
       try
         { the original aFilename }
         s := ExtractFilename(aFiles[i]);
         l := Length(s);

         //write filename
         outfile.Write(l,SizeOf(l));
         outfile.Write(s[1],l);


         { the original filesize }
         l := infile.Size;
         outfile.Write(l,SizeOf(l));
         { compress and store the file temporary}

         tmpFile := TFileStream.Create(sTempFileName,fmCreate);
         compr := TCompressionStream.Create(clFastest,tmpfile);

         try
           compr.CopyFrom(infile,l);
         finally
           compr.Free;
           tmpFile.Free;
         end;
         
         { append the compressed file to the destination file }
         tmpFile := TFileStream.Create(sTempFileName,fmShareDenyWrite	);
         try
           outfile.CopyFrom(tmpFile,0);
         finally
           tmpFile.Free;
         end;
       finally
         infile.Free;
       end;
     end;
   finally
     outfile.Free;
   end;
   DeleteFile(sTempFileName);
 end;
end;


// -------------------------------------------------------------------
procedure zlib_DecompressFileList(aFilename, aDestDirectory: String);
// -------------------------------------------------------------------
var
  iRead: integer;
  sDestDir,s : String;
  decompr : TDecompressionStream;
  infile, outfile : TFilestream;
  i,l,c : Integer;
begin
 // IncludeTrailingPathDelimiter (D6/D7 only)
 sDestDir := IncludeTrailingPathDelimiter(aDestDirectory);

 ForceDirectories(sDestDir);

 infile := TFileStream.Create(aFilename, fmShareDenyWrite	);
 try
   { number of files }
   infile.Read(c,SizeOf(c));
   for i := 1 to c do
   begin
     { read Filenames }
     iRead:=infile.Read(l,SizeOf(l));

     SetLength(s,l);
     infile.Read(s[1],l);

     { read filesize }
     infile.Read(l,SizeOf(l));
     { decompress the files and store it }
     s := sDestDir+s; //include the path 
     outfile := TFileStream.Create(s,fmCreate);
     decompr := TDecompressionStream.Create(infile);
     try
       outfile.CopyFrom(decompr,l);
     finally
       outfile.Free;
       decompr.Free;
     end;
   end;
 finally
   infile.Free;
 end;
end;

// -------------------------------------------------------------------
procedure zlib_CompressFile(aSrcFileName, aDestFilename: String);
// -------------------------------------------------------------------
var
  oList: TStringList;
begin
  oList:=TStringList.Create;
  oList.Text := aSrcFileName;

  zlib_CompressFileList(oList, aDestFilename);

  oList.Free;
end;


begin

//  zlib_Compress('k:\1\BR0001_1.kup.bin','k:\1\BR0001_1.kup.zip');


end.

