unit dm_DBManager;

interface

uses
  SysUtils, Classes, ADODB, Forms,

  u_db, DB;

type
  TdmDataManager = class(TDataModule)
    ADOCommand1: TADOCommand;
    ADOStoredProc1: TADOStoredProc;
    ADOQuery1: TADOQuery;
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);

  public
    ParamList: TdbParamList;

//    destructor Destroy; override;
    class procedure Init;

    procedure SetADOConnection(Value: TADOCOnnection);

    function ExecCommand(aSQL : string; aParamList: TdbParamList): boolean;

    function UpdateRecord(aTableName : string; aID : integer; aParamList:
        TdbParamList): boolean;
  end;

(*
var
 dmDBManager: TdmDBManager;
*)

implementation

{$R *.dfm}

class procedure TdmDataManager.Init;
begin
 //zz if not Assigned(dmDBManager) then
 //zzz   dmDBManager := TdmDBManager.Create(Application);
end;


procedure TdmDataManager.DataModuleCreate(Sender: TObject);
begin
  ParamList:=TdbParamList.Create;
end;

procedure TdmDataManager.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(ParamList);
end;

//------------------------------------------------------------------
procedure TdmDataManager.SetADOConnection(Value: TADOCOnnection);
//------------------------------------------------------------------
begin
  ADOCommand1.Connection:=Value;
  ADOStoredProc1.Connection:=Value;
  ADOQuery1.Connection:=Value;


end;

//----------------------------------------------------------------------------
function TdmDataManager.UpdateRecord(aTableName : string; aID : integer;
    aParamList: TdbParamList): boolean;
//----------------------------------------------------------------------------
var
  s: string;
begin
  Assert (aTableName<>'', 'function TDBManager.UpdateRecord - aTableName=''''');

  s:=db_MakeUpdateString(aTableName, aID, aParamList);

  Result := ExecCommand (s, aParamList);

end;



//--------------------------------------------------------------------
function TdmDataManager.ExecCommand(aSQL : string; aParamList: TdbParamList):
    boolean;
//--------------------------------------------------------------------
var i: integer;
  oParameter: TParameter;
begin
  ADOCommand1.CommandText:=aSQL;

  for i:=0 to aParamList.Count-1 do
  begin
    oParameter:=ADOCommand1.Parameters.FindParam (aParamList[i].FieldName);
    if oParameter<>nil then
      try
        oParameter.Value :=aParamList[i].FieldValue;
      except
      end;
  end;

  try
    ADOCommand1.Execute;
    Result:=True;
  except
    Result:=False;
  end;

end;


end.
