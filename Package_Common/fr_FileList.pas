unit fr_FileList;

interface

uses
  Windows, SysUtils, Classes, Controls, Forms, Dialogs, Menus,
  ExtCtrls, StdCtrls, ActnList, ComCtrls, cxPropertiesStore,

///  u_func,
  u_reg,
  u_dlg, ToolWin
  ;

type
  Tframe_FileList = class(TForm)
    OpenDialog1: TOpenDialog;
    ActionList1: TActionList;
    ToolBar1: TToolBar;
    pn_Buttons: TPanel;
    Button4: TButton;
    Button1: TButton;
    act_Add: TAction;
    act_Del: TAction;
    StatusBar1: TStatusBar;
    btn_Tools: TButton;
    act_Tools: TAction;
    pop_Tools__: TPopupMenu;
    cxPropertiesStore1: TcxPropertiesStore;
    Files: TListBox;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FilesKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure act_AddExecute(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction;
      var Handled: Boolean);
    procedure Button2Click(Sender: TObject);
  private
  //  FLastFileDir: string;

    FRegPath: string;
    FFileList: TstringList;

    procedure Add(aFileName: string);
    procedure DelSelected;
    procedure InitOpenDialog(aFileName,aFilter,aFileExt: string);

  public
  //  FFilter: string;
    procedure SetFilter(aFilter: string);


    procedure Dlg_Add;
    function  GetFocusedFileName (): string;

    function  SelCount (): integer;
    function  Count (): integer;
    procedure ShowToolBar (Value: boolean);
    procedure ShowButtonTools (aPopupMenu: TPopupMenu);

    function GetSelectedString (): string;
    procedure SetRegistryPath(aPath: string);

  end;


const
{
  FILTER_MAPINFO_TAB ='MapInfo TAB|*.tab';
  FILTER_MSI ='MSI (*.msi)|*.msi';
}



  FILTER_MIF = 'MapInfo (*.tab)|*.tab';
//  FILTER_RLF = 'Onega ������ (*.rlf)|*.rlf';
  FILTER_MSI = 'MSI (*.msi)|*.msi';
  FILTER_XML ='XML (*.xml)|*.xml';
  FILTER_XSL ='XSL (*.xsl)|*.xsl';


implementation  {$R *.DFM}


procedure Tframe_FileList.FormCreate(Sender: TObject);
begin
  Files.Align:=alClient;

  if Owner is TPanel then begin
    (Owner as TPanel).BevelOuter:=bvNone;
    (Owner as TPanel).BorderStyle:=bsNone;

  end;


  FFileList := TstringList.Create();
end;


procedure Tframe_FileList.FormDestroy(Sender: TObject);
begin
//////  Assert(FRegPath<>'');


 // ShowMessage(IntToStr(Files.Items.Count));


 if FRegPath<>'' then
  begin
    reg_SaveToRegistry(FRegPath, FFileList);
    reg_WriteString (FRegPath,'InitialDir',OpenDialog1.InitialDir);
  end;
  //  SaveToRegistry(FRegPath, FFileList);


  FreeAndNil(FFileList);

  inherited;
end;


//------------------------------------------------------
procedure Tframe_FileList.InitOpenDialog(aFileName,aFilter,aFileExt: string);
//------------------------------------------------------
begin
  aFileName:=Trim(aFileName);
  OpenDialog1.FileName:=aFileName;
  OpenDialog1.InitialDir:=ExtractFileDir (aFileName);
  OpenDialog1.Filter:=aFilter;
//  OpenDialog1.DefaultExt:=aFileExt;
end;

//--------------------------------------------------------
function Tframe_FileList.GetFocusedFileName (): string;
//--------------------------------------------------------
begin
  with Files do begin
    if ItemIndex>=0 then Result:=Items[ItemIndex]
                    else Result:='';
    if Items.Count=1 then Result:=Items[0];
  end;
end;


//--------------------------------------------------------
procedure Tframe_FileList.Dlg_Add;
//--------------------------------------------------------
var i: integer; // sFileName: string;
begin
 // OpenDialog1.Filter:=aFilter;

  if OpenDialog1.Execute then
    for i:=0 to OpenDialog1.Files.Count-1 do
      Add(OpenDialog1.Files[i]);
end;

//--------------------------------------------------------
procedure Tframe_FileList.DelSelected;
//--------------------------------------------------------
var i: integer;
begin
  if not ConfirmDlg (MSG_ASK_DEL_FILES) then
    Exit;

  with Files do
  begin
  //  if Items.Count>0 then
    //  FLastFileDir:=ExtractFileDir (Items[0]);

    for i:=Items.Count-1 downto 0 do
     if Selected[i] then Items.Delete(i);
  end;

  FFileList.Assign(Files.Items);

end;




procedure Tframe_FileList.FilesKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key=VK_DELETE then act_Del.Execute;
end;


function Tframe_FileList.SelCount: integer;
begin
  Result:=Files.SelCount;
end;

function Tframe_FileList.Count: integer;
begin
  Result:=Files.Items.Count;
end;

procedure Tframe_FileList.act_AddExecute(Sender: TObject);
begin
  if Sender=act_Add then
    Dlg_Add () else

  if Sender=act_Del then
    DelSelected else ;

  if Sender=act_Tools then begin
    if Assigned(btn_Tools.PopupMenu) then
      btn_Tools.PopupMenu.Popup (Mouse.CursorPos.X, Mouse.CursorPos.Y);
  end;

end;


procedure Tframe_FileList.ShowButtonTools (aPopupMenu: TPopupMenu);
begin
  btn_Tools.Visible:=True;
  btn_Tools.PopupMenu:=aPopupMenu;
end;


procedure Tframe_FileList.ShowToolBar(Value: boolean);
begin
  pn_Buttons.Visible:=Value;
end;


function Tframe_FileList.GetSelectedString (): string;
//var ind: integer;
begin
  Result:='';
  with Files do //begin
    if Items.Count>0 then Result:=Items[ItemIndex];
//    if Items.Count=1 then Result:=Items[0];
//  end;
end;


procedure Tframe_FileList.ActionList1Update(Action: TBasicAction; var Handled: Boolean);
begin
  act_Del.Enabled:=(Files.SelCount>0);
  act_Tools.Enabled:=act_Del.Enabled;

  StatusBar1.SimpleText:=Format('������: %d', [Files.Items.Count]);
end;

procedure Tframe_FileList.Button2Click(Sender: TObject);
begin

end;

//--------------------------------------------------------
procedure Tframe_FileList.Add(aFileName: string);
//--------------------------------------------------------
begin
  if Files.Items.IndexOf(aFileName)<0 then
    Files.Items.Add(aFileName);

  FFileList.Assign(Files.Items);
end;

procedure Tframe_FileList.SetFilter(aFilter: string);
begin
  OpenDialog1.Filter := aFilter;
end;



//-------------------------------------------------------------------
procedure Tframe_FileList.SetRegistryPath(aPath: string);
//-------------------------------------------------------------------   
begin
//  aPath:=aPath + 'FileList\';

  cxPropertiesStore1.StorageName:=aPath + cxPropertiesStore1.Name;
  cxPropertiesStore1.Active := True;
  cxPropertiesStore1.RestoreFrom;

  FRegPath:=aPath;

  reg_LoadFromRegistry(aPath, Files.Items);
  OpenDialog1.InitialDir:=reg_ReadString(FRegPath,'InitialDir',OpenDialog1.InitialDir);


  FFileList.Assign(Files.Items);


end;


end.
