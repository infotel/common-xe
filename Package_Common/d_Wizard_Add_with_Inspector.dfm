inherited dlg_Wizard_add_with_Inspector: Tdlg_Wizard_add_with_Inspector
  Left = 536
  Top = 382
  Width = 568
  Height = 413
  Caption = 'dlg_Wizard_add_with_Inspector'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pn_Buttons: TPanel
    Top = 344
    Width = 560
    inherited Bevel1: TBevel
      Width = 560
    end
    inherited Panel3: TPanel
      Left = 364
      inherited btn_Ok: TButton
        Left = 397
      end
      inherited btn_Cancel: TButton
        Left = 481
      end
    end
  end
  inherited pn_Top_: TPanel
    Width = 560
    inherited Bevel2: TBevel
      Width = 560
    end
    inherited pn_Header: TPanel
      Width = 560
      Height = 54
    end
  end
  inherited Panel1: TPanel
    Width = 560
    DesignSize = (
      560
      50)
    inherited ed_Name_: TEdit
      Top = 20
      Width = 549
    end
  end
  object Panel2: TPanel [3]
    Left = 0
    Top = 107
    Width = 560
    Height = 222
    Align = alTop
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelOuter = bvNone
    BorderWidth = 5
    TabOrder = 3
    object PageControl1: TPageControl
      Left = 5
      Top = 5
      Width = 550
      Height = 212
      ActivePage = TabSheet1
      Align = alClient
      Style = tsButtons
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = 'Params'
      end
    end
  end
  inherited ActionList1: TActionList
    Left = 456
  end
  inherited FormStorage1: TFormStorage
    Left = 380
  end
  inherited cxPropertiesStore: TcxPropertiesStore
    Top = 4
  end
end
