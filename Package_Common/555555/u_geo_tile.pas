unit u_geo_tile;

//http://www.maptiler.org/google-maps-coordinates-tile-bounds-projection/

interface
uses
  Math,

  u_geo;


//procedure geo_Get_Tile_Lat_Lon(aBlock_X, aBlock_Y, aZ: integer; var aLat, aLon:
//    Double);

function geo_LatLonBounds_to_BLRect(aValue: TLatLonBounds): TBLRect;

function geo_Get_Tile_LatLonBounds(aZ: integer; aBlock_X, aBlock_Y: integer):
    TLatLonBounds;

    
//procedure geo_LatLonToMeters_Mercator(aLat, aLon: Double; var aY_lat, aX_lon:
//    double);

implementation



// ---------------------------------------------------------------
function geo_Get_Tile_LatLonBounds(aZ: integer; aBlock_X, aBlock_Y: integer):
    TLatLonBounds;
// ---------------------------------------------------------------
var
  eLat_step: Double;
  eLon_step: Double;
  iMax: Integer;
  x, y, s: Double;

  eLat, eLon: Double;
begin
  iMax:=Trunc(Math.Power(2, aZ));

  x:=aBlock_X / iMax;
  y:=aBlock_Y / iMax;
  
  eLon:=(X-0.5)*360;
  eLat:=(2*arctan(exp(Pi-2*Pi*Y))-Pi/2)*180/Pi;

  Result.Lat_Max:=eLat;
  Result.Lon_Min:=eLon;  
  
  //-----------------------------------------------
  x:=(aBlock_X+1) / iMax;
  y:=(aBlock_Y+1) / iMax;
  
  eLon:=(X-0.5)*360;
  eLat:=(2*arctan(exp(Pi-2*Pi*Y))-Pi/2)*180/Pi;
  
  Result.Lat_Min:=eLat;
  Result.Lon_Max:=eLon; 

  
//  eLat_step:=(2*85)/iMax;
//  eLon_step:=(2*180)/iMax; 
// 
//  Result.Lon_Min:=85 - eLon_step * aBlock_X;
//  Result.Lon_Max:=Result.Lon_Min + eLon_step;  
//
//  Result.Lat_Min:=-180 + eLat_step * aBlock_Y;
//  Result.Lat_Max:=Result.Lat_Min + eLat_step;  


end;


// ---------------------------------------------------------------
function geo_LatLonBounds_to_BLRect(aValue: TLatLonBounds): TBLRect;
// ---------------------------------------------------------------
begin
  Result.TopLeft.B    :=aValue.Lat_Max;
  Result.TopLeft.L    :=aValue.Lon_Min;
  Result.BottomRight.B:=aValue.Lat_Min;
  Result.BottomRight.L:=aValue.Lon_Max;

  
end;


procedure Test();
var
 iZ: integer; 
 iBlock_X, iBlock_Y: integer; 
  x: Double;
  y: Double;
begin
  //geo_LatLonToMeters_Mercator (43,56, x,y);

//  geo_Get_Tile_XY_blocks (-180,-85, iZ, iBlock_X, iBlock_Y);


//  geo_Get_Tile_LatLonBounds(0,0,0 );
  
end;


begin
//  Test;



end.


{
//
//				<ows:WGS84BoundingBox>
//					<ows:LowerCorner>-180 -85</ows:LowerCorner>
//					<ows:UpperCorner>180 85</ows:UpperCorner>
//				</ows:WGS84BoundingBox>



TLatLonBounds = record
 // TXYBounds_MinMax = record
                Lat_Min  : Double;
                Lat_Max  : Double;
                Lon_Min  : Double;
                Lon_Max  : Double;
              end;

              
      {

// ---------------------------------------------------------------
procedure geo_Get_Tile_XY_blocks(aLat, aLon: Double; aZ: integer; var aBlock_X,
    aBlock_Y: integer);
// ---------------------------------------------------------------    
var
  iMax: Integer;
  x, y, s: Double;
begin
  x:=0.5 + aLon/360.0;
  s:=Sin(DegToRad(aLat));
    

  try    
    if Abs(s)<>1 then
      y:=0.5-(1/(4*Pi))*ln((1+s)/(1-s))
    else
      y:=0;
  except        

  end;              
    
  iMax:=  Trunc(Math.Power(2, aZ));

  aBlock_X:=Floor(iMax*x);
  aBlock_Y:=Floor(iMax*y);    

end;


// ---------------------------------------------------------------
procedure geo_Get_Tile_XY_blocks_meter(aLat_x, aLon_y: Double; aZ: integer; var
    aBlock_X, aBlock_Y: integer);
// ---------------------------------------------------------------
const
// epsg 3857
  DEF_MAX =  20037508; //.34279000;         20 037 508 

var
  eLat_step: Double;
  eLon_step: Double;

  iMax: Integer;
//  x, y, s: Double;
begin
  iMax:=  Trunc(Math.Power(2, aZ));

  eLat_step:=(2*DEF_MAX)/iMax;
  eLon_step:=(2*DEF_MAX)/iMax; 

  
  aBlock_X:=Trunc ((DEF_MAX - aLat_x) / eLat_step);
  aBlock_Y:=Trunc ((DEF_MAX - aLon_y) / eLat_step);
  
  

end;




 
// ---------------------------------------------------------------
procedure geo_Get_Tile_Lat_Lon(aBlock_X, aBlock_Y, aZ: integer; var aLat, aLon:
    Double);
// ---------------------------------------------------------------
begin

//      Result:=TWGS.Create(0, 0);
//  Result.Lon:=(X-0.5)*360;
//  Result.Lat:=(2*arctan(exp(Pi-2*Pi*Y))-Pi/2)*180/Pi;
  
end;
