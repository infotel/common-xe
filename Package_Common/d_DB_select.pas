unit d_DB_Select;

interface

uses
  Classes, Controls, Forms, Dialogs,
  Db, DBGrids, ADODB,ExtCtrls, 

  d_Wizard1,
  u_DB, Grids, rxPlacemnt, ActnList, StdCtrls;

type
  Tdlg_DB_select = class(Tdlg_Wizard1)
    pn_Main: TPanel;
    DBGrid1: TDBGrid;
    DataSource1: TDataSource;
    ADOQuery1: TADOQuery;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
  private
//    FDataset: TDataSet;
  public
    class function ExecDlg (aCaption: string;
                            aADOConnection: TADOConnection;
                            aTableName: string;
                            var aID: integer;
                            var aName: string): Boolean;
  end;


implementation {$R *.DFM}


procedure Tdlg_DB_select.Button1Click(Sender: TObject);
begin

end;


//-------------------------------------------------------------------
class function Tdlg_DB_select.ExecDlg(aCaption: string; aADOConnection: TADOConnection;
    aTableName: string; var aID: integer; var aName: string): Boolean;
//-------------------------------------------------------------------
begin
  with Tdlg_DB_select.Create(Application) do
  try
    if aCaption='' then
      aCaption:=aTableName;

    Caption:=aCaption;

    ADOQuery1.Connection:=aADOConnection;

    db_OpenQuery (ADOQuery1, 'SELECT id,name FROM '+ aTableName +' ORDER BY Name' );


    if not ADOQuery1.Active then
    begin
      ShowMessage('if not ADOQuery1.Active then');

      db_OpenQuery (ADOQuery1, 'SELECT id,name FROM '+ aTableName +' ORDER BY Name', [EMPTY_PARAM] );
    end;

    ADOQuery1.Locate (FLD_ID, aID, []);

   // ShowModal;
    result := ShowModal=mrOk;

  //  result := ModalResult=mrOk;

    if ModalResult=mrOk then
    begin
      aID   :=ADOQuery1.FieldValues['ID'];
      aName :=ADOQuery1.FieldValues['NAME'];
    end;// else
     // Result:=0;

  finally
    Free;
  end;
end;

//--------------------------------------------------------------------
procedure Tdlg_DB_select.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;
//  db_SetComponentADOConn (Self, dmMain.ADOConnection);

  pn_Main.Align:=alClient;

  pn_Top_.Visible := False;

  //DBGrid1.Columns[1].Visible:=g_Debug;


  //  db_OpenQuery (ADOQuery1,
    //              'SELECT id,name FROM '+ TBL_PROJECT   +' ORDER BY name'
      //            );



{  pn_Main.Align:=alClient;
  FDataset:=DBGrid1.DataSource.DataSet;

  db_SetComponentADOConnection (Self, dmMain.ADOConnection2);
       }
end;


//-------------------------------------------------------------------
procedure Tdlg_DB_select.DBGrid1DblClick(Sender: TObject);
//-------------------------------------------------------------------
begin
  ModalResult:=mrOk;
//  inherited;
 // act_Ok.Execute;
end;

end.
