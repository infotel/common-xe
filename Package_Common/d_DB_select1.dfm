object dlg_DB_select1: Tdlg_DB_select1
  Left = 685
  Top = 253
  Width = 439
  Height = 493
  Caption = 'dlg_DB_select1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDefault
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object pn_Main: TPanel
    Left = 0
    Top = 0
    Width = 431
    Height = 207
    Align = alTop
    BevelOuter = bvNone
    BorderWidth = 5
    TabOrder = 0
    object DBGrid1: TDBGrid
      Left = 5
      Top = 5
      Width = 421
      Height = 197
      Align = alClient
      DataSource = DataSource1
      Options = [dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDblClick = DBGrid1DblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'name'
          Width = 292
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'id'
          Visible = True
        end>
    end
  end
  object pn_Buttons: TPanel
    Left = 0
    Top = 424
    Width = 431
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    Constraints.MaxHeight = 35
    Constraints.MinHeight = 35
    TabOrder = 1
    DesignSize = (
      431
      35)
    object Bevel1: TBevel
      Left = 0
      Top = 0
      Width = 431
      Height = 2
      Align = alTop
      Shape = bsTopLine
    end
    object btn_Ok: TButton
      Left = 268
      Top = 8
      Width = 75
      Height = 23
      Anchors = [akTop, akRight]
      Caption = 'Ok'
      Default = True
      ModalResult = 1
      TabOrder = 0
    end
    object btn_Cancel: TButton
      Left = 352
      Top = 8
      Width = 75
      Height = 23
      Anchors = [akTop, akRight]
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
    end
    object Button1: TButton
      Left = 43
      Top = 8
      Width = 75
      Height = 25
      Action = act_Ok
      Anchors = [akTop, akRight]
      TabOrder = 2
    end
  end
  object DataSource1: TDataSource
    Left = 236
    Top = 356
  end
  object ActionList1: TActionList
    Left = 68
    Top = 304
    object act_Ok: TAction
      Category = 'Main'
      Caption = 'Ok'
    end
    object act_Cancel: TAction
      Category = 'Main'
      Caption = #1054#1090#1084#1077#1085#1072
    end
  end
  object FormStorage1: TFormStorage
    Active = False
    IniFileName = 'Software\Onega\'
    Options = []
    StoredValues = <>
    Left = 96
    Top = 304
  end
  object ADOQuery1: TADOQuery
    Parameters = <>
    Left = 224
    Top = 240
  end
end
