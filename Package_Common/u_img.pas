unit u_img;

interface

uses Graphics,SysUtils, Windows, Dialogs, Forms, FileCtrl, extctrls, Math,
    // Bmp2tiff,

    Jpeg,
    pngimage;

   //  PNGImage,
   //  RxGif;


  function Dlg_SelectColor(aColor: TColor): TColor;


//  procedure img_SaveBitmapToFile_file(aBitmapFileName, aFileName: String);
//  procedure img_SaveBitmapToFile (aBitmap: Graphics.TBitmap; aFileName: String);

  //---------------------------------------------------
  // Color functions
  //---------------------------------------------------


  procedure img_FillRectGradient_OnCanvas (aCanvas: TCanvas; aRect:TRect; aBeginColor,aEndColor: integer);

  function  img_ColorFromString (Value: String): integer;


  //---------------------------------------------------
  // colors, images
  //---------------------------------------------------
  procedure ColorTo_R_G_B(Value: TColor; var R,G,B: byte);
  function  ColorToHEX (Value: integer): String;

  function  MakeRGB (r,g,b: integer): integer;
  function  img_GetRandomColor(): integer;

  function img_MakeGradientColor (aValue,aMinValue,aMaxValue: double;
                                aBeginColor,aEndColor: integer ): integer;

  function img_MakeGradientColor_ln (aValue,aMinValue,aMaxValue: double;
                                aBeginColor,aEndColor: integer): integer; 

  procedure img_FillRectGradientVert (aHandle: THandle; //aCanvas: TCanvas;
                                aRect: TRect;
                                aBeginColor,aEndColor: integer);

  procedure FillRectGradient_test (aImage: TImage;
                                 aMinValue,aMaxvalue: double;
                                 aBeginColor,aEndColor: integer); 

//  procedure SaveBitmapToJPG  (aBitmap: Graphics.TBitmap; aFileName: String);

//  procedure SaveBitmapToGIF  (aBitmap: Graphics.TBitmap; aFileName: String);


  procedure img_DrawCharacter (aHandle: THandle; //ACanvas: TCanvas;
                              ARect: TRect; aBackColor: integer;
                              aFontName: String; aCharacter: Integer );

  procedure img_DrawRect(aCanvas: TCanvas; aRect: TRect; aColor,aBackColor: integer; aBorderColor: integer=clBlack);



  procedure img_DrawColorRect_OnCanvas  (aCanvas: TCanvas;
                                         aRect: TRect;
                                         aBackColor,aColor: integer;
                                         aOffset: integer);

  procedure img_BMP_moveto_JPEG(aBmpFileName, aJPEGFileName: String);


  procedure img_InsertScaleRuleIntoJPG(aSrcFileName, aDestFileName: string;
      aScale: integer);

  procedure img_InsertScaleRuleIntoBMP(aSrcFileName, aDestFileName: string;
      aScale: integer);




//==================================================================
implementation
//==================================================================

  procedure img_InsertScaleRuleIntoBitmap(aBitmap: Graphics.TBitmap; aScale,
      aFontSize: integer); forward;

  procedure img_FillRectGradient (aHandle: THandle; aRect:TRect; aBeginColor,aEndColor: integer);  forward;




procedure ForceDirByFileName (aFileName: string);
begin
  ForceDirectories (ExtractFileDir (aFileName));
end;

function ReplaceStr (Value, OldPattern, NewPattern: string): string;
begin
  Result:=StringReplace (Value, OldPattern, NewPattern, [rfReplaceAll]);
end;





//------------------------------------------------------------------------------ 
// Function for getting mixed color from two given colors, with a relative 
// distance from two colors determined by aPosition value inside 
// aMinPosition..aMaxPosition range
// Author: Dmitri Papichev (c) 2001
// License type: Freeware 
//------------------------------------------------------------------------------ 

function GetMixedColor(const aStartColor, aEndColor: TColor; const
    aMinPosition, aPosition, aMaxPosition: integer): TColor;
var
  Fraction: double; 
  R, G, B, 
    R0, G0, B0, 
    R1, G1, B1: byte; 
begin 
  {process aPosition out of range situation} 
  if (aMaxPosition < aMinPosition) then
  begin
    raise Exception.Create 
      ('GetMixedColor: aMaxPosition is less then aMinPosition'); 
  end; {if}

  {if aPosition is outside aMinPosition..aMaxPosition range, the closest boundary 
   is effectively substituted through the adjustment of Fraction}
  Fraction := 
    Min(1, Max(0, (aPosition - aMinPosition) / (aMaxPosition - aMinPosition))); 

  {extract the intensity values} 
  R0 := GetRValue(aStartColor); 
  G0 := GetGValue(aStartColor); 
  B0 := GetBValue(aStartColor); 
  R1 := GetRValue(aEndColor);
  G1 := GetGValue(aEndColor); 
  B1 := GetBValue(aEndColor); 

  {calculate the resulting intensity values} 
  R := R0 + Round((R1 - R0) * Fraction); 
  G := G0 + Round((G1 - G0) * Fraction); 
  B := B0 + Round((B1 - B0) * Fraction); 

  {combine intensities in a resulting color} 
  Result := RGB(R, G, B); 
end; {--GetMixedColor--}



procedure img_BMP_moveto_JPEG(aBmpFileName, aJPEGFileName: String);
var
  JPEG: TJPEGImage;
  oBitmap: Graphics.TBitmap;
begin
  if not FileExists( aBmpFileName) then
    Exit;

  oBitmap:= Graphics.TBitmap.Create;
  JPEG := TJPEGImage.Create ();

  try
    oBitmap.LoadFromFile(aBmpFileName);
    JPEG.Assign(oBitmap);
    JPEG.SaveToFile(aJPEGFileName);

   // Image1.Picture.Assign(JPEG);
  finally
    JPEG.Free;
    oBitmap.Free;

  //  Sysutils.DeleteFile (aBmpFileName);
  end;
end;



//---------------------------------------------------------
procedure img_DrawColorRect (aHandle: THandle;
                             aRect: TRect;
                             aBackColor,aColor: integer;
                             aOffset: integer);
//---------------------------------------------------------
var
  oCanvas: TCanvas;
  iColor: integer;
  iColor2: integer;
begin

  oCanvas:=TCanvas.Create;
  oCanvas.Handle:=aHandle;

  iColor:=oCanvas.Brush.Color;
  iColor2:=oCanvas.Pen.Color;

  oCanvas.Brush.Color:=aBackColor;
  oCanvas.FillRect(aRect);

  InflateRect (aRect,-aOffset,-aOffset);

//  oCanvas.Brush.Color:=aBackColor;
//  oCanvas.FillRect(aRect);
  oCanvas.Brush.Color:=aColor;
  oCanvas.FillRect(aRect);
  oCanvas.Brush.Color:=0;
  oCanvas.FrameRect(aRect);

  oCanvas.Brush.Color:=iColor;
  oCanvas.Pen.Color:=iColor2;

  oCanvas.Handle:=0;
  oCanvas.Free;

end;


//============================================================================//
//  ��������� ��� ���������� ��������
//    - ������� Bitmap � ������� �� ���� ������
//    - �������� ������ � ������� ������ �������� ������
//
//============================================================================//
procedure img_DrawCharacter  (aHandle: THandle; //ACanvas: TCanvas;
                              ARect: TRect; aBackColor: integer;
                              aFontName: String; aCharacter: Integer );
//-------------------------------------------------------------------
var n: integer;
    b: Graphics.TBitmap;

    rFrom,
    rTo: TRect;
    oCanvas: TCanvas;

begin
  if aFontName='' then Exit;

  n:=aCharacter;

  oCanvas:=TCanvas.Create;
  oCanvas.Handle:=aHandle;


  b := Graphics.TBitmap.Create;
  try
    oCanvas.Pen.Color:=clBlack;
    oCanvas.Brush.Color:=aBackColor;
    oCanvas.FillRect(aRect);

    b.Width  := 100;
    b.Height := 100;
    b.Canvas.Font.Name := aFontName;
    b.Canvas.Font.Size:=12;
    b.Canvas.TextOut(0,0, Char(n));

    rFrom.Left   := 0;
    rFrom.Top    := 0;
    rFrom.Right  := b.Canvas.TextWidth(Char(n));
    rFrom.Bottom := b.Canvas.TextHeight(Char(n));

    rTo := ARect;
    rTo.Right  := rTo.Left + (rFrom.Right - rFrom.Left);
    rTo.Bottom := rTo.Top + (rFrom.Bottom - rFrom.Top);
    oCanvas.CopyRect(rTo, b.Canvas, rFrom);
  finally
    b.Free;
  end;


  oCanvas.Handle:=0;
  oCanvas.Free;

end;


//============================================================================//
//  ��������� ��� ���������� ��������
//    - ������� Bitmap � ������� �� ���� ������
//    - �������� ������ � ������� ������ �������� ������
//
//============================================================================//
procedure img_DrawCharacter_OnCanvas (aCanvas: TCanvas;
                                      ARect: TRect; aBackColor: integer;
                                      aFontName: String; aCharacter: Integer );
//-------------------------------------------------------------------
var n: integer;
    b: Graphics.TBitmap;

    rFrom,
    rTo: TRect;
  //  oCanvas: TCanvas;

begin
  if aFontName='' then Exit;

  n:=aCharacter;

 // oCanvas:=TCanvas.Create;
  //oCanvas.Handle:=aHandle;


  b := Graphics.TBitmap.Create;
  try
    aCanvas.Pen.Color:=clBlack;
    aCanvas.Brush.Color:=aBackColor;
    aCanvas.FillRect(aRect);

    b.Width  := 100;
    b.Height := 100;
    b.Canvas.Font.Name := aFontName;
    b.Canvas.Font.Size:=12;
    b.Canvas.TextOut(0,0, Char(n));

    rFrom.Left   := 0;
    rFrom.Top    := 0;
    rFrom.Right  := b.Canvas.TextWidth(Char(n));
    rFrom.Bottom := b.Canvas.TextHeight(Char(n));

    rTo := ARect;
    rTo.Right  := rTo.Left + (rFrom.Right - rFrom.Left);
    rTo.Bottom := rTo.Top + (rFrom.Bottom - rFrom.Top);
    aCanvas.CopyRect(rTo, b.Canvas, rFrom);
  finally
    b.Free;
  end;


 { oCanvas.Handle:=0;
  oCanvas.Free;
}
end;


//---------------------------------------------------------
procedure img_DrawColorRect_OnCanvas  (aCanvas: TCanvas;
                                       aRect: TRect;
                                       aBackColor,aColor: integer;
                                       aOffset: integer);
//---------------------------------------------------------
var
  oCanvas: TCanvas;
  iColor: integer;
  iColor2: integer;
begin
{
  oCanvas:=TCanvas.Create;
  oCanvas.Handle:=aHandle;
}
 // iColor :=aCanvas.Brush.Color;
 // iColor2:=aCanvas.Pen.Color;

  aCanvas.Brush.Color:=aBackColor;
  aCanvas.FillRect(aRect);

  InflateRect (aRect,-aOffset,-aOffset);

//  oCanvas.Brush.Color:=aBackColor;
//  oCanvas.FillRect(aRect);
  aCanvas.Brush.Color:=aColor;
  aCanvas.FillRect(aRect);
  aCanvas.Brush.Color:=0;
  aCanvas.FrameRect(aRect);

 // aCanvas.Brush.Color:=iColor;
 // aCanvas.Pen.Color:=iColor2;

 { oCanvas.Handle:=0;
  oCanvas.Free;}

end;




//-------------------------------------------------
procedure img_DrawRect(aCanvas: TCanvas; aRect: TRect; aColor,aBackColor: integer; aBorderColor: integer=clBlack);
//-------------------------------------------------
var
  iOldPenColor: integer;
begin
  iOldPenColor:=aCanvas.Pen.Color;

  aCanvas.Brush.Color:=aBackColor;
  aCanvas.Pen.Color:=aBackColor;
  aCanvas.Rectangle (aRect);

  InflateRect (aRect, -2,-2);
  aCanvas.Pen.Width:=0;
  aCanvas.Pen.Color:=aBorderColor;
  aCanvas.Rectangle (aRect);

  aCanvas.Brush.Color:=aColor;
  aCanvas.Rectangle (aRect);

  aCanvas.Pen.Color := iOldPenColor;
end;


 (*

//------------------------------------------------------
procedure SaveBitmapToGIF (aBitmap: Graphics.TBitmap; aFileName: String);
//------------------------------------------------------
var obj : TGIFImage;
begin
  obj:=TGIFImage.Create;
  obj.Transparent:=true;
//  obj.BackgroundColor:=clWhite;
  obj.BackgroundColor:=clBlack;

  obj.Assign (aBitmap);

  obj.Transparent:=true;
  obj.BackgroundColor:=clBlack;

{  aBitmap.Transparent:=True;
  aBitmap.TransparentMode:=tmFixed;
  aBitmap.TransparentColor:=clWhite;
 }
  obj.SaveToFile (ChangeFileExt(aFileName,'.gif'));
  obj.Free;
end;

*)

  {
//------------------------------------------------------
procedure SaveBitmapToJPG (aBitmap:Graphics.TBitmap; aFileName: String);
//------------------------------------------------------
var obj : TJPEGImage; // GifImage;
begin
  obj:=TJPEGImage.Create;
  obj.Assign (aBitmap);
//  obj.Transparent:=True;
 // obj.CompressionQuality:=100;
//  obj.Performance:=jpBest Quality;
 // obj.Smoothing:=False;
  obj.SaveToFile(ChangeFileExt(aFileName,'.jpg'));
  obj.Free;
end;
}

 {
//------------------------------------------------------
procedure img_SaveBitmapToFile (aBitmap: Graphics.TBitmap; aFileName: String);
//------------------------------------------------------
var i: TPixelFormat;
  sExt: string;
begin
  ForceDirByFileName (aFileName);

  sExt:=LowerCase(ExtractFileExt(aFileName));

  if sExt='.tif' then Bmp2tiff.WriteTiffToFile (aFileName, aBitmap ) else



  if sExt='.tif' then Bmp2tiff.WriteTiffToFile (aFileName, aBitmap ) else
  if sExt='.jpg' then SaveBitmapToJPG (aBitmap, aFileName) else
  if sExt='.gif' then SaveBitmapToGIF (aBitmap, aFileName) else
  if sExt='.bmp' then begin
                      aBitmap.PixelFormat:=pf32bit;
                      aBitmap.SaveToFile (ChangeFileExt(aFileName,'.bmp'));
                 end else
    raise Exception.Create('');
   ;
end;
  }

//---------------------------------------------------------
procedure img_FillRectGradient (aHandle: THandle; aRect:TRect; aBeginColor,aEndColor: integer);
//---------------------------------------------------------
var i,iMin,iMax:integer;
  oCanvas: TCanvas;

begin
  oCanvas:=TCanvas.Create;
  oCanvas.Handle:=aHandle;


  oCanvas.Pen.Width:=1;
  iMin:=aRect.Left;  iMax:=aRect.Right;

  for i:=iMin to iMax do
  begin
    oCanvas.Pen.Color:=img_MakeGradientColor (i, iMin,iMax, aBeginColor,aEndColor);
    oCanvas.MoveTo(i, aRect.Top);
    oCanvas.LineTo(i, aRect.Bottom);
  end;

  oCanvas.Handle:=0;
  oCanvas.Free;

end;



//---------------------------------------------------------
procedure img_FillRectGradient_OnCanvas (aCanvas: TCanvas; aRect:TRect; aBeginColor,aEndColor: integer);
//---------------------------------------------------------
var i,iMin,iMax:integer;
begin

  aCanvas.Pen.Width:=1;
  iMin:=aRect.Left;
  iMax:=aRect.Right;

  for i:=iMin to iMax do
  begin
    aCanvas.Pen.Color:=img_MakeGradientColor (i, iMin,iMax, aBeginColor,aEndColor);
    aCanvas.MoveTo(i, aRect.Top);
    aCanvas.LineTo(i, aRect.Bottom);
  end;

end;




//---------------------------------------------------------
function img_ColorFromString(Value: String): integer;
//---------------------------------------------------------
begin
  if Length(Value)>1 then
    if Value[1]='#' then Value[1]:='$';
  Result:=StrToIntDef (Value, -1);

end;




//---------------------------------------------------------
procedure FillRectGradient_test (aImage: TImage;
                                 aMinValue,aMaxValue: double;
                                 aBeginColor,aEndColor: integer);
//---------------------------------------------------------
var iWidth,i,iMin,iMax: integer;
   rt: TRect;
   d: double;
begin
  aImage.Canvas.Pen.Width:=1;

  rt:=aImage.ClientRect;
  iMin:=rt.Left;  iMax:=rt.Right;

  iWidth:=rt.Right - rt.Left;

  for i:=0 to iWidth-1 do
  begin
    d:=aMinValue + i * ((aMaxValue - aMinValue) / iWidth);

//    aImage.Canvas.Pen.Color:=MakeGradientColor (d, aMinValue,aMaxValue,
//                                                aBeginColor,aEndColor);


    aImage.Canvas.Pen.Color:=img_MakeGradientColor_ln (d, aMinValue,aMaxValue,
                                                aBeginColor,aEndColor);
    aImage.Canvas.MoveTo (rt.Left + i, rt.Top);
    aImage.Canvas.LineTo (rt.Left + i, rt.Bottom);
  end;
end;



//---------------------------------------------------------
function img_MakeGradientColor (aValue,aMinValue,aMaxValue: double;
                            aBeginColor,aEndColor     : integer): integer;
//---------------------------------------------------------
var r1,g1,b1,r2,g2,b2: byte;
begin
  if (aValue<aMinValue) then aValue:=aMinValue;
  if (aValue>aMaxValue) then aValue:=aMaxValue;

  aMaxValue:=aMaxValue-aMinValue;
  aValue   :=aValue-aMinValue;

  ColorTo_R_G_B(aBeginColor, R1,G1,B1);
  ColorTo_R_G_B (aEndColor,   R2,G2,B2);

  if Abs(aMaxValue) > 0.0001 then
    Result:=RGB(Byte(Trunc(R1 + (R2-R1) * aValue / aMaxValue)),
                Byte(Trunc(G1 + (G2-G1) * aValue / aMaxValue)),
                Byte(Trunc(B1 + (B2-B1) * aValue / aMaxValue)))
  else Result:=0;
end;

//---------------------------------------------------------
function img_MakeGradientColor_ln (aValue,aMinValue,aMaxValue: double;
                               aBeginColor,aEndColor     : integer): integer;
//---------------------------------------------------------
var r1,g1,b1,r2,g2,b2: byte;
    dDiff,dFullDiff,dValue,dMin,dMax: double;
begin
//  if (aValue<aMinValue) then aValue:=aMinValue;
//  if (aValue>aMaxValue) then aValue:=aMaxValue;

  dMin  :=Ln(aMinValue);
  dMax  :=Ln(aMaxValue);
  dValue:=Ln(aValue);
  dFullDiff:=Abs(dMax - dMin);
  dDiff:=Abs(dValue-dMax); // - dMin;

  ColorTo_R_G_B(aEndColor, R1,G1,B1);
  ColorTo_R_G_B(aBeginColor,   R2,G2,B2);

  if dFullDiff > 0.0001 then
    Result:=RGB(Byte(Trunc(R1 + (R2-R1) * dDiff / dFullDiff)),
                Byte(Trunc(G1 + (G2-G1) * dDiff / dFullDiff)),
                Byte(Trunc(B1 + (B2-B1) * dDiff / dFullDiff)))
  else Result:=0;
end;



//--------------------------------------------------------
procedure ColorTo_R_G_B(Value: TColor; var R,G,B: byte);
//--------------------------------------------------------
var cl: packed record
        case z:byte of
          0: (int: TColor);
          1: (RGB: array[0..4] of byte);
        end;
begin
  cl.int:=Value;
  R:=cl.rgb[0];  G:=cl.rgb[1];  B:=cl.rgb[2];
end;



function ColorToHEX (Value: integer): String;
var R,G,B: byte;
begin
  ColorTo_R_G_B (Value, R,G,B);
  Result:=Format('%2x%2x%2x',[r,g,b]);
  Result:=ReplaceStr (Result,' ','0');
end;


function img_GetRandomColor(): integer;
var r,g,b:byte;
begin
  r:=Random(255); g:=Random(255); b:=Random(255);
  Result:=RGB(r,g,b);
end;


function MakeRGB (r,g,b: integer): integer;
begin
  Result:=RGB (r,g,b);
end;


//============================================================================//
// ����� ����� � ������� ������������ �������
//============================================================================//
function Dlg_SelectColor(aColor: TColor): TColor;
var
  oColorDialog: TColorDialog;
begin
  oColorDialog:=TColorDialog.Create(Application);

  oColorDialog.Color := aColor;
  if oColorDialog.Execute then
    Result := oColorDialog.Color
  else
    Result :=0;

  oColorDialog.Free;
end;


//---------------------------------------------------------
procedure img_FillRectGradientVert (aHandle: THandle;//aCanvas: TCanvas;
                              aRect: TRect;
                              aBeginColor,aEndColor: integer);
//---------------------------------------------------------
var
  i,iMin,iMax:integer;
  oCanvas: TCanvas;
begin

  oCanvas:=TCanvas.Create;
  oCanvas.Handle:=aHandle;

  oCanvas.Pen.Width:=1;
  iMin:=aRect.Top;  iMax:=aRect.Bottom;
  for i:=iMin to iMax do begin
    oCanvas.Pen.Color:=img_MakeGradientColor (i, iMin,iMax, aEndColor,aBeginColor);
    oCanvas.MoveTo(aRect.Left, i);
    oCanvas.LineTo(aRect.Right,i );
  end;

  oCanvas.Handle:=0;
  oCanvas.Free;

end;



//-------------------------------------------------------------------
procedure img_InsertScaleRuleIntoJPG(aSrcFileName, aDestFileName: string;
    aScale: integer);
//-------------------------------------------------------------------
var
  oImage: TJPEGImage;
  oBitmap: Graphics.TBitmap;

begin

  oImage:=TJPEGImage.Create;
  oImage.LoadFromFile(aSrcFileName);

  oBitmap := Graphics.TBitmap.Create();
  oBitmap.Assign(oImage);


  img_InsertScaleRuleIntoBitmap (oBitmap, aScale, 20);


  oImage.Assign(oBitmap);
  oImage.SaveToFile(aDestFileName);

  oBitmap.Free;
  oImage.Free;
end;



//-------------------------------------------------------------------
procedure img_InsertScaleRuleIntoBMP(aSrcFileName, aDestFileName: string;
    aScale: integer);
//-------------------------------------------------------------------
var
  oBitmap: Graphics.TBitmap;
begin
  oBitmap := Graphics.TBitmap.Create();
  oBitmap.LoadFromFile(aSrcFileName);

  img_InsertScaleRuleIntoBitmap (oBitmap, aScale, 20);

  oBitmap.SaveToFile(aDestFileName);

  FreeAndNil(oBitmap);

//  oBitmap.Free;
end;



//-------------------------------------------------------------------
procedure img_InsertScaleRuleIntoBitmap(aBitmap: Graphics.TBitmap; aScale,
    aFontSize: integer);
//-------------------------------------------------------------------
const
//  DEF_LEFT_OFFSET = 35;
 // DEF_BOTTOM_OFFSET = 35;
  DEF_RULE_WIDTH = 5;  

  DEF_LEFT_OFFSET = 35;
  DEF_BOTTOM_OFFSET = 45;


var
  iY: integer;
  s: string;
  iX: integer;
  iStartX: integer;
  iStartY: integer;
  iSm: integer;
  iH: integer;
 // e: Double;
  i: integer;
  tr: TRect;
begin

  aBitmap.Canvas.Pen.Color:=clWhite;
  aBitmap.Canvas.Brush.Color:=clWhite;
  aBitmap.Canvas.Rectangle(0,aBitmap.Height-DEF_BOTTOM_OFFSET, aBitmap.Width, aBitmap.Height);
  aBitmap.Canvas.Rectangle(0,0, DEF_LEFT_OFFSET, aBitmap.Height);

  aBitmap.Canvas.Pen.Color:=clBlack;

  iSm:=round(Screen.PixelsPerInch / 2.54);
  iStartY:=aBitmap.Height - DEF_BOTTOM_OFFSET;

  //-------------------------------
  // ����� �������
  //-------------------------------


  for i := 0 to 100 do
  begin
    iY:=iStartY - i*iSm;

    if i mod 2=0 then
      aBitmap.Canvas.Brush.Color:=clBlack
    else
      aBitmap.Canvas.Brush.Color:=clWhite;

    tr.Left  :=DEF_LEFT_OFFSET;
    tr.Top   :=iY-iSm;
    tr.Right :=DEF_LEFT_OFFSET + DEF_RULE_WIDTH;
    tr.Bottom:=iY;

    aBitmap.Canvas.Rectangle(tr);

    case aScale of
       10000: if (i mod 10)= 0 then s:=Format('%d',[Round(0.1*i)])
                               else s:=Format('%1.1f',[0.1*i]);
      100000: s:=Format('%d',[i]);
      200000: s:=Format('%d',[i*2]);
      400000: s:=Format('%d',[i*4]);
    else
      s:='';
    end;
             

    aBitmap.Canvas.Brush.Color:=clWhite;
    aBitmap.Canvas.TextOut(DEF_LEFT_OFFSET - 20, tr.Top, s);//Format('%d', [i]));

    if tr.Top<0 then
      Break;
  end;

  //-------------------------------
  // ������ �������
  //-------------------------------

  iStartX:=DEF_LEFT_OFFSET;

  for i := 0 to 100 do
  begin
    iX:=iStartX + i*iSm;

    if i mod 2=0 then
      aBitmap.Canvas.Brush.Color:=clBlack
    else
      aBitmap.Canvas.Brush.Color:=clWhite;

    tr.Left  :=iX;
    tr.Top   :=aBitmap.Height - DEF_BOTTOM_OFFSET - DEF_RULE_WIDTH;
    tr.Right :=iX+iSm;
    tr.Bottom:=aBitmap.Height - DEF_BOTTOM_OFFSET;

    aBitmap.Canvas.Rectangle(tr);

    case aScale of
       10000: if (i mod 10)= 0 then s:=Format('%d',[Round(0.1*i)])
                               else s:=Format('%1.1f',[0.1*i]);

      100000: s:=Format('%d',[i]);
      200000: s:=Format('%d',[i*2]);
      400000: s:=Format('%d',[i*4]);
    else
      s:='';
    end;

    aBitmap.Canvas.Brush.Color:=clWhite;
    aBitmap.Canvas.TextOut(iX, tr.Bottom + 3, s);

    if tr.Left>aBitmap.Width then
      Break;

  end;

  s:= Format('�������: 1:%d', [aScale]);

  aBitmap.Canvas.Font.Size:=12;
  aBitmap.Canvas.TextOut(0, aBitmap.Height - 15 - 5, s);

end;


{
procedure img_SaveBitmapToFile_file(aBitmapFileName, aFileName: String);
var
  oBitmap: Graphics.TBitmap;

begin
  oBitmap:=Graphics.TBitmap.Create;
  oBitmap.LoadFromFile(ChangeFileExt(aBitmapFileName, '.bmp'));

  img_SaveBitmapToFile (oBitmap, aFileName);

  FreeAndNil(oBitmap);

end;
}






end.


{
//============================================================================//
//  ��������� ��� ���������� ��������
//    - ������� Bitmap � ������� �� ���� ������
//    - �������� ������ � ������� ������ �������� ������
//
//============================================================================//
procedure img_DrawCharacter (aHandle: THandle; //ACanvas: TCanvas;
    ARect: TRect; aBackColor: integer;
    aFontName: String; aCharacter: Integer );
//-------------------------------------------------------------------
var n: integer;
    b: Graphics.TBitmap;

    rFrom,
    rTo: TRect;
    oCanvas: TCanvas;

begin
  if aFontName='' then Exit;

  n:=aCharacter;

  oCanvas:=TCanvas.Create;
  oCanvas.Handle:=aHandle;


  b := Graphics.TBitmap.Create;
  try
    oCanvas.Pen.Color:=clBlack;
    oCanvas.Brush.Color:=aBackColor;
    oCanvas.FillRect(aRect);

    b.Width  := 100;
    b.Height := 100;
    b.Canvas.Font.Name := aFontName;
    b.Canvas.Font.Size:=12;
    b.Canvas.TextOut(0,0, Char(n));

    rFrom.Left   := 0;
    rFrom.Top    := 0;
    rFrom.Right  := b.Canvas.TextWidth(Char(n));
    rFrom.Bottom := b.Canvas.TextHeight(Char(n));

    rTo := ARect;
    rTo.Right  := rTo.Left + (rFrom.Right - rFrom.Left);
    rTo.Bottom := rTo.Top + (rFrom.Bottom - rFrom.Top);
    oCanvas.CopyRect(rTo, b.Canvas, rFrom);
  finally
    b.Free;
  end;


  oCanvas.Handle:=0;
  oCanvas.Free;

end;
}

{
//---------------------------------------------------------
procedure img_DrawColorRect (aHandle: THandle;    //ACanvas: TCanvas;
                             aRect: TRect;
                             aBackColor,aColor: integer;
                             aOffset: integer);
//---------------------------------------------------------
var
  oCanvas: TCanvas;
  iColor: integer;
  iColor2: integer;
begin

  oCanvas:=TCanvas.Create;
  oCanvas.Handle:=aHandle;

  iColor:=oCanvas.Brush.Color;
  iColor2:=oCanvas.Pen.Color;

  oCanvas.Brush.Color:=aBackColor;
  oCanvas.FillRect(aRect);

  InflateRect (aRect,-aOffset,-aOffset);

//  oCanvas.Brush.Color:=aBackColor;
//  oCanvas.FillRect(aRect);
  oCanvas.Brush.Color:=aColor;
  oCanvas.FillRect(aRect);
  oCanvas.Brush.Color:=0;
  oCanvas.FrameRect(aRect);

  oCanvas.Brush.Color:=iColor;
  oCanvas.Pen.Color:=iColor2;

  oCanvas.Handle:=0;
  oCanvas.Free;

end;
}

{
//-------------------------------------------------
procedure img_DrawRect  (aHandle: THandle;
                         aRect: TRect;
                         aColor,aBackColor: integer;
                         aBorderColor: integer);
//-------------------------------------------------
var
  oCanvas: TCanvas;
begin

  oCanvas:=TCanvas.Create;
  oCanvas.Handle:=aHandle;


  oCanvas.Brush.Color:=aBackColor;

  oCanvas.Pen.Color:=aBackColor;
  oCanvas.Rectangle (aRect);

  InflateRect (aRect, -2,-2);
  oCanvas.Pen.Width:=0;
  oCanvas.Pen.Color:=aBorderColor;
  oCanvas.Rectangle (aRect);

  oCanvas.Brush.Color:=aColor;
  oCanvas.Rectangle (aRect);

  oCanvas.Handle:=0;
  oCanvas.Free;
end;


}
{
//-------------------------------------------------
procedure img_DrawRect (aCanvas: TCanvas; aRect: TRect;
                        aColor,aBackColor: integer;
                        aBorderColor: integer=clBlack);
//-------------------------------------------------
begin
  aCanvas.Brush.Color:=aBackColor;
  aCanvas.Pen.Color:=aBackColor;
  aCanvas.Rectangle (aRect);

  InflateRect (aRect, -2,-2);
  aCanvas.Pen.Width:=0;
  aCanvas.Pen.Color:=aBorderColor;
  aCanvas.Rectangle (aRect);

  aCanvas.Brush.Color:=aColor;
  aCanvas.Rectangle (aRect);
end;}


{

//---------------------------------------------------------
procedure img_DrawColorRect (aHandle: THandle;//ACanvas: TCanvas;
                             aRect:TRect;
                             aBackColor,aColor: integer;
                             aOffset: integer);
//---------------------------------------------------------
var
  oCanvas: TCanvas;
  iColor: integer;
begin

  oCanvas:=TCanvas.Create;
  oCanvas.Handle:=aHandle;

  iColor:=oCanvas.Brush.Color;

  oCanvas.Brush.Color:=aBackColor;
  oCanvas.FillRect(aRect);

  InflateRect (aRect,-aOffset,-aOffset);

//  oCanvas.Brush.Color:=aBackColor;
//  oCanvas.FillRect(aRect);
  oCanvas.Brush.Color:=aColor;
  oCanvas.FillRect(aRect);
  oCanvas.Brush.Color:=0;
  oCanvas.FrameRect(aRect);

  oCanvas.Brush.Color:=iColor;

  oCanvas.Handle:=0;
  oCanvas.Free;

end;}



{

//============================================================================//
//  ��������� ��� ���������� ��������
//    - ������� Bitmap � ������� �� ���� ������
//    - �������� ������ � ������� ������ �������� ������
//
//============================================================================//
procedure img_DrawCharacter (aHandle: THandle; //ACanvas: TCanvas;
                             ARect: TRect; aBackColor: integer;
                             aFontName: string; aCharacter: Integer );
//-------------------------------------------------------------------
var
    n: integer;
    b: Graphics.TBitmap;

    oCanvas: TCanvas;

    rFrom,
    rTo: TRect;

begin
  if aFontName='' then Exit;

  n:=aCharacter;

  oCanvas:=TCanvas.Create;
  oCanvas.Handle:=aHandle;


  b := Graphics.TBitmap.Create;
  try
    oCanvas.Brush.Color:=aBackColor;
    oCanvas.FillRect(aRect);

    b.Width  := 100;
    b.Height := 100;
    b.Canvas.Font.Name := aFontName;
    b.Canvas.Font.Size:=12;
    b.Canvas.TextOut(0,0, Char(n));

    rFrom.Left   := 0;
    rFrom.Top    := 0;
    rFrom.Right  := b.Canvas.TextWidth(Char(n));
    rFrom.Bottom := b.Canvas.TextHeight(Char(n));

    rTo := ARect;
    rTo.Right  := rTo.Left + (rFrom.Right - rFrom.Left);
    rTo.Bottom := rTo.Top + (rFrom.Bottom - rFrom.Top);
    oCanvas.CopyRect(rTo, b.Canvas, rFrom);
  finally
    b.Free;
  end;


  oCanvas.Handle:=0;
  oCanvas.Free;

end;



}

{
  procedure img_DrawRect (aCanvas: TCanvas; aRect: TRect;
                          aColor,aBackColor: integer;
                          aBorderColor: integer=clBlack);
}

{  procedure img_DrawRect (aHandle: THandle;
                           aRect: TRect;
                           aColor,aBackColor: integer;
                           aBorderColor: integer); //
}


{  procedure img_DrawColorRect (aHandle: THandle;    //ACanvas: TCanvas;
                               aRect: TRect;
                               aBackColor,aColor: integer;
                               aOffset: integer);
}
{
  procedure img_DrawColorRect (aHandle: THandle;//ACanvas: TCanvas;
                               aRect: TRect;
                               aBackColor,aColor: integer; aOffset: integer=2);
}

{
  procedure img_DrawCharacter (aHandle: THandle; //ACanvas: TCanvas;
                               ARect: TRect; aBackColor: integer;
                               aFontName: string; aCharacter: Integer );
}


{
exports
//  img_DrawColorRect,
//  img_DrawCharacter,

  Dlg_SelectColor,

 // img_DrawRect,

  SaveBitmapToJPG,
  SaveBitmapToGIF,

  img_SaveBitmapToFile ,
//  img_DrawColorRect    ,
  img_FillRectGradient,
  img_ColorFromString,
  ColorTo_R_G_B,
  ColorToHEX,
  MakeRGB,
  img_GetRandomColor,
  img_MakeGradientColor,
  img_MakeGradientColor_ln,
  img_FillRectGradientVert,
  
  FillRectGradient_test
  ;}

