unit u_reg;

interface

uses  Registry,IniFiles,Classes,SysUtils ;


  procedure ini_WriteInteger (aFileName: string; aSection,aIdent: string; aValue: integer);
  function  ini_ReadInteger (aFileName, aSection, aIdent: string; aDefValue: integer): Integer;

  procedure ini_WriteBool (aFileName: string; aSection,aIdent: string; aValue: boolean);
  function  ini_ReadBool  (aFileName, aSection, aIdent: string; aDefValue: boolean): boolean;

  procedure ini_WriteString (aFileName: string; aSection,aIdent: string; aValue: String);
  function  ini_ReadString (aFileName, aSection, aIdent: string; aDefValue: String): String;

  procedure ini_WriteDateTime (aFileName: string; aSection,aIdent: string; aValue: TDateTime);
  function  ini_ReadDateTime  (aFileName, aSection, aIdent: string; aDefValue: TDateTime): TDateTime;


  procedure reg_WriteBool (aRegPath,aIdent: string; aValue: boolean);
  function  reg_ReadBool  (aRegPath,aIdent: string; aDefValue: boolean): Boolean;

  procedure reg_WriteInteger (aRegPath, aIdent: string; aValue: integer);       
  function  reg_ReadInteger  (aRegPath, aIdent: string; aDefault: integer): integer;

  procedure reg_WriteString (aRegPath,aIdent: string; aValue: string);
  function  reg_ReadString  (aRegPath,aIdent: string; aDefValue: string): string;


  procedure reg_SaveToRegistry(aPath: string; aStrings: TStrings);
  procedure reg_LoadFromRegistry(aPath: string; aStrings: TStrings);



const
//  PROP_ACTIVE_PAGE_INDEX = 'ActivePageIndex';
  PROP_ACTIVE_PAGE       = 'ActivePage';
  PROP_INITIAL_DIR       = 'InitialDir';
  PROP_HEIGHT            = 'height';
  PROP_WIDTH             = 'width';
  PROP_TEXT              = 'text';
  PROP_VALUE             = 'value';
  PROP_FILENAME          = 'filename';
  PROP_ITEMINDEX         = 'itemindex';
  PROP_CHECKED           = 'checked';
  PROP_COLOR             = 'color';
  PROP_ITEMS             = 'items';


//=============================================================
implementation
//=============================================================



procedure ini_WriteString (aFileName: string; aSection,aIdent: string; aValue: String);
var
  oIni: TIniFile;
begin
  oIni:=TIniFile.Create (aFileName);
  oIni.Writestring(aSection,aIdent,aValue);
  oIni.Free;
end;

function ini_ReadString (aFileName, aSection, aIdent: string; aDefValue: String): String;
var
  oIni: TIniFile;
begin
  ForceDirectories(ExtractFileDir(aFileName));

  oIni:=TIniFile.Create (aFileName);
  Result := oIni.Readstring(aSection,aIdent,aDefValue);
  oIni.Free;
end;


procedure ini_WriteBool (aFileName: string; aSection,aIdent: string; aValue: boolean);
var
  oIni: TIniFile;
begin
  ForceDirectories(ExtractFileDir(aFileName));

  oIni:=TIniFile.Create (aFileName);
  oIni.WriteBool(aSection,aIdent,aValue);
  oIni.Free;
end;


function ini_ReadBool (aFileName, aSection, aIdent: string; aDefValue: boolean): boolean;
var
  oIni: TIniFile;
begin
  oIni:=TIniFile.Create (aFileName);
  Result := oIni.ReadBool(aSection,aIdent,aDefValue);
  oIni.Free;
end;


procedure ini_WriteDateTime (aFileName: string; aSection,aIdent: string; aValue: TDateTime);
var
  oIni: TIniFile;
begin
  ForceDirectories(ExtractFileDir(aFileName));

  oIni:=TIniFile.Create (aFileName);
  oIni.WriteDateTime (aSection,aIdent,aValue);
  oIni.Free;
end;


function ini_ReadDateTime  (aFileName, aSection, aIdent: string; aDefValue: TDateTime): TDateTime;
var
  oIni: TIniFile;
begin
  oIni:=TIniFile.Create (aFileName);
  Result := oIni.ReadDateTime(aSection,aIdent,aDefValue);
  oIni.Free;
end;


procedure ini_WriteInteger (aFileName: string; aSection,aIdent: string; aValue: integer);
var
  oIni: TIniFile;
begin
  ForceDirectories(ExtractFileDir(aFileName));

  oIni:=TIniFile.Create (aFileName);
  oIni.WriteInteger(aSection,aIdent,aValue);
  oIni.Free;
end;



function ini_ReadInteger(aFileName, aSection, aIdent: string; aDefValue: integer): Integer;
var
  oIni: TIniFile;
begin
  oIni:=TIniFile.Create (aFileName);
  Result := oIni.ReadInteger(aSection,aIdent,aDefValue);
  FreeAndNil(oIni);

end;


function reg_ReadBool(aRegPath,aIdent: string; aDefValue: boolean): Boolean;
var
  oRegIni: TRegistryIniFile;
begin
  Assert((aRegPath<>'') and (aIdent<>''),'(aRegPath<>'''') and (aIdent<>'''')');


  oRegIni:=TRegistryIniFile.Create (aRegPath);
  Result := oRegIni.ReadBool ('', aIdent, aDefValue);
  FreeAndNil(oRegIni);

end;


procedure reg_WriteBool(aRegPath,aIdent: string; aValue: boolean);
var
  oRegIni: TRegistryIniFile;
begin
  Assert((aRegPath<>'') and (aIdent<>''),'(aRegPath<>'''') and (aIdent<>'''')');

  oRegIni:=TRegistryIniFile.Create (aRegPath);
  oRegIni.WriteBool ('', aIdent, aValue);
  FreeAndNil(oRegIni);

end;


// -------------------------------------------------------------------
procedure reg_WriteString (aRegPath, aIdent: string; aValue: string);
// -------------------------------------------------------------------
var oReg: TRegIniFile;
begin
  Assert((aRegPath<>'') and (aIdent<>''),'(aRegPath<>'''') and (aIdent<>'''')');

  try
    oReg:= TRegIniFile.Create(aRegPath);
    oReg.WriteString('', aIdent, aValue);

  finally
  FreeAndNil(oReg);

  end;
end;

// -------------------------------------------------------------------
function reg_ReadString(aRegPath,aIdent: string; aDefValue: string): string;
// -------------------------------------------------------------------
var oReg: TRegIniFile;
begin
  Assert(aRegPath<>'','aRegPath<>''''');

  try
    oReg:= TRegIniFile.Create(aRegPath);
    Result:= oReg.ReadString('', aIdent, aDefValue);
  except
    Result:=aDefValue;
  end;

  FreeAndNil(oReg);

end;



// -------------------------------------------------------------------
procedure reg_WriteInteger(aRegPath, aIdent: string; aValue: integer);
// -------------------------------------------------------------------
var oReg: TRegIniFile;
begin
  Assert((aRegPath<>'') and (aIdent<>''), '(aRegPath<>'''') and (aIdent<>'''')');

  try
    oReg:= TRegIniFile.Create(aRegPath);
    oReg.WriteInteger('', aIdent, aValue);
  finally
   FreeAndNil(oReg);

  end;
end;

// -------------------------------------------------------------------
function reg_ReadInteger(aRegPath, aIdent: string; aDefault: integer): integer;
// -------------------------------------------------------------------
var oReg: TRegIniFile;
 // s: string;
 // v: Variant;
begin
  Assert((aRegPath<>'') and (aIdent<>''),'(aRegPath<>'''') and (aIdent<>'''')');

  try
    oReg:= TRegIniFile.Create(aRegPath);
   // V:= oReg.ReadString('', aIdent, '');
    Result:= oReg.ReadInteger('', aIdent, aDefault);
  except
    Result:=aDefault;
  end;

  FreeAndNil(oReg);

end;
              

procedure reg_SaveToRegistry(aPath: string; aStrings: TStrings);
var
  I: integer;
begin
  with TRegIniFile.Create(aPath) do
  begin
    WriteInteger('','count', aStrings.Count);

    for I := 0 to aStrings.Count - 1 do
      WriteString('','item'+intToStr(i), aStrings[i]);
   // Wr

//    ReadSectionValues('items', Files.Items);
    Free;
  end;


//  with  do
//  begin
//
//  end;

end;


procedure reg_LoadFromRegistry(aPath: string; aStrings: TStrings);
var
  i,iCount: Integer;
begin
  with TRegIniFile.Create(aPath) do
  begin
    iCount:=ReadInteger('','count',0);

    for I := 0 to iCount - 1 do
      aStrings.Add(ReadString('','item'+intToStr(i), ''));

    Free;
  end;
end;







end.





 
{
exports
  ini_WriteInteger,
  ini_ReadInteger,

  ini_WriteBool,
  ini_ReadBool,

  ini_WriteString,
  ini_ReadString,

  ini_WriteDateTime,
  ini_ReadDateTime,

  reg_WriteBool,
  reg_ReadBool,

  reg_WriteInteger,
  reg_ReadInteger,

  reg_WriteString,
  reg_ReadString;}



{
const
//  PROP_ACTIVE_PAGE_INDEX = 'ActivePageIndex';
  PROP_ACTIVE_PAGE       = 'ActivePage';
  PROP_INITIAL_DIR       = 'InitialDir';
  PROP_HEIGHT            = 'height';
  PROP_WIDTH             = 'width';
  PROP_TEXT              = 'text';
  PROP_VALUE             = 'value';
  PROP_FILENAME          = 'filename';
  PROP_ITEMINDEX         = 'itemindex';
  PROP_CHECKED           = 'checked';
  PROP_COLOR             = 'color';
  PROP_ITEMS             = 'items';

}
