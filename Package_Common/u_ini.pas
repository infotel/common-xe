unit u_ini;

interface

uses DB, Forms,SysUtils, IniFiles;

  procedure ini_LoadFieldCaptions(aDataSet: TDataSet; aSection: string; aFileName: string =
      '');


implementation


procedure ini_LoadFieldCaptions(aDataSet: TDataSet; aSection: string; aFileName: string =
    '');
var
  oIni: TIniFile;
  sFieldName: string;
  s: string;
  i: integer;
begin
  Assert(Assigned(aDataSet), 'db_LoadFieldCaptions - Assigned(aDataSet)');

  if aFileName='' then
    aFileName:=ChangeFileExt(Application.ExeName, '.ini');

  oIni:=TIniFile.Create (aFileName);

  for i:=0 to aDataSet.Fields.Count-1 do
  begin
    sFieldName := aDataSet.Fields[i].FieldName;

    s:= oIni.ReadString(aSection,sFieldName,'');

    if s<>'' then
      aDataSet.Fields[i].DisplayLabel :=s;
  end;

//    if not Eq(Fields[i].FieldName, 'RecId') then

//  Result := oIni.ReadInteger(aSection,aIdent,aDefValue);

  oIni.Free;
end;


end.
