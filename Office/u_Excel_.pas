
unit u_Excel_;

interface
uses
   Variants, DB, SysUtils, AdoDB, Windows, ExcelXP
   ;


procedure ExportDataSetToExcel(aDataSet: TDataSet);


implementation


procedure ExportDataSetToExcel(aDataSet: TDataSet);
var
  x: integer;
  i: integer;
  vSheet: Variant;
  vData: Variant;

  oExcelApplication1: TExcelApplication;

begin
   oExcelApplication1:=TExcelApplication.Create(nil);

   oExcelApplication1.Connect;
//      i := ExcelApplication1.Workbooks.Count;

   oExcelApplication1.Workbooks.Add(xlWBATWorksheet,0);
//    i := ExcelApplication1.Workbooks.Count;

//      ExcelApplication1.ActiveWorkbook. := 'aaaaaaaaaaaaa';

    i := oExcelApplication1.ActiveWorkbook.Sheets.Count;

 //   oExcelApplication1.ActiveWorkbook.Name  := '';


    vSheet := oExcelApplication1.ActiveWorkbook.ActiveSheet;
  //  vSheet.Name  := 'aaaaaaaaaaaaa';

 //   ExcelApplication1.ActiveWorkbook.Sheets.Add(NULL,NULL, NULL,NULL,NULL);
   // i := ExcelApplication1.ActiveWorkbook.Sheets.Count;

  vData := VarArrayCreate( [0, aDataSet.RecordCount, 0, aDataSet.FieldCount-1], varVariant);

  for x := 0 to aDataSet.FieldCount - 1 do
    vData[ 0, x ] := aDataSet.Fields[x].FieldName;

(*  for x := 0 to aDataSet.FieldCount - 1 do
    vData[ 0, x ] := aDataSet.Fields[x].FieldLabel;
*)
  aDataSet.DisableControls;
  aDataSet.First;
  while not aDataSet.Eof do
  begin
     for x := 0 to aDataSet.FieldCount - 1 do
        vData[ aDataSet.RecNo, x] := aDataSet.Fields[x].Value;
     aDataSet.Next;
  end;

  aDataSet.EnableControls;


  vSheet.Range[ vSheet.Cells[1,1], vSheet.Cells[aDataSet.RecordCount + 1, aDataSet.FieldCount ]].Value := vData;
  vSheet.Range[ vSheet.Cells[1,1], vSheet.Cells[aDataSet.RecordCount + 1, aDataSet.FieldCount ]].Select;



  oExcelApplication1.Visible  [LOCALE_USER_DEFAULT] := True;
  oExcelApplication1.Disconnect;


  FreeAndNil(oExcelApplication1);


end;


end.