unit u_dll_img;

interface
uses
  Graphics;


const
  DEF_DLL_IMG = 'dll_img';


  procedure img_BMP_to_PNG(aBmpFileName, aPNGFileName: ShortString;
      aIsTransparent: boolean = false; aTransparentColor: TColor = 0); stdcall;
      external DEF_DLL_IMG;


  procedure img_BMP_to_JPEG(aBmpFileName, aJPEGFileName: ShortString);  stdcall;
      external DEF_DLL_IMG;

  procedure img_BMP_to_TIFF(aBmpFileName, aTifFileName: ShortString);  stdcall;
      external DEF_DLL_IMG;


  procedure img_Dlg(aAppHandle : integer);  stdcall;
      external DEF_DLL_IMG;


implementation

end.
