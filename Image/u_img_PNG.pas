unit u_img_PNG;

interface

uses Graphics,SysUtils, Windows, Dialogs, Forms, FileCtrl, extctrls,

//     PNGImage_;
     PNGImage;


  procedure img_BMP_to_PNG(aBmpFileName, aPNGFileName: String);



implementation

//------------------------------------------------------
procedure img_BMP_to_PNG(aBmpFileName, aPNGFileName: String);
//------------------------------------------------------
var
  oBMP: Graphics.TBitmap;

  oPNG:TPNGObject;
  i,j: Integer;
  pLine: pRGBLine;

//  pAlphaLine: pByteArray;
begin
  Assert(aPNGFileName<>'');


  aBmpFileName:= ChangeFileExt(aBmpFileName, '.bmp');
  aPNGFileName:= ChangeFileExt(aPNGFileName, '.png');

  Assert( FileExists( aBmpFileName));

//  DeleteFile(aPNGFileName);


  oBMP:= Graphics.TBitmap.Create;
 // oBMP.PixelFormat := pf16bit;
  try
    oBMP.LoadFromFile(aBmpFileName);
  except
  //  ShowMessage('�� ������� ��������� ����: '+aBmpFileName);

    raise Exception.Create('�� ������� ��������� ����: '+aBmpFileName);

    FreeAndNil(oBMP);
    Exit;
  end;

  oPNG:= TPNGObject.Create;
  oPNG.Transparent:= True;

  try
    oPNG.Assign(oBMP);
    oPNG.SaveToFile(aPNGFileName);

  except
    raise Exception.Create('�� ������� ��������� ����: '+aPNGFileName);

  end;


  FreeAndNil(oBMP);
  FreeAndNil(oPNG);

end;




end.
