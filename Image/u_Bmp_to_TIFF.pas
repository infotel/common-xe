unit u_Bmp_to_TIFF;

interface

uses Graphics,SysUtils, Windows, Dialogs, Forms, FileCtrl, Bmp2Tiff, Jpeg,

d_Test
;

//     PNGImage_;
//     PNGImage;



  procedure img_BMP_to_TIFF(aBmpFileName, aDestFileName: ShortString); stdcall;
  procedure img_BMP_to_JPEG(aBmpFileName, aJPEGFileName: ShortString); stdcall;
            


exports

  img_BMP_to_TIFF,
  img_BMP_to_JPEG;



implementation


procedure img_BMP_to_JPEG(aBmpFileName, aJPEGFileName: ShortString);
var
  JPEG: TJPEGImage;
  oBitmap: Graphics.TBitmap;
begin
  Assert(FileExists( aBmpFileName));

//  if not FileExists( aBmpFileName) then
//    Exit;

  oBitmap:= Graphics.TBitmap.Create;
  JPEG := TJPEGImage.Create ();

  try
    oBitmap.LoadFromFile(aBmpFileName);
    JPEG.Assign(oBitmap);
    JPEG.SaveToFile(aJPEGFileName);

   // Image1.Picture.Assign(JPEG);
  finally
    JPEG.Free;
    oBitmap.Free;

  //  Sysutils.DeleteFile (aBmpFileName);
  end;
end;



//------------------------------------------------------
procedure img_BMP_to_TIFF(aBmpFileName, aDestFileName: ShortString);
//------------------------------------------------------
var
  oBMP: Graphics.TBitmap;
  i,j: Integer;

begin
  Assert(aDestFileName<>'');


  aBmpFileName := ChangeFileExt(aBmpFileName, '.bmp');
  aDestFileName:= ChangeFileExt(aDestFileName, '.tiff');

  Assert( FileExists( aBmpFileName));


  oBMP:= Graphics.TBitmap.Create;
  oBMP.PixelFormat := pf32bit;
  try
    oBMP.LoadFromFile(aBmpFileName);
  except
    raise Exception.Create('�� ������� ��������� ����: '+aBmpFileName);
    oBMP.Free;
    Exit;
  end;


  WriteTiffToFile (aDestFileName, oBMP);


  oBMP.Free;

end;    //






end.