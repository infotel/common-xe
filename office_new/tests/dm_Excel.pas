unit dm_Excel;

interface

uses
  Windows, SysUtils, Classes, Forms, Variants, Excel2000, ExcelXP, OleServer;


  //http://www.delphikingdom.com/asp/viewitem.asp?catalogid=1274#04

type
  TdmExcel_file = class(TDataModule)
    ExcelApplication1: TExcelApplication;
    ExcelWorksheet1: TExcelWorksheet;
    ExcelWorkbook1: TExcelWorkbook;
  private
    FArrData: Variant;

    FStrArr: array of array of string;

//            s:=VarToStr(dmExcel.Cells[iRow,iCol]);


    FvWorkbook: _Workbook;

    function Get_CellValue(ARow, ACol: Cardinal): Variant;
  public
    Active : Boolean;

    RowCount: Integer;
    ColCount: Integer;

    class procedure Init;


    procedure Open(aFileName: string);
    procedure Close;

    procedure LoadWorksheetNames(aStrings: TStrings);

    function LoadWorksheetByIndex(aIndex: Integer): Boolean;

    property Cells[ARow: Cardinal; ACol: Cardinal]: Variant read Get_CellValue;

  end;

var
  dmExcel_file: TdmExcel_file;

implementation

{$R *.dfm}

class procedure TdmExcel_file.Init;
begin
  if not Assigned(dmExcel_file) then
    Application.CreateForm(TdmExcel_file, dmExcel_file);
end;



//------------------------------------------------------------------------------
function TdmExcel_file.Get_CellValue(ARow, ACol: Cardinal): Variant;
//------------------------------------------------------------------------------
begin
 // Inc(ARow);
 // Inc(ACol);

  Result:=Null;

  if not VarIsEmpty(FArrData) then
    if (ARow<=RowCount) and (ACol<=ColCount) then
      Result:=FArrData[ARow,ACol];
end;

//------------------------------------------------------------------------------
function TdmExcel_file.LoadWorksheetByIndex(aIndex: Integer): Boolean;
//------------------------------------------------------------------------------
var
  c: Integer;
  I: Integer;
  r: Integer;
//  vNewWorkbook: _Workbook;
  vExcelRange: ExcelRange;

  v: Variant;
begin
// vNewWorkbook := ExcelApplication1.Workbooks.Add (aFileName, GetUserDefaultLCID);
//  ExcelWorkbook1.ConnectTo(vNewWorkbook);
  Result:=False;

  if not Active then
    Exit;


 // Inc(aIndex);

  if aIndex<ExcelWorkbook1.Worksheets.count then
  begin
    ExcelWorksheet1.ConnectTo(ExcelWorkbook1.Worksheets[aIndex+1] as _Worksheet);

    ColCount:=ExcelWorksheet1.UsedRange[GetUserDefaultLCID].Columns.Count;
    RowCount:=ExcelWorksheet1.UsedRange[GetUserDefaultLCID].Rows.Count;


    vExcelRange:=ExcelWorksheet1.UsedRange[GetUserDefaultLCID];
    FArrData:=vExcelRange.Value2;

    Result := not VarIsEmpty(FArrData);


    SetLength(FStrArr, RowCount);
    for r := 0 to RowCount - 1 do
      SetLength(FStrArr[r], ColCount);


    for r := 0 to RowCount - 1 do
     for c := 0 to ColCount - 1 do
       FStrArr[r,c] := FArrData[r+1,c+1];



//            s:=VarToStr(dmExcel.Cells[iRow,iCol]);




 //   v:=FArrData[5,1];
  end;

//  vNewWorkbook.Close(false, EmptyParam, EmptyParam, GetUserDefaultLCID);
end;


//------------------------------------------------------------------------------
procedure TdmExcel_file.LoadWorksheetNames(aStrings: TStrings);
//------------------------------------------------------------------------------
var
  i: Integer;
begin
  aStrings.Clear;

  if not Active then
    Exit;
    

//zz  FvWorkbook := ExcelApplication1.Workbooks.Add (aFileName, GetUserDefaultLCID);
//  ExcelWorkbook1.ConnectTo(FvWorkbook);

  for i := 1 to ExcelWorkbook1.Worksheets.count do
  begin
    ExcelWorksheet1.ConnectTo(ExcelWorkbook1.Worksheets[i] as _Worksheet);
    aStrings.Add(ExcelWorksheet1.Name);
  end;

//  FvWorkbook.Close(false, EmptyParam, EmptyParam, GetUserDefaultLCID);

end;

// ---------------------------------------------------------------
procedure TdmExcel_file.Open(aFileName: string);
// ---------------------------------------------------------------
begin

  if FileExists(aFileName) then
  begin
    FvWorkbook := ExcelApplication1.Workbooks.Add (aFileName, GetUserDefaultLCID);
    ExcelWorkbook1.ConnectTo(FvWorkbook);
    Active := True;
  end else
    Active := False;

end;


procedure TdmExcel_file.Close;
begin
  if Active then
  begin
    FvWorkbook.Close(false, EmptyParam, EmptyParam, GetUserDefaultLCID);
  //  FvWorkbook:=nil;
    Active := False;
  end;
end;


end.
