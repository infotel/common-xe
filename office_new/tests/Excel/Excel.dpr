program Excel;

uses
  Forms,
  dm_Excel in '..\..\src\dm_Excel.pas' {dmExcel: TDataModule},
  f_Main_test_excel in 'f_Main_test_excel.pas' {Form3},
  u_Excel_to_Class in 'u_Excel_to_Class.pas',
  u_xml_document in '..\..\src\u_xml_document.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
 // Application.CreateForm(TForm4, Form4);
  Application.CreateForm(TdmExcel, dmExcel);
  Application.CreateForm(TForm3, Form3);
  Application.Run;
end.
