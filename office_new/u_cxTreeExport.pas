unit u_cxTreeExport;
interface

uses
  Variants,
  Excel97,

  MyExcel,
 // dxTL,
  Forms, cxTL;

//procedure ExportTreeToExcel(ATree: TCustomdxTreeListControl; aForm: TForm);
procedure ExportTreeToExcel(ATree: TcxTreeList; aForm: TForm);

implementation


const
  // ???-?? ???????, ?????????????? ?? ???
  BufferSize = 5;

procedure ExportTreeToExcel(ATree: TcxTreeList; aForm: TForm);
var
  Buffer: Variant;
  vSheet: ExcelWorkSheet;
  MaxTreeLevel, BufferIndex, LastSavedRow: integer;

  function GetMaxTreeLevel: integer;
  var
//    ANode: TdxTreeListNode;
    ANode: TcxTreeListNode;
  begin
    Result := 0;
    ANode := ATree.TopNode;
    while Assigned(ANode) do
    begin
      if ANode.Level > Result then
        Result := ANode.Level;
      ANode := ANode.GetNext;
    end;
  end;

  procedure PrepareExport;
  var
    i: integer;
    ARange: Excel97.ExcelRange;
  begin
    AddWorkBook;
    vSheet := XL.Workbooks[1].Sheets[1] as ExcelWorkSheet;
    vSheet.Cells.ColumnWidth := 2; // = 19 ????????
    BufferIndex := 0;
    LastSavedRow := 2;
    MaxTreeLevel := GetMaxTreeLevel;
    Buffer := VarArrayCreate(
      [0, BufferSize, 0, MaxTreeLevel + ATree.VisibleColumnCount],
      VarVariant
    );
    for i := 0 to ATree.VisibleColumnCount - 1 do
//      Buffer[0, MaxTreeLevel + i] := ATree.VisibleColumns[i].Caption;
      Buffer[0, MaxTreeLevel + i] := ATree.VisibleColumns[i].Caption.Text;
    ARange := vSheet.Range[
      GetAddress(1, 1),
      GetAddress(1, MaxTreeLevel + ATree.VisibleColumnCount)
    ];
    ARange.Value := Buffer;
    ARange.Font.Bold := true;
  end;

  procedure SaveBuffer(ABufferSize: integer);
  var
    i, j: integer;
    Dest: ExcelRange;
  begin
    Dest := vSheet.Range[
      GetAddress(LastSavedRow, 1),
      GetAddress(LastSavedRow + ABufferSize - 1, MaxTreeLevel + ATree.VisibleColumnCount)
    ];
    Dest.Value := Buffer;
    for i := 0 to BufferSize do
      for j := 0 to MaxTreeLevel + ATree.VisibleColumnCount do
        Buffer[i, j] := null;
  end;

  procedure ExportNode(ANode: TcxTreeListNode);
  var
    i: integer;
  begin
    for i := 0 to ATree.VisibleColumnCount - 1 do
    begin
      if i = 0 then
        Buffer[BufferIndex, ANode.Level] :=
//          ANode.Values[ATree.VisibleColumns[i].Index]
          ANode.Values[ATree.VisibleColumns[i].ItemIndex]
      else
        Buffer[BufferIndex, MaxTreeLevel + i] :=
//          ANode.Values[ATree.VisibleColumns[i].Index]
          ANode.Values[ATree.VisibleColumns[i].ItemIndex]
    end;

    Inc(BufferIndex);
    if BufferIndex = BufferSize then
    begin
      SaveBuffer(BufferSize);
      BufferIndex := 0;
      Inc(LastSavedRow, BufferSize);
    end;
  end;

  // -------------------------
  procedure FitWidth;
  // -------------------------
  var
    i: integer;
    ColumnName: string;
  begin
    for i := 0 to ATree.VisibleColumnCount - 1 do
    begin
      if i < 26 then
        ColumnName := Chr(Ord('A') + i)
       else
        ColumnName :=
          Chr(Ord('A') + (i div 26) - 1) +
          Chr(Ord('A') + (i mod 26));
      vSheet.Cells.Item[ColumnName, ColumnName].EntireColumn.AutoFit;
    end;
  end;

  procedure ExportData;
  var
    ANode: TcxTreeListNode;
  begin
    ANode := ATree.TopNode;
    while Assigned(ANode) do
    begin
      ExportNode(ANode);
      ANode := ANode.GetNext;
    end;
    SaveBuffer(BufferIndex);
    //FitWidth;
  end;

begin
  CreateExcel (aForm);
  try
    PrepareExport;
    ExportData;
    ShowExcel;

   // SaveWorkbook(v)

    vSheet := nil;
  finally
    ReleaseExcel(aForm);
  end;
end;

end.