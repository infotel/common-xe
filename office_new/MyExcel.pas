  unit MyExcel;

interface

uses
Windows, Excel97, Forms;


  procedure CreateExcel(Form: TForm);
  procedure ReleaseExcel(Form: TForm);
  procedure ShowExcel;
  procedure HideExcel;
  function AddWorkbook: ExcelWorkbook;

  function OpenWorkbook(const BookName: string): ExcelWorkbook;
  procedure CloseWorkbook(var Workbook: ExcelWorkbook);

  function GetAddress(Row, Column: integer): string;
  function GetBook(BookIndex: OleVariant): ExcelWorkbook;
  function GetSheet(Workbook: ExcelWorkbook; SheetIndex: OleVariant): ExcelWorksheet;

  function SaveWorkbook(Book: ExcelWorkbook; aFileName: string): boolean;

  procedure BoldRange(CRange: Excel97.ExcelRange);


var
  XL: TExcelApplication;
  FLCID: LCID;


implementation

uses
  OleServer, OleCtrls, ActiveX, ComObj, Classes, SysUtils;




procedure CreateExcel(Form: TForm);
begin
  FLCID := LOCALE_USER_DEFAULT;

  if not Assigned(XL) then
  begin // TOleServer
    XL := Excel97.TExcelApplication.Create(Form);
    XL.ConnectKind := ckNewInstance;
    XL.Connect;
  end;
end;

procedure ShowExcel;
begin// TLanguages
  if Assigned(XL) then
  begin // ? ???? ?? ?? ???????
    XL.Visible[FLCID] := true;

    if XL.WindowState[FLCID] = xlWindowState(xlMinimized) then
      XL.WindowState[FLCID] := XlWindowState(xlNormal);

    XL.ScreenUpdating[FLCID] := true;
  end;
end;

procedure HideExcel;
begin
if Assigned(XL) then
XL.Visible[FLCID] := false;
end;

procedure ReleaseExcel(Form: TForm);
begin
if Assigned(XL) then begin
if (XL.Workbooks.Count > 0) then begin
XL.WindowState[FLCID] := TOLEEnum(xlMaximized);
XL.Visible[FLCID] := true;
{
if not(csDestroying in Form.ComponentState) then
Form.SetFocus;
Application.BringToFront;
}
end;
FreeAndNil(XL);
end;
end;

function AddWorkbook: ExcelWorkbook;
begin
  Result := nil;
  XL.SheetsInNewWorkbook[FLCID] := 1;
  if Assigned(XL) then
  Result := XL.Workbooks.Add(EmptyParam, FLCID);
end;

procedure CloseWorkbook(var Workbook: ExcelWorkbook);
begin
  if not Assigned(Workbook) then Exit;

  Workbook.Close(false, EmptyParam, EmptyParam, FLCID);
  WorkBook := nil;
end;

function OpenWorkbook(const BookName: string): ExcelWorkbook;
begin
  try
    Result := XL.Workbooks.Open(BookName, false, false,
    EmptyParam, EmptyParam, false, true, EmptyParam,
    EmptyParam, EmptyParam, EmptyParam, EmptyParam,
    false, FLCID);
  except on EOleException do
  Result := nil;
  end;
end;

function GetAddress(Row, Column: integer): string;
begin
// Max - 2 chars
Column := Column - 1;
if Column < 26 then
Result := Chr(Ord('A') + Column)
else
Result := Chr(Ord('A') + (Column div 26) - 1) +
Chr(Ord('A') + (Column mod 26));
Result := Result + IntToStr(Row);
end;

function GetBook(BookIndex: OleVariant): ExcelWorkbook;
begin
try
Result := XL.Workbooks.Item[BookIndex];
except on EOleException do
Result := nil;
end;
end;

function GetSheet(Workbook: ExcelWorkbook;  SheetIndex: OleVariant): ExcelWorksheet;
begin
  try
    Result := Workbook.Worksheets.Item[SheetIndex] as ExcelWorksheet;
  except on EOleException do
    Result := nil;
  end;
end;

function SaveWorkbook(Book: ExcelWorkbook; aFileName: string): boolean;
begin
  Result := true;
  XL.DisplayAlerts[FLCID] := false;
  
  try
    Book.SaveAs(aFileName, xlFileFormat(xlWorkbookNormal), '', '',
    false, false, xlSaveAsAccessMode(xlExclusive),
    EmptyParam, EmptyParam, EmptyParam, EmptyParam, FLCID);
  except on EOleException do
    Result := false;
  end;

  XL.DisplayAlerts[FLCID] := true;
end;


procedure BoldRange(CRange: Excel97.ExcelRange);
begin
  CRange.Borders.Item[xlEdgeLeft].Weight :=
  XlBorderWeight(xlMedium);
  CRange.Borders.Item[xlEdgeRight].Weight :=
  XlBorderWeight(xlMedium);
  CRange.Borders.Item[xlEdgeTop].Weight :=
  XlBorderWeight(xlMedium);
  CRange.Borders.Item[xlEdgeBottom].Weight :=
  XlBorderWeight(xlMedium);
end;

end.