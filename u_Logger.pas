unit u_Logger;

interface

uses
  Dialogs,
  WinApi.Windows, StrUtils, IOUtils,  System.SysUtils;

type
  TLogger = class(TObject)
  type
    TLogEvent = procedure (aMsg: string) of object;
    TProgressEvent = procedure (aPosition,aMax : word) of object;

  private
    FOnLog: TLogEvent;

  private
    FOnProgress: TProgressEvent;
    FRootDir: string;
    procedure SetRootDir(Value: string);
  public

    procedure Progress(aPosition,aMax : word);
//    procedure Progress2(aPos,aMax : word);

    procedure Add(aMsg: string);
    procedure Error(aMsg: string);

    property RootDir: string read FRootDir write SetRootDir;

    property OnLog: TLogEvent read FOnLog write FOnLog;

    property OnProgress: TProgressEvent read FOnProgress write FOnProgress;


  end;


var
  g_Logger: TLogger;

implementation

var
  csCriticalSection: TRTLCriticalSection;


//-------------------------------------------------------------------
procedure TLogger.Add(aMsg: string);
//-------------------------------------------------------------------
var
  sDir: string;
begin
 EnterCriticalSection(csCriticalSection);
  try
    if Assigned(FOnLog) then
      FOnLog (aMsg);

    if FRootDir<>'' then
    begin
      DateTimeToString(sDir, 'yyyy mm dd', Date());
      TFile.AppendAllText(FRootDir + sDir + '.txt', aMsg);
    end;

  finally
    LeaveCriticalSection(csCriticalSection);
  end;
end;

//-------------------------------------------------------------------
procedure TLogger.Error(aMsg: string);
//-------------------------------------------------------------------
var
  sDir: string;
begin
  EnterCriticalSection(csCriticalSection);
  try
    if Assigned(FOnLog) then
      FOnLog (aMsg);

    if FRootDir<>'' then
    begin
      DateTimeToString(sDir, 'yyyy mm dd', Date());
      TFile.AppendAllText(FRootDir + 'error ' +sDir + '.txt', aMsg);
    end;

  finally
    LeaveCriticalSection(csCriticalSection);
  end;
end;


procedure TLogger.Progress(aPosition,aMax : word);
begin
  if Assigned(FOnProgress) then
    FOnProgress (aPosition,aMax);
end;


procedure TLogger.SetRootDir(Value: string);
begin
  FRootDir:= IncludeTrailingBackslash(Value);
  ForceDirectories(FRootDir);
end;




initialization

  InitializeCriticalSection(csCriticalSection);

  g_Logger:=TLogger.Create;


 // ShowMessage ('unit u_Logger;');

finalization
  DeleteCriticalSection(csCriticalSection);

  FreeAndNil(g_Logger);
end.


{
  DateTimeToString(sDir, 'yyyy mm dd', Date());

  sFile:=sDir + 'log.txt';

  sDir:= IncludeTrailingBackslash ( g_Config.Log_Dir + sDir);

//  ForceDirectories(sDir);

  TFile.AppendAllText(sFile, aMsg);

