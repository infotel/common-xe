unit d_Wizard;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ActnList, StdCtrls, ExtCtrls, Placemnt, dxTL,  dxDBGrid,
  dxCntner, GSTabs, rxPlacemnt;

type
  Tdlg_Wizard = class(TForm)
    pn_Buttons: TPanel;
    Bevel1: TBevel;
    ActionList1: TActionList;
    act_Ok: TAction;
    act_Cancel: TAction;
    pn_Header: TPanel;
    FormStorage1: TFormStorage;
    lb_Action: TLabel;
    btn_Ok: TButton;
    btn_Cancel: TButton;
    Bevel2: TBevel;
    procedure act_CancelExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure act_OkExecute(Sender: TObject);
 //   procedure btn_OkClick(Sender: TObject);
  private
  protected
    FRegPath: string;

    //
  public
    procedure AlignTop;
    procedure SetActionName (aValue: string);

    procedure SetDefaultSize();
    procedure SetDefaultWidth();
  end;


//==================================================================
implementation {$R *.DFM}
//==================================================================


procedure Tdlg_Wizard.act_CancelExecute(Sender: TObject);
begin
//  if Sender=act
  Close;
end;


procedure Tdlg_Wizard.AlignTop;
var
  i: integer;
begin
  for i:=0 to ControlCount-1 do
    Controls[i].Top:=Controls[i].Top - pn_Header.Height;

end;

//-------------------------------------------------------------------
procedure Tdlg_Wizard.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  lb_Action.Top:=8;
  lb_Action.Left:=5;
  pn_Header.BorderWidth:=0;

  {
  }

 {
  with pn_Header.Constraints do begin
    MaxHeight:=60;
    MinHeight:=MaxHeight;
  end;

  }

  SetActionName('');

  if not pn_Header.Visible then
    AlignTop();

end;


procedure Tdlg_Wizard.SetActionName(aValue: string);
begin
  lb_Action.Caption:=aValue;
end;

{
procedure Tdlg_Wizard.btn_OkClick(Sender: TObject);
begin
//
end;}

procedure Tdlg_Wizard.SetDefaultSize;
begin
  with Constraints do begin
    MaxHeight:=385;
    MinHeight:=MaxHeight;
  end;

  SetDefaultWidth();
end;


procedure Tdlg_Wizard.SetDefaultWidth();
begin
  with Constraints do begin
    MaxWidth :=505;
    MinWidth :=MaxWidth;
  end;
end;



procedure Tdlg_Wizard.act_OkExecute(Sender: TObject);
begin
// !!!!!!!!!!!!
end;

end.

