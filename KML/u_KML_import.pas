unit u_KML_import;

interface

uses
  Classes, XMLIntf , SysUtils,  DB,

  u_geo,

  u_db,
  
  u_func,
  u_func_arr,
 
  u_xml_document
  ;

type
  //---------------------------------------------------------
  TPlacemark = class(TCollectionItem)
  //---------------------------------------------------------
  public
    Name: string;
    Lat_WGS: Double;
    Lon_WGS: double;

    Description: string;
   // Comments: string;
  end;

  //---------------------------------------------------------
  TKmlStyle = class(TCollectionItem)
  //---------------------------------------------------------
  public
    Name: string;
  end;

  //---------------------------------------------------------
  TKmlPolygon = class(TCollectionItem)
  //---------------------------------------------------------
  public
    BLPointArray_Pulkovo: TBLPointArray;
  end;
               

//
//procedure AddPlacemark_Polygon(var aCoords_Pulkovo: TBLPointArrayF;
//    aID_style: Integer);



  //-------------------------------------------------------------------
  TPlacemarkList = class(TCollection)
  //-------------------------------------------------------------------
  private
    function GetItems(Index: Integer): TPlacemark;
    procedure LoadFromXMLNode(aParentNode: IXmlNode);
  public
    constructor Create;
    function AddItem: TPlacemark;

    property Items[Index: Integer]: TPlacemark read GetItems; default;

    procedure LoadFromFile(aFileName: string);
    procedure LoadFromStream(aStream: TStream);
    
    procedure SaveToDataset(aDataset: TDataset);

  end;


//
//procedure AddPlacemark_Polygon(var aCoords_Pulkovo: TBLPointArrayF;
//    aID_style: Integer);



  //-------------------------------------------------------------------
  TStyleList = class(TCollection)
  //-------------------------------------------------------------------
  private
    function GetItems(Index: Integer): TKmlStyle;
  public
    constructor Create;
    function AddItem: TKmlStyle;

    property Items[Index: Integer]: TKmlStyle read GetItems; default;

  end;

 
  TKML = class
  public
    Placemarks: TPlacemarkList;
    Styles: TStyleList;

    constructor Create;
    destructor Destroy; override;
    
    procedure LoadFromFile(aFileName: string);

  end;



implementation



constructor TPlacemarkList.Create;
begin
  inherited Create(TPlacemark);
end;

function TPlacemarkList.AddItem: TPlacemark;
begin
  Result := TPlacemark (inherited Add);
end;

function TPlacemarkList.GetItems(Index: Integer): TPlacemark;
begin
  Result := TPlacemark(inherited Items[Index]);
end;

// ---------------------------------------------------------------
procedure TPlacemarkList.LoadFromStream(aStream: TStream);
// ---------------------------------------------------------------
var
  oStringStream: TStringStream;

  s: string;

  oXMLDoc: TXMLDoc;
  vDoc: IXmlNode;

begin
  oStringStream:=TStringStream.Create('');
  oStringStream.CopyFrom(aStream,0);

  s:=oStringStream.ReadString(0);

  FreeAndNil(oStringStream);

  oXMLDoc:=TXMLDoc.Create;

  oXMLDoc.LoadFromText(s);

  //Clear;
  vDoc := xml_FindNode(oXMLDoc.DocumentElement, 'Document');

  LoadFromXMLNode (vDoc);

  FreeAndNil(oXMLDoc);


end;


// ---------------------------------------------------------------
procedure TPlacemarkList.LoadFromXMLNode(aParentNode: IXmlNode);
// ---------------------------------------------------------------
var

  I: Integer;

  vPoint,
  vPlacemark,
  v: IXmlNode;

  s: string;
  sName: string;

  strArr: TStrArray;

  oPlacemark: TPlacemark;
//  oXMLDoc: TXMLDoc;
  vNode,vDoc,vKML: IXmlNode;

  va: variant;

begin
  Assert( Assigned(aParentNode));


  for i:=0 to aParentNode.ChildNodes.Count-1 do
  begin
    vNode:=aParentNode.ChildNodes[i];

    s:=vNode.LocalName;

    if Eq(vNode.LocalName,'Style') then
      Continue;
    
    if Eq(vNode.LocalName,'Folder') then
      LoadFromXMLNode(vNode)

    else

    if Eq(vNode.LocalName,'Placemark') then
    begin
      vPlacemark := vNode;
      vPoint :=vPlacemark.ChildNodes['Point'];

      if not Assigned(vPoint) then
        Continue;

      oPlacemark := AddItem();


      v:=vPlacemark.ChildNodes['name'];
      oPlacemark.Name := v.Text;

      v:=vPlacemark.ChildNodes['description'];
      oPlacemark.Description :=v.Text;


      v:=vPoint.ChildNodes['coordinates'];
      s := v.Text;

      strArr := StringToStrArray(s, ',');

      if Length(strArr)>1 then
      begin
        oPlacemark.Lat_WGS:= AsFloat(strArr[1]);
        oPlacemark.Lon_WGS:= AsFloat(strArr[0]);
      end;
    end;

  end;
end;
// ---------------------------------------------------------------


// ---------------------------------------------------------------
procedure TPlacemarkList.LoadFromFile(aFileName: string);
// ---------------------------------------------------------------
var
  s: string;

  oXMLDoc: TXMLDoc;
  vDoc: IXmlNode;

begin
  oXMLDoc:=TXMLDoc.Create;

  oXMLDoc.LoadFromFile(aFileName);

  vDoc := oXMLDoc.DocumentElement;
//  s:=vDoc.LocalName;


  vDoc := xml_FindNode(oXMLDoc.DocumentElement, 'Document');

  Assert( Assigned(vDoc));


  if Assigned(vDoc) then
  begin
   // s:=vDoc.LocalName;
    LoadFromXMLNode (vDoc);
  end;


  FreeAndNil(oXMLDoc);

end;

// ---------------------------------------------------------------
procedure TPlacemarkList.SaveToDataset(aDataset: TDataset);
// ---------------------------------------------------------------
var
  I: Integer;
begin
  aDataset.DisableControls;

  for I := 0 to Count-1 do
    db_AddRecord_(aDataset,
      [     
        FLD_CHECKED,     True,
        
        FLD_NAME,        Items[i].Name,        
        FLD_Description, Items[i].Description,
        
        FLD_LAT, Items[i].Lat_WGS,
        FLD_LON, Items[i].Lon_WGS
      ]);

  aDataset.EnableControls;
    
end;


constructor TKML.Create;
begin
  inherited Create;

  Styles := TStyleList.Create();
  Placemarks := TPlacemarkList.Create();
end;

 
destructor TKML.Destroy;
begin
  FreeAndNil(Styles);
  FreeAndNil(Placemarks);
  inherited Destroy;
end;


procedure TKML.LoadFromFile(aFileName: string);
begin
  Placemarks.LoadFromFile(aFileName);
end;


constructor TStyleList.Create;
begin
  inherited Create(TKmlStyle);
end;

function TStyleList.AddItem: TKmlStyle;
begin
  Result := TKmlStyle (inherited Add);
end;

function TStyleList.GetItems(Index: Integer): TKmlStyle;
begin
  Result := TKmlStyle(inherited Items[Index]);
end;



procedure u_KML_import_test();
var
  oKML: TKML;
begin
  oKML:=TKML.Create;
  oKML.LoadFromFile('W:\RPLS_DB_XE_PARTS\KML\demos\_�����57.kml');
  
end;


begin
//  u_KML_import_test;


end.

