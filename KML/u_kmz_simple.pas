unit u_kmz_simple;

interface

uses SysUtils, Graphics,

  //   dm_Progress,

     KAZip,
     unicodefile,
     
     u_files,

     u_img_PNG,
//     u_Geo_convert,
     u_Geo_convert_new,
     u_Geo
     ;


type

  TKMZCreateRec = record
    FolderName,
    Name_,
    BMPFileName: string;
    BL42_BottomLeft,
    BL42_BottomRight,
    BL42_TopRight,
    BL42_TopLeft: TBLPoint;
  end;

  TKMZCreateRecArr = record
    KmzFileName: string;

    Files: array of TKMZCreateRec;
  end;


  TKMZ_simple = class(TObject)
  private
    FStrList: TUnicodeFile;
    FKAZip: TKAZip;

    procedure AddHeader;
    procedure SaveToFile(aFileName: string);

(*    procedure AddPic_Box(aName, aBMPFileName, aKMZFileName: string; aTransparence: Integer;
                     const aTopLeftPoint_SK42, aBottomRightPoint_SK42: TBLPoint
                     ); overload;
*)
    procedure AddPic(aName, aBMPFileName, aKMZFileName: string; aTransparence: Integer;

                     aBLPoints_WGS: TBLPointArray);

                   //  const aBL42_BottomLeft, aBL42_BottomRight,
                     //      aBL42_TopRight, aBL42_TopLeft: TBLPoint); overload;
//    procedure Clear;

//    --------------
    procedure AddPicNew(aRec: TKMZCreateRec; aKMZFileName: string; aTransparence: Integer );

    procedure CreateFiles(aArr: TKMZCreateRecArr; aTransparence: Integer );

//    --------------

  public
    constructor Create;
    destructor Destroy; override;



    // aName - ��� ����� � ������ Google Earth
    // aKMZFilename - �������� ����
    // aBMPFileName - �������� �������� � ������� BMP,
    //                ����� ������������� � PNG ��� ����������� ������������
    // aTransparence - ������� ������������ � ��������� 0..255 (0 - ��������� ���������)

(*    procedure CreateFile_forBox(aName, aKMZFileName, aBMPFileName : string;
                                aTransparence: Integer;
                                const aTopLeftPoint_SK42, aBottomRightPoint_SK42: TBLPoint);
*)


    // TBLPoint -
    //   ��������� ���������� ������ ����������������, ������������� ���� ���������.
    //   ���������� ������� ��������� ������ ������� �������,
    //   ������� � ������� ������ ���� ����������� �����������.

  private
   procedure CreateFile(aName, aKMZFileName, aBMPFileName : string;
                         aTransparence: Integer; //viewBoundScale

                         aBLPoints_WGS: TBLPointArray);
                       //  aBLRect_42: TBLRect;

                       //  const aBL42_BottomLeft, aBL42_BottomRight,
                        //       aBL42_TopRight, aBL42_TopLeft: TBLPoint); overload;

  public
    // 4-� ���������� ����������� �������������
    procedure CreateFileByXYRect(aName: string;
                         aKMZFileName, aBMPFileName : string;
                         aTransparence: Integer;

                         aXYRect_42: TXYRect;
                         aZone : Integer

                        // aBL42_TopLeft,
                         //aBL42_BottomRight: TBLPoint
                         ); //overload;

  end;

implementation

const
  CRLF = #13+#10;
  PNG_FILE = 'attachment.png';



//----------------------------------------------------------------------------
constructor TKMZ_simple.Create;
//----------------------------------------------------------------------------
begin

  inherited Create;

  FKAZip:= TKAZip.Create(nil);
  FStrList := TUnicodeFile.Create();
end;

//----------------------------------------------------------------------------
destructor TKMZ_simple.Destroy;
//----------------------------------------------------------------------------
begin
  FStrList.Free;
  FKAZip.Free;

  inherited;// Destroy;
end;

//----------------------------------------------------------------------------
procedure TKMZ_simple.CreateFiles(aArr: TKMZCreateRecArr; aTransparence: Integer        );
//----------------------------------------------------------------------------
var
  i: Integer;
  s, sKMLFileName, sFolder, sPNGFileName: string;
  b: boolean;
begin
  FStrList.Clear;

  AddHeader;
  FStrList.Add('<Document>');

  sFolder:= '-1zdvdC';
  for i := 0 to High(aArr.Files) do
    with aArr.Files[i] do
  begin
 //   if Assigned(aOnProgress) then
   //   aOnProgress(i, High(aArr.Files), b);

    if sFolder <> FolderName then begin
      if i<>0 then
        FStrList.Add(' </Folder>');

      FStrList.Add(' <Folder>');
      FStrList.Add(Format('  <name>%s</name>', [FolderName]));
      sFolder:= FolderName;
    end;

    FStrList.Add('   <Folder>');
    FStrList.Add(Format('     <name>%s</name>', [Name_]));

    AddPicNew( aArr.Files[i], aArr.KmzFileName, aTransparence );
//    DeleteFile( aArr.Files[i].BMPFileName );

    FStrList.Add('   </Folder>');
  end;

  FStrList.Add(' </Folder>');
  FStrList.Add('</Document>');
  FStrList.Add('</kml>'); //�������� ��������� ����

  sKMLFileName:= ExtractFilePath(aArr.KmzFileName)+'doc.kml';

  SaveToFile(sKMLFileName);

  s:= ExtractFileDir(ExcludeTrailingBackslash(aArr.KmzFileName));

  FKAZip.CreateZip(aArr.KmzFileName);
  FKAZip.Open(aArr.KmzFileName);
  // ������ ���������� - ��� � ���� ������ ������
  FKAZip.AddFile(sKMLFileName, ExtractFileName(sKMLFileName));         // 'doc.kml');

  for i := 0 to High(aArr.Files) do
    with aArr.Files[i] do
  begin
  //  if Assigned(aOnProgress) then
   //   aOnProgress(i, High(aArr.Files), b);

    sPNGFileName:= s+'\files\'+ ExtractFileNameNoExt(BMPFileName)+'.png';
    FKAZip.AddFile(sPNGFileName, 'files\'+ExtractFileName(sPNGFileName));//'files\kup.png');
  end;

  FKAZip.Close;

  DeleteFile(sKMLFileName);
//  DeleteFile(sPNGFileName);
end;

//----------------------------------------------------------------------------
procedure TKMZ_simple.AddPicNew(aRec: TKMZCreateRec; aKMZFileName: string; aTransparence: Integer);
//----------------------------------------------------------------------------
//------------------<gx:LatLonQuad>---------------------------
//��������� ���������� ������ ����������������, ������������� ���� ���������.
//������ ���� ����� ������ ����� ���������, ������ �� ������� ������� ��
//�������� � ��������� ������� ��� ������� � ������.
//���������� ������ ����� �����������.
//�� ��������� ������� � �������� ���������.
//���������� ������� ��������� ������ ������� �������,
//������� � ������� ������ ���� ����������� �����������.
const
  DEF_STR_GroundOverlay =
'         <GroundOverlay>' + CRLF +
'             <name>%s</name>' + CRLF +
'             <color>%xffffff</color> ' + CRLF +
'             <Icon> ' + CRLF +
'                 <href>%s</href> ' + CRLF +
'                 <viewBoundScale>%d</viewBoundScale>' + CRLF  +
'             </Icon> ' + CRLF  +
'             <gx:LatLonQuad>' + CRLF  +
'               <coordinates>' + CRLF  +
'                 %3.8f,%3.8f %3.8f,%3.8f %3.8f,%3.8f %3.8f,%3.8f' + CRLF  +
'               </coordinates>' + CRLF  +
'             </gx:LatLonQuad>' + CRLF  +
'         </GroundOverlay>';

var i:integer;
  s , sPNGFileName, sPNGFilePath: string;
  blPoint1, blPoint2, blPoint3, blPoint4: TBlPoint;
begin
//  s:= ExtractFileDir(ExcludeTrailingBackslash(aRec.BMPFileName));
//  sPNGFileName:= ExtractFileNameNoExt(aRec.BMPFileName)+'.png';
//  sPNGFilePath:=  IncludeTrailingBackslash(s)+'files\'+sPNGFileName;
//  ForceDirectories(IncludeTrailingBackslash(s)+'files\');
  s:= ExtractFileDir(ExcludeTrailingBackslash(aKMZFileName));
  sPNGFileName:= ExtractFileNameNoExt(aRec.BMPFileName)+'.png';
  sPNGFilePath:= s+'\files\'+sPNGFileName;

  ForceDirectories(s+'\files\');

  if FileExists(aRec.BMPFileName) then
//    img_BMP_to_PNG(PChar(aRec.BMPFileName), PChar(sPNGFilePath), true, clWhite);
    img_BMP_to_PNG(PChar(aRec.BMPFileName), PChar(sPNGFilePath), true, clBlack);



end;


//----------------------------------------------------------------------------
procedure TKMZ_simple.CreateFile(aName, aKMZFileName, aBMPFileName : string; aTransparence: Integer;
    // aBLRect_42: TBLRect;
    aBLPoints_WGS: TBLPointArray);

//     const aBL42_BottomLeft, aBL42_BottomRight, aBL42_TopRight, aBL42_TopLeft: TBLPoint);
//----------------------------------------------------------------------------
var sKMLFileName : string;
    s , sPNGFileName: string;
begin
  FStrList.Clear;

  try
    AddHeader;

    AddPic(aName, aBMPFileName, aKMZFilename,aTransparence,
           aBLPoints_WGS);
           //aBL42_BottomLeft, aBL42_BottomRight, aBL42_TopRight, aBL42_TopLeft);
    //�������� ��������� ����
    FStrList.Add('</kml>');

    sKMLFileName:= ExtractFilePath(aKMZFilename)+'doc.kml';

    SaveToFile(sKMLFileName);

    // ��� ���������� ������� � �������� ������� ������ - ������������ ���������
    s:= ExtractFileDir(ExcludeTrailingBackslash(aKMZFileName));
    sPNGFileName:= s+'\files\'+PNG_FILE; //ExtractFileNameNoExt(aBMPFileName)+'.png';

    FKAZip.CreateZip(aKMZFilename);
    FKAZip.Open(aKMZFilename);
    // ������ ���������� - ��� � ���� ������ ������
    FKAZip.AddFile(sKMLFileName, ExtractFileName(sKMLFileName));         // 'doc.kml');
    FKAZip.AddFile(sPNGFileName, 'files\'+ExtractFileName(sPNGFileName));//'files\kup.png');
    FKAZip.Close;



    DeleteFile(sKMLFileName);
    DeleteFile(sPNGFileName);
    DeleteFile(aBMPFileName);

    RemoveDir(s+'\files\');

  except
  end;

end;    //



//----------------------------------------------------------------------------
procedure TKMZ_simple.CreateFileByXYRect(aName: string;
                                 aKMZFileName, aBMPFileName : string;
                                 aTransparence: Integer;

                                 aXYRect_42: TXYRect;
                                 aZone : Integer

                               //  aBL42_TopLeft,
                               //  aBL42_BottomRight : TBLPoint
                                 );
//----------------------------------------------------------------------------
var
  xyPoints: TXYPointArray;
  blPoints_WGS: TBLPointArray;
  i: Integer;

begin
  xyPoints :=  geo_XYRectToXYPoints_BottomLeft_first (aXYRect_42);

  SetLength(blPoints_WGS,4);


  for i:=0 to High(xyPoints) do
    blPoints_WGS[i] := geo_GK_XY_to_WGS84_BL(xyPoints[i], aZone);

  aKMZFileName:= ChangeFileExt(aKMZFileName, '.kmz');

  CreateFile(aName, aKMZFileName, aBMPFileName, aTransparence,  blPoints_WGS );
end;    //

//----------------------------------------------------------------------------
procedure TKMZ_simple.AddHeader;
//----------------------------------------------------------------------------
const
  DEF_STR = '<kml xmlns="http://www.opengis.net/kml/2.2" xmlns:gx="http://www.google.com/kml/ext/2.2" xmlns:kml="http://www.opengis.net/kml/2.2" xmlns:atom="http://www.w3.org/2005/Atom"> ';
begin
   FStrList.Add('<?xml version="1.0" encoding="UTF-8"?>');
   FStrList.Add(DEF_STR);
end;


//----------------------------------------------------------------------------
procedure TKMZ_simple.AddPic(aName, aBMPFileName, aKMZFileName: string;
      aTransparence: Integer;
      aBLPoints_WGS: TBLPointArray);

     // const aBL42_BottomLeft, aBL42_BottomRight, aBL42_TopRight, aBL42_TopLeft: TBLPoint);

//----------------------------------------------------------------------------
//------------------<gx:LatLonQuad>---------------------------
//��������� ���������� ������ ����������������, ������������� ���� ���������.
//������ ���� ����� ������ ����� ���������, ������ �� ������� ������� ��
//�������� � ��������� ������� ��� ������� � ������.
//���������� ������ ����� �����������.
//�� ��������� ������� � �������� ���������.
//���������� ������� ��������� ������ ������� �������,
//������� � ������� ������ ���� ����������� �����������.
const
  DEF_STR_GroundOverlay =
' <GroundOverlay>' + CRLF +
'     <name>%s</name>' + CRLF +
'     <color>%xffffff</color> ' + CRLF +
'     <Icon> ' + CRLF +
'         <href>%s</href> ' + CRLF +
'         <viewBoundScale>%d</viewBoundScale>' + CRLF  +
'     </Icon> ' + CRLF  +
'     <gx:LatLonQuad>' + CRLF  +
'       <coordinates>' + CRLF  +
'         %3.8f,%3.8f %3.8f,%3.8f %3.8f,%3.8f %3.8f,%3.8f' + CRLF  +
'       </coordinates>' + CRLF  +
'     </gx:LatLonQuad>' + CRLF  +
' </GroundOverlay>';

var i:integer;
  s , sPNGFileName, sPNGFilePath: string;
 // blPoint1, blPoint2, blPoint3, blPoint4: TBlPoint;
begin
  s:= ExtractFileDir(ExcludeTrailingBackslash(aKMZFileName));
  sPNGFileName:= PNG_FILE; //ExtractFileNameNoExt(aBMPFileName)+'.png';
  sPNGFilePath:=  s+'\files\'+sPNGFileName;
  ForceDirectories(s+'\files\');

//  img_BMP_to_PNG(aBMPFileName, sPNGFilePath, True, clWhite);
  img_BMP_to_PNG(aBMPFileName, sPNGFilePath, True, clBlack);


(*
  blPoint1 := geo_BL_to_BL(aBL42_BottomLeft, EK_KRASOVSKY42, EK_WGS_84);
  blPoint2 := geo_BL_to_BL(aBL42_BottomRight,EK_KRASOVSKY42, EK_WGS_84);
  blPoint3 := geo_BL_to_BL(aBL42_TopRight,   EK_KRASOVSKY42, EK_WGS_84);
  blPoint4 := geo_BL_to_BL(aBL42_TopLeft,    EK_KRASOVSKY42, EK_WGS_84);
*)
  FStrList.Add(Format(DEF_STR_GroundOverlay,
            [aName,
             aTransparence, //'6effffff',
             'files/'+sPNGFileName, 1,

             aBLPoints_WGS[0].L, aBLPoints_WGS[0].B,
             aBLPoints_WGS[1].L, aBLPoints_WGS[1].B,
             aBLPoints_WGS[2].L, aBLPoints_WGS[2].B,
             aBLPoints_WGS[3].L, aBLPoints_WGS[3].B

             //          blPoint1.L, blPoint1.B,
              //         blPoint2.L, blPoint2.B,
               //        blPoint3.L, blPoint3.B,
                //       blPoint4.L, blPoint4.B
                       ]));
end;


//----------------------------------------------------------------------------
procedure TKMZ_simple.SaveToFile(aFileName: string);
//----------------------------------------------------------------------------
begin
 // DeleteFile(aFileName);
  FstrList.SaveToUnicodeFile(aFileName);
end;


end.


(*
//----------------------------------------------------------------------------
procedure TKMZ_simple.Clear;
//----------------------------------------------------------------------------
begin
  FstrList.Clear;
//  Fnum:= 0;
end;
*)

//
////----------------------------------------------------------------------------
//procedure TKMZ_simple.CreateFile_forBox(aName, aKMZFilename, aBMPFileName : string; aTransparence: Integer;
//                             const aTopLeftPoint_SK42, aBottomRightPoint_SK42: TBLPoint);
////----------------------------------------------------------------------------
//var sKMLFileName : string;
//    s , sPNGFileName: string;
//begin
//  FStrList.Clear;
//
//  try
//    AddHeader;
//    AddPic_Box(aName, aBMPFileName, aKMZFilename, aTransparence, aTopLeftPoint_SK42, aBottomRightPoint_SK42);
//    //�������� ��������� ����
//    FStrList.Add('</kml>');
//
//    sKMLFileName:= ExtractFilePath(aKMZFilename)+'doc.kml';
//
//    SaveToFile(sKMLFileName);
//
//    // ��� ���������� ������� � �������� ������� ������ - ������������ ���������
//    s:= ExtractFileDir(ExcludeTrailingBackslash(aKMZFileName));
//    sPNGFileName:= s+'\files\'+PNG_FILE; //ExtractFileNameNoExt(aBMPFileName)+'.png';
//
//    FKAZip.CreateZip(aKMZFilename);
//    FKAZip.Open(aKMZFilename);
//    // ������ ���������� - ��� � ���� ������ ������
//    FKAZip.AddFile(sKMLFileName, ExtractFileName(sKMLFileName));         // 'doc.kml');
//    FKAZip.AddFile(sPNGFileName, 'files\'+ExtractFileName(sPNGFileName));//'files\kup.png');
//    FKAZip.Close;
//
//    DeleteFile(sKMLFileName);
//    DeleteFile(sPNGFileName);
//
//  except
//  end;
//end;    //



(*
//----------------------------------------------------------------------------
procedure TKMZ_simple.AddPic_Box(aName, aBMPFileName, aKMZFileName: string; aTransparence: Integer;
                       const aTopLeftPoint_SK42, aBottomRightPoint_SK42: TBLPoint);
//----------------------------------------------------------------------------
const
  DEF_STR_GroundOverlay =
' <GroundOverlay>' + CRLF +
'     <name>%s</name>' + CRLF +
'     <color>%xffffff</color> ' + CRLF +
'     <Icon> ' + CRLF +
'         <href>%s</href> ' + CRLF +
'         <viewBoundScale>%d</viewBoundScale>' + CRLF  +
'     </Icon> ' + CRLF  +
'     <LatLonBox>'  + CRLF  +
'         <north>%3.8f</north>' + CRLF +
'         <south>%3.8f</south>' + CRLF +
'         <east>%3.8f</east>' + CRLF +
'         <west>%3.8f</west>' + CRLF +
'     </LatLonBox>' + CRLF +
' </GroundOverlay>';

var i:integer;
  s , sPNGFileName, sPNGFilePath: string;
  blTopLeftPoint, blBottomRightPoint: TBlPoint;
begin
  s:= ExtractFileDir(ExcludeTrailingBackslash(aKMZFileName));
  sPNGFileName:= PNG_FILE; //ExtractFileNameNoExt(aBMPFileName)+'.png';
  sPNGFilePath:=  s+'\files\'+sPNGFileName;
  ForceDirectories(s+'\files\');

  img_BMP_to_PNG(aBMPFileName, sPNGFilePath, True, clWhite);

  blTopLeftPoint := geo_BL_to_BL(aTopLeftPoint_SK42, EK_KRASOVSKY42, EK_WGS_84);
  blBottomRightPoint:= geo_BL_to_BL(aBottomRightPoint_SK42, EK_KRASOVSKY42, EK_WGS_84);

  FStrList.Add(Format(DEF_STR_GroundOverlay,
                      [aName, aTransparence,
                       'files/'+sPNGFileName, 1,
                       blTopLeftPoint.B,
                       blBottomRightPoint.B,
                       blTopLeftPoint.L,
                       blBottomRightPoint.L]));

end;
*)

