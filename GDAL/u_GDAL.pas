unit u_GDAL;

interface
        
uses

  System.SysUtils, System.Classes, //   Dialogs,
  
  Math, 

  StrUtils,

  //.TDirectory

  
  u_run,

  
  
  u_geo,
  u_files,

//  u_func_arr,
  
  u_func;


type
  TGDal = class(TObject)
  private
    FPath_bin: string;
    FPath_epsg_csv: string;

    FImg_width : integer;
    FImg_Height: integer;
    
    XYbounds: TXYBounds;
    
    procedure LoadFromFile_GDalInfo(aFileName: string);
    procedure SaveTabFile(aFileName, aImgFileName: string; aImg_width, aImg_Height:
        integer; aXYbounds: TXYBounds);

  public     
    constructor Create;  

    procedure BmpToTabFile(aFileName_bmp,aFileName_tab: string);
                   
    
  end;

  
implementation


// ---------------------------------------------------------------
constructor TGDal.Create;
// ---------------------------------------------------------------

var
  s: string;
begin
  inherited;

  s:=SearchFileInPath ('gdal_translate.exe');
  Assert(s<>'', 'not found: gdal_translate.exe');

  FPath_bin:=ExtractFilePath(s);  
                   
  
  FPath_epsg_csv:=GetParentFileDir (ExtractFilePath(FPath_bin)) + 'share\epsg_csv';
  Assert(DirectoryExists(FPath_epsg_csv));

end;


// ---------------------------------------------------------------
procedure TGDal.SaveTabFile(aFileName, aImgFileName: string; aImg_width,
    aImg_Height: integer; aXYbounds: TXYBounds);
// ---------------------------------------------------------------
const
  DEF_FILE = 
  '!table'+ CRLF +
  '!version 300'+ CRLF +
  '!charset WindowsCyrillic'+ CRLF +
  ''+ CRLF +
  'Definition Table'+  CRLF +
  '  File ":file"'+ CRLF +
  '  Type "RASTER"'+  CRLF +
  '  (:lon_max,:lat_min) (:w,:h) Label "point 1",'+ CRLF +
  '  (:lon_min,:lat_min) (0,:h) Label "point 2",'+CRLF +
  '  (:lon_min,:lat_max) (0,0) Label "point 3",'+ CRLF +
  '  (:lon_max,:lat_max) (:w,0) Label "point 4"'+ CRLF +
//  '  CoordSys Earth Projection 10, 157, "m", 0'+ CRLF +
  '  CoordSys Earth Projection 10, 104, "m", 0'+ CRLF +
  
//  '  CoordSys Earth Projection 10, 104, "m", 0'+ CRLF +
  
//  '  CoordSys Earth Projection 10, 104'+ CRLF +
  '  Units "m"'+ CRLF +
//  '  Units "degree"'+ CRLF +  
  '  RasterStyle 4 1'+ CRLF +
  '  RasterStyle 7 0';//+ CRLF +
//  '  RasterStyle 8 127';


           
var
  iMax: Integer;
  s: string;

begin
  Assert(aFileName<>'');

  
  aImgFileName:=ExtractFileName(aImgFileName);
  

  s:=DEF_FILE;
  s:=ReplaceStr(s, ':file',  aImgFileName);

  s:=ReplaceStr(s, ':lon_max', FloatToStr(Trunc(aXYbounds.y_Max)));
  s:=ReplaceStr(s, ':lon_min', FloatToStr(Trunc(aXYbounds.y_Min)));
  s:=ReplaceStr(s, ':lat_min', FloatToStr(Trunc(aXYbounds.x_Min)));
  s:=ReplaceStr(s, ':lat_max', FloatToStr(Trunc(aXYbounds.x_Max)));

  
  s:=ReplaceStr(s, ':w', IntToStr(aImg_width));  
  s:=ReplaceStr(s, ':h', IntToStr(aImg_Height));  

 // ShowMessage(s);
  
  StrToFile_ (aFileName, s);
  
end;



// ---------------------------------------------------------------
procedure TGDal.LoadFromFile_GDalInfo(aFileName: string);
// ---------------------------------------------------------------
//  procedure DoParse(aFileName: string);
//  begin
//  
//  end;


var
  I: Integer;
  k1: Integer;
  k2: Integer;
  oSList: TStringList;
  s: string;

  sArr: TStrArray;

//  Img_width, FImg_Height : integer;    
//
//  bounds: TXYBounds;

begin
  Assert(FileExists(aFileName));


  oSList:=TStringList.Create;
  oSList.LoadFromFile(aFileName);


  for I := 0 to oSList.Count-1 do
  begin
    s:=oSList[i];
  
    // ---------------------------------------------------------------          
    if Pos('Size is', s)>0 then
    begin
      s:=Copy(s, Length('Size is')+1, 100);
                     
      sArr:=StringToStrArray_new(s,',');

      FImg_width :=AsInteger(sArr[0]); 
      FImg_Height:=AsInteger(sArr[1]);

    end else

    // ---------------------------------------------------------------          
    if Pos('Lower Left', s)>0 then
    begin
      k1:=Pos('(',s);
      k2:=Pos(')',s,k1);
      s:=Copy(s, k1+1, k2-k1-1);

      sArr:=StringToStrArray_new(s,',');
 
      XYbounds.Y_Min:= AsFloat(sArr[0]);
      XYbounds.X_Min:= AsFloat(sArr[1]);
 
    end else

    
    // ---------------------------------------------------------------          
    if Pos('Upper Right', s)>0 then
    begin
      k1:=Pos('(',s);
      k2:=Pos(')',s,k1);
      s:=Copy(s, k1+1, k2-k1-1);
      
      sArr:=StringToStrArray_new(s,',');

      XYbounds.Y_Max:= AsFloat(sArr[0]);
      XYbounds.X_Max:= AsFloat(sArr[1]);

    end;
        

//
//Upper Right 
//Lower Right 
//    
    
    
  end;
    

  FreeAndNil(oSList);
  
end;

// ---------------------------------------------------------------
procedure TGDal.BmpToTabFile(aFileName_bmp,aFileName_tab: string);
// ---------------------------------------------------------------
var
  rec: TXYBounds;

  s: string;
  sBat: string;
  sDir: string;
  sInfo1: string;
  sInfo2: string;
  sTif1: string;
  sTif2: string;
  s_ullr: string;

begin
//         //  // epsgcode  - ��� EPSG, ��� ��-42 ���� 2-32 : 28402-28432, ��� ��-95 ���� 4-32: 20004-20032 



  rec.LoadFromIniFile( ChangeFileExt(aFileName_bmp,'.ini'));


// rec.Y_Min := Round(rec.Y_Min) mod 1000000;
//  rec.Y_Max := Round(rec.Y_Max) mod 1000000;  

  

  sDir:= ExtractFilePath(aFileName_bmp);
  sDir:= IncludeTrailingBackslash(sDir +  ExtractFileName(ChangeFileExt(aFileName_bmp,'')));
                                           
  
//  cfe (aFileName_bmp));
  
  sTif1 := sDir + 'gk.tif';
//  sTif2 := ChangeFileExt(aFileName_tab,'_3395.tif');
  sTif2 := ChangeFileExt(aFileName_tab,'.tif');  
    
  sInfo1 := sDir + 'info1.txt';  
  sInfo2 := sDir + 'info2.txt';  
  sBat   := sDir + 'run.bat';  

//  oSList:=TStringList.Create;
  


  Assert( RightStr(FPath_epsg_csv,1)<>'\');
  
  

  s_ullr := Format('%1.1f %1.1f %1.1f %1.1f', [rec.Y_Min, rec.X_Max, rec.Y_Max, rec.X_Min]);
  
//  s_bounds := Format('%1.1f %1.1f %1.1f %1.1f',[rec.Y_Min, rec.X_Min, rec.Y_Max, rec.X_Max ]);  
 

  s:= '%path%gdal_translate.exe --config GDAL_DATA "%GDAL_DATA%"   -a_srs  "EPSG:%EPSG%" -of GTiff  -a_ullr %bounds%   "%bmp%" "%tif1%"  ' + CRLF+
      '%path%gdalwarp.exe   -overwrite    --config GDAL_DATA "%GDAL_DATA%"   -t_srs  EPSG:3395  -of GTiff   "%tif1%" "%tif2%"  '+ CRLF+
      '%path%gdalinfo.exe "%tif1%" > "%txt1%" '+ CRLF+
      '%path%gdalinfo.exe "%tif2%" > "%txt2%" ';

  s:=ReplaceStr(s,'%path%', FPath_bin);
  s:=ReplaceStr(s,'%GDAL_DATA%', FPath_epsg_csv);  
  s:=ReplaceStr(s,'%bmp%', aFileName_bmp);  
  s:=ReplaceStr(s,'%bounds%', s_ullr);  
  s:=ReplaceStr(s,'%tif1%', sTif1);
  s:=ReplaceStr(s,'%tif2%', sTif2);  
  s:=ReplaceStr(s,'%txt1%',  sInfo1);  
  s:=ReplaceStr(s,'%txt2%',  sInfo2);  
  s:=ReplaceStr(s,'%EPSG%',  intToStr(rec.Zone + 28400 ) );  
  
  

  StrToFile_(sBat, s);


  RunApp(sBat,'');

  LoadFromFile_GDalInfo(sInfo2);

  
  SaveTabFile (aFileName_tab, sTif2, FImg_width, FImg_Height,  XYbounds )
  
//
//  DeleteFile(sBat);
//  DeleteFile(sInfo1);  
//  DeleteFile(sInfo2);

    

     {

  
  --config GDAL_DATA "D:\OSGeo4W\share\epsg_csv"   -a_srs "+proj=utm  +zone=38 +datum=WGS84" -of GTiff  -a_ullr 413937.4  6253717.4 454361.4 6224449.3  "D:\RLF\nnovg_pl_50_2D.bmp" "D:\RLF\nnovg_pl_50_2D.tif" 
rem


gdalwarp.exe  --config GDAL_DATA "D:\OSGeo4W\share\epsg_csv"   -overwrite  -t_srs  EPSG:3395     -of GTiff    "D:\RLF\nnovg_pl_50_2D.tif" "D:\RLF\nnovg_pl_50_2D.3395.tif" 

gdalinfo.exe "D:\RLF\nnovg_pl_50_2D.3395.tif" -proj4 > "nnovg_pl_50_2D.3395.prj4"


  }
  

end;


procedure TGDal_Test();
var
  obj: TGDal;
begin
 // ShowMessage('TGDal_Test');

  obj:=TGDal.Create;
  obj.BmpToTabFile('d:\rlf\nnovg_pl_50_2D.bmp', 'd:\rlf\nnovg_pl_50_2D__.tab');
 // obj.LoadFromFile_GDalInfo('');

  
end;

begin
 // TGDal_Test;

//  TGDal.Run;
 // Test;
//  LoadFromFile('');


end.


{                        D:\RLF\nnovg_pl_50_2D.bmp

gdal_translate.exe  --config GDAL_DATA "D:\OSGeo4W\share\epsg_csv"   -a_srs "+proj=utm  +zone=38 +datum=WGS84" -of GTiff  -a_ullr 413937.4  6253717.4 454361.4 6224449.3  "D:\RLF\nnovg_pl_50_2D.bmp" "D:\RLF\nnovg_pl_50_2D.tif" 
rem


gdalwarp.exe  --config GDAL_DATA "D:\OSGeo4W\share\epsg_csv"   -overwrite  -t_srs  EPSG:3395     -of GTiff    "D:\RLF\nnovg_pl_50_2D.tif" "D:\RLF\nnovg_pl_50_2D.3395.tif" 

gdalinfo.exe "D:\RLF\nnovg_pl_50_2D.3395.tif" -proj4 > "nnovg_pl_50_2D.3395.prj4"




Driver: GTiff/GeoTIFF
Files: D:\RLF\nnovg_pl_50_2D.3395.tif
Size is 1022, 748
Coordinate System is:
LOCAL_CS["WGS 84 / World Mercator",
    GEOGCS["WGS 84",
        DATUM["unknown",
            SPHEROID["unretrievable - using WGS84",6378137,298.257223563]],
        PRIMEM["Greenwich",0],
        UNIT["degree",0.0174532925199433]],
    AUTHORITY["EPSG","3395"],
    UNIT["metre",1]]
PROJ.4 string is:
''
Origin = (4854070.042256287300000,7608022.355274244200000)
Pixel Size = (71.905841401870617,-71.905841401870617)
Metadata:
  AREA_OR_POINT=Area
Image Structure Metadata:
  INTERLEAVE=PIXEL
Corner Coordinates:
Upper Left  ( 4854070.042, 7608022.355) 
Lower Left  ( 4854070.042, 7554236.786) 
Upper Right ( 4927557.812, 7608022.355) 
Lower Right ( 4927557.812, 7554236.786) 

Band 1 Block=1022x2 Type=Byte, ColorInterp=Red
Band 2 Block=1022x2 Type=Byte, ColorInterp=Green
Band 3 Block=1022x2 Type=Byte, ColorInterp=Blue
}
