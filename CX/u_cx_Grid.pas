unit u_cx_Grid;

interface
uses
   Classes, cxGridDBBandedTableView, cxGridDBTableView, Controls,
   cxCheckBox,

   SysUtils, Db, Dialogs, Forms;


function cx_BandedTableView_GetSelectedList(aBandedTableView:
    TcxGridDBBandedTableView; aKey: string = ''): TStringList;

function cx_Grid_Check(aBandedTableView: tobject): Boolean;

procedure cx_Grid_Init(aForm: TForm);


implementation


// ---------------------------------------------------------------
function cx_Grid_Check(aBandedTableView: tobject): Boolean;
// ---------------------------------------------------------------
var
  oStrList: TStringList;
  oDataSet: TDataSet;


  procedure DoCheck(aName, aFieldName: string);
  begin
    if aFieldName='' then
      oStrList.Add(Format('Column %s: fieldname=''''', [aName]));

    if oDataSet.Active then
      if not Assigned( oDataSet.FindField(aFieldName)) then
        oStrList.Add(Format('Column %s: fieldname %s not correct', [aName,aFieldName]));

  end;

var
  I: Integer;
  sFieldName: string;

  sName: string;

  oTableView : TcxGridDBTableView;
  oBandedTableView: TcxGridDBBandedTableView;
begin
  oStrList:=TStringList.Create;

  if aBandedTableView is TcxGridDBTableView then
  begin
    oTableView :=(aBandedTableView as TcxGridDBTableView);

    oDataSet := oTableView.DataController.DataSource.DataSet;
    Assert(Assigned(oDataSet), 'Value not assigned');

    for i := 0 to oTableView.ColumnCount-1 do
    begin
      sName := oTableView.Columns[i].Name;
      sFieldName := oTableView.Columns[i].DataBinding.FieldName;

      DoCheck(sName, sFieldName);
    end;
  end;


  if aBandedTableView is TcxGridDBBandedTableView then
  begin
    oBandedTableView :=(aBandedTableView as TcxGridDBBandedTableView);

    oDataSet := oBandedTableView.DataController.DataSource.DataSet;
    Assert(Assigned(oDataSet), 'Value not assigned');

    for i := 0 to oBandedTableView.ColumnCount-1 do
    begin
      sName := oBandedTableView.Columns[i].Name;
      sFieldName := oBandedTableView.Columns[i].DataBinding.FieldName;

      DoCheck(sName, sFieldName);
    end;
  end;



  if oStrList.Count>0 then
   raise Exception.Create('fields not found: '+ oStrList.Text);
//   ShowMessage( oStrList.Text);

  Result := oStrList.Count=0;

  oStrList.Free;




end;



// ---------------------------------------------------------------
function cx_BandedTableView_GetSelectedList(aBandedTableView:
    TcxGridDBBandedTableView; aKey: string = ''): TStringList;
// ---------------------------------------------------------------
var
  I: Integer;
//  v: Variant;
  oColumn: TcxGridDBBandedColumn;
 // oDS: TDataSet;
 // j: integer;
  iID: integer;
 // sKey: string;
begin
  Result := TStringList.Create;

 // oDS:=aBandedTableView.DataController.DataSet;
 // oDS.DisableControls;

  if aKey='' then
    aKey := aBandedTableView.DataController.KeyFieldNames;

  Assert(aKey<>'');

  oColumn:=aBandedTableView.GetColumnByFieldName(aKey);
  Assert(Assigned(oColumn));

  for I := 0 to aBandedTableView.Controller.SelectedRowCount - 1 do
  begin
    iID:=aBandedTableView.Controller.SelectedRows[i].Values[oColumn.Index];
    Result.AddObject (IntToStr(iID), Pointer(iID));
  end;

 // oDS.EnableControls;
end;

//---------------------------------------------------------------------------
procedure cx_Grid_Init(aForm: TForm);
//---------------------------------------------------------------------------
var
  I: Integer;
  j: Integer;
  oComponent: TComponent;
  s: string;

  oLog: TStringList;

  oDBTableView: TcxGridDBTableView;
  oColumn: TcxGridDBColumn;
  oProperties: TcxCheckBoxProperties;

begin
  oLog:=TStringList.Create;



  for I := 0 to aForm.ComponentCount - 1 do    // Iterate
  begin
    oComponent:= aForm.Components[i];


    s:=oComponent.ClassName;


    //---------------------------------------------------------------------------
    if oComponent is TcxGridDBTableView then
    //---------------------------------------------------------------------------
    begin
      oDBTableView:=TcxGridDBTableView(oComponent);

      oLog.Add(oDBTableView.Name);

    //  ShowMessage(oDBTableView.Name);

      for j:=0 to oDBTableView.ColumnCount-1 do
      begin
        oColumn:=oDBTableView.Columns[j];

         //------------------------------------------------------
        if oColumn.PropertiesClass  = TcxCheckBoxProperties then
        //------------------------------------------------------
        begin
          oLog.Add('TcxCheckBoxProperties:'+ oColumn.Name);

          oProperties:=TcxCheckBoxProperties(oColumn.Properties);
          oProperties.ImmediatePost := True;
          oProperties.NullStyle:=nssUnchecked;

        //  oProperties.UseAlignmentWhenInplace := True;

//          oReg.WriteInteger('', oEditRow.Name, IIF(VarIsNull(v), False, v));
        end else
        ;

      end;

    end;



//    oControl.Con

//    if oControl is

  end;


 // ShowMessage(oLog.text);

  FreeAndNil(oLog);

end;




end.


{





}
