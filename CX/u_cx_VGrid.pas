unit u_cx_VGrid;

interface
uses
  SysUtils,Forms,Dialogs ,Classes, Controls,  Registry,
  cxVGrid, Variants, cxGraphics, Graphics, cxCheckBox,    cxTextEdit,
  cxDBLookupComboBox,cxButtonEdit, cxControls, cxDBVGrid, cxLookAndFeels,
  cxDropDownEdit,  cxEdit,

  u_func  ;

//  PBFolderDialog;


  procedure cx_InitVerticalGrid(Sender: TcxVerticalGrid);
  procedure cx_InitDBVerticalGrid(Sender: TcxDBVerticalGrid);

  procedure cx_BrowseFileFolder(aOwnerForm: TComponent; Sender: TObject);

  function cx_BrowseFile(Sender: TObject; aFilter: String; aDefaultExt: String=''): boolean;

  procedure cx_SetButtonEditText(Sender: TObject; aText: string);

  procedure cx_VerticalGrid_SaveToReg(aVerticalGrid: TcxVerticalGrid; aRegPath: string);
  procedure cx_VerticalGrid_LoadFromReg(aVerticalGrid: TcxVerticalGrid; aRegPath: string);

  procedure cx_ColorRow(aSender: TcxEditorRow;
                        aRowArr: array of TcxEditorRow;
                        ACanvas: TcxCanvas;
                        aIsRequiredParam: boolean = false;
                        Color: TColor = clSilver );

//  procedure cx_Dlg_ExportToFile (aGrid: TcxGrid);

//  procedure InitRes;

  procedure cx_SetReadOnly(aRowArray: array of TcxEditorRow; aReadOnly: boolean = true);

// -------------------------
//TcxEditorRow
// -------------------------

function cx_GetRowItemIndex(aEditorRow: TcxEditorRow): Integer;
procedure cx_SetRowItemIndex(aEditorRow: TcxEditorRow; aItemIndex: Integer);
procedure cx_SetRowItemValues(aEditorRow: TcxEditorRow; aStrArr: array of string);

procedure cx_DBVGrid(Sender: TcxDBVerticalGrid);

procedure cx_DBVerticalGrid_SetCaptions(aGrid: TcxDBVerticalGrid; aValues :
    array of string);

procedure cx_DBVerticalGrid_AlignLeft(aGrid: TcxDBVerticalGrid);


implementation

uses
  System.IniFiles;



procedure cx_InitVerticalGrid(Sender: TcxVerticalGrid);
begin
  Sender.BorderStyle:=cxcbsNone;
//  Sender.LookAndFeel.Kind:=lfStandard;
  Sender.LookAndFeel.Kind:=lfFlat;
//  Sender.OptionsView.ShowEditButtons :=  ecsbAlways;
end;


procedure cx_InitDBVerticalGrid(Sender: TcxDBVerticalGrid);
begin
  Sender.BorderStyle:=cxcbsNone;
//  Sender.LookAndFeel.Kind:=lf Standard;
  Sender.LookAndFeel.Kind:=lfFlat;
 // Sender.OptionsView.ShowEditButtons :=  ecsbAlways;

//  lfOffice11
end;


//-------------------------------------------------------------------
procedure cx_BrowseFileFolder(aOwnerForm: TComponent; Sender: TObject);
//-------------------------------------------------------------------


//  oDialog: TPBFolderDialog;
//  oButtonEdit: TcxButtonEdit;
//
begin
//  Assert(Sender is TcxButtonEdit);
//  oButtonEdit :=Sender as TcxButtonEdit;
//
//  oDialog:=TPBFolderDialog.Create(aOwnerForm);
//  oDialog.Folder:=oButtonEdit.Text;
//
//  if oDialog.Execute then
//    cx_SetButtonEditText (oButtonEdit, oDialog.Folder);
//
//  oDialog.Free;

end;


//-------------------------------------------------------------------
function cx_BrowseFile(Sender: TObject; aFilter: String; aDefaultExt: String=''): boolean;
//-------------------------------------------------------------------
var
  oDialog: TOpenDialog;
  sFileName: string;
  oButtonEdit: TcxButtonEdit;
begin
  Assert(Sender is TcxButtonEdit);
  oButtonEdit :=Sender as TcxButtonEdit;


  oDialog:=TOpenDialog.Create(Application);
  oDialog.Filter:=aFilter;

  oDialog.FileName:=oButtonEdit.Text;
  oDialog.InitialDir:=ExtractFileDir(oDialog.FileName);

  Result:=oDialog.Execute;

  if Result then
  begin
    sFileName := oDialog.FileName;
    if aDefaultExt<>'' then
      sFileName:=ChangeFileExt(sFileName, aDefaultExt);

    cx_SetButtonEditText (oButtonEdit, sFileName);

  //  oRow.Properties.Value := sFileName;
  //  oRow.VerticalGrid.HideEdit;
  end;

  FreeAndNil(oDialog);
end;


// ---------------------------------------------------------------
function cx_GetRowItemIndex(aEditorRow: TcxEditorRow): Integer;
// ---------------------------------------------------------------
begin
  Assert(aEditorRow.Properties.EditPropertiesClass = TcxComboBoxProperties );


   Result := -1;

   if not VarIsNull(aEditorRow.Properties.Value) then
     if aEditorRow.Properties.EditPropertiesClass = TcxComboBoxProperties then
     begin
       with TcxComboBoxProperties(aEditorRow.Properties.EditProperties) do
         Result := Items.IndexOf(aEditorRow.Properties.Value);
     end;
end;

// ---------------------------------------------------------------
procedure cx_SetRowItemIndex(aEditorRow: TcxEditorRow; aItemIndex: Integer);
// ---------------------------------------------------------------
begin
  Assert(aEditorRow.Properties.EditPropertiesClass = TcxComboBoxProperties );


   if aEditorRow.Properties.EditPropertiesClass = TcxComboBoxProperties then
   begin
     with TcxComboBoxProperties(aEditorRow.Properties.EditProperties) do
     begin
       Assert(Items.Count>0);

       if aItemIndex <= Items.Count-1 then
         aEditorRow.Properties.Value :=Items[aItemIndex];
     end;
   end;

end;


// ---------------------------------------------------------------
procedure cx_SetRowItemValues(aEditorRow: TcxEditorRow; aStrArr: array of string);
// ---------------------------------------------------------------
var
  I: Integer;
begin
  Assert(aEditorRow.Properties.EditPropertiesClass = TcxComboBoxProperties );


   if aEditorRow.Properties.EditPropertiesClass = TcxComboBoxProperties then
   begin
     with TcxComboBoxProperties(aEditorRow.Properties.EditProperties) do
     begin
       Items.Clear;

       for I := 0 to High(aStrArr) do
         Items.Add(aStrArr[i]);
     end;
   end;

end;
     

//--------------------------------------------------------------
procedure cx_SetButtonEditText(Sender: TObject; aText: string);
//--------------------------------------------------------------
var
  bReadOnly : boolean;
begin
  Assert(Sender is TcxButtonEdit);

  bReadOnly:=(Sender as TcxButtonEdit).Properties.ReadOnly;

  (Sender as TcxButtonEdit).Properties.ReadOnly := False;

  (Sender as TcxButtonEdit).Text:=aText;
  (Sender as TcxButtonEdit).PostEditValue;

  (Sender as TcxButtonEdit).Properties.ReadOnly := bReadOnly;
end;


//------------------------------------------------------
procedure cx_VerticalGrid_SaveToReg(aVerticalGrid: TcxVerticalGrid; aRegPath:
    string);
//------------------------------------------------------
var
  I: Integer;
  oReg: TRegIniFile;
  v: Variant;
  oEditRow: TcxEditorRow;
begin
  Assert(aRegPath<>'');

  aRegPath:=IncludeTrailingBackslash (aRegPath) + aVerticalGrid.Name;


  oReg:= TRegIniFile.Create(aRegPath);

  for I := 0 to aVerticalGrid.Rows.Count - 1 do
  begin
    if aVerticalGrid.Rows[i] is TcxEditorRow then
    begin
      try
        oEditRow:=TcxEditorRow(aVerticalGrid.Rows[i]);
        v:=oEditRow.Properties.Value;

//        oEditRow.Properties.EditPropertiesClass.ClassName;

        //------------------------------------------------------
        if oEditRow.Properties.EditPropertiesClass = TcxLookupComboBoxProperties then
        begin
          oReg.WriteInteger('', oEditRow.Name, IIF(VarIsNull(v), False, v));
        end else

        //------------------------------------------------------
        if oEditRow.Properties.EditPropertiesClass = TcxCheckBoxProperties then
        begin
          oReg.WriteBool('', oEditRow.Name, IIF(VarIsNull(v), False, v));
        end else

        //------------------------------------------------------
        if oEditRow.Properties.EditPropertiesClass = TcxTextEditProperties then
        begin
          oReg.WriteString('', oEditRow.Name, VarToStr(v));
        end else

        //------------------------------------------------------
        if oEditRow.Properties.EditPropertiesClass = TcxButtonEditProperties then
        begin
          oReg.WriteString ('', oEditRow.Name,        VarToStr(v));
          oReg.WriteInteger('', oEditRow.Name+'_TAG', oEditRow.Tag);
        end else

          oReg.WriteString('', oEditRow.Name, VarToStr(v));

      except
        raise Exception.Create('');
      end;
    end;
  end;

  oReg.Free;

end;

//------------------------------------------------------
procedure cx_VerticalGrid_LoadFromReg(aVerticalGrid: TcxVerticalGrid; aRegPath:
    string);
//------------------------------------------------------
var
  I: Integer;
  oReg: TRegIniFile;
  v: Variant;
  oEditRow: TcxEditorRow;
begin
  Assert(aRegPath<>'');

  aRegPath:=IncludeTrailingBackslash (aRegPath) + aVerticalGrid.Name;

  oReg:= TRegIniFile.Create(aRegPath);

  for I := 0 to aVerticalGrid.Rows.Count - 1 do
  begin
    if aVerticalGrid.Rows[i] is TcxEditorRow then
    begin
      oEditRow:=TcxEditorRow(aVerticalGrid.Rows[i]);

      v:=oEditRow.Properties.Value;

      //------------------------------------------------------
      if oEditRow.Properties.EditPropertiesClass = TcxLookupComboBoxProperties then
      begin
        v:=oReg.ReadInteger('', oEditRow.Name, IIF(VarIsNull(v), False, v));
      end else

      //------------------------------------------------------
      if oEditRow.Properties.EditPropertiesClass = TcxCheckBoxProperties then
      begin
        v:=oReg.ReadBool('', oEditRow.Name, IIF(VarIsNull(v), False, v));
      end else

      //------------------------------------------------------
      if oEditRow.Properties.EditPropertiesClass = TcxTextEditProperties then
      begin
        v:=oReg.ReadString('', oEditRow.Name, VarToStr(v));
      end else

      //------------------------------------------------------
      if oEditRow.Properties.EditPropertiesClass = TcxButtonEditProperties then
      begin
        oEditRow.Tag:=oReg.ReadInteger('', oEditRow.Name+'_TAG', oEditRow.Tag);
        v :=oReg.ReadString ('', oEditRow.Name, VarToStr(v));

//         oReg.WriteString('', oEditRow.Name, VarToStr(v));
//        oReg.WriteInteger('', oEditRow.Name+'_TAG', oEditRow.Tag);
      end else
 //       raise Exception.Create('cx_VerticalGrid_SaveToReg(: '+ oEditRow.Properties.EditPropertiesClass.ClassName);

       v:=oReg.ReadString('', oEditRow.Name, VarToStr(v));

       oEditRow.Properties.Value:=v;
    end;

  end;

  oReg.Free;

end;

//----------------------------------------------
procedure cx_ColorRow(aSender: TcxEditorRow;
                      aRowArr: array of TcxEditorRow;
                      ACanvas: TcxCanvas;
                      aIsRequiredParam: boolean = false;
                      Color: TColor = clSilver );
//----------------------------------------------
var
  I: Integer;
begin
  for I := 0 to High(aRowArr) do
    if aSender = aRowArr[i] then
    begin
      if aRowArr[i].Properties.Options.Editing=False then
        ACanvas.Brush.Color:=clGrayText
      else

      if (aIsRequiredParam) then
        ACanvas.Brush.Color:=Color;
    end;
end;

//----------------------------------------------------
procedure cx_SetReadOnly(aRowArray: array of TcxEditorRow; aReadOnly: boolean = true);
//----------------------------------------------------
var
  I: Integer;
begin
  for I := 0 to High(aRowArray) do
    aRowArray[i].Properties.EditProperties.ReadOnly:= aReadOnly;
end;


procedure cx_DBVGrid(Sender: TcxDBVerticalGrid);
var
  I: Integer;
  oRow: TcxDBEditorRow;
begin
  for I := 0 to Sender.Rows.Count - 1 do
  begin
    if Sender.Rows[i] is TcxDBEditorRow then
    begin
      oRow:=Sender.Rows[i] as TcxDBEditorRow;
      if oRow.Properties.DataBinding.FieldName='' then
        ShowMessage('procedure cx_DBVGrid(Sender: TcxDBVerticalGrid);  '+oRow.Name);
    end;
  end;


end;


//----------------------------------------------------------------------------
procedure cx_DBVerticalGrid_AlignLeft(aGrid: TcxDBVerticalGrid);
//----------------------------------------------------------------------------
var 
  I,j: Integer; 

  oRow: TcxDBEditorRow;  
  p: TcxCustomEditPropertiesClass;
begin
// oEditRow.Properties.EditPropertiesClass:=TcxLookupComboBoxProperties;



  for I := 0 to aGrid.Rows.Count -1 do
  begin
    // ------------------------------------------
    if aGrid.Rows[i] is TcxDBEditorRow then
    begin
      oRow:=aGrid.Rows[i] as TcxDBEditorRow;

     // s:=
     p:= oRow.Properties.EditPropertiesClass ; //:=TcxLookupComboBoxProperties;

//      s:=oRow.Properties.ClassName;
      
     // sFieldName:=oRow.Properties.DataBinding.FieldName;
      
    end;
  end;
    
end;




//----------------------------------------------------------------------------
procedure cx_DBVerticalGrid_SetCaptions(aGrid: TcxDBVerticalGrid; aValues :
    array of string);
//----------------------------------------------------------------------------
var I,j: Integer;
  sFieldName: string;

  oRow: TcxDBEditorRow;  
  oMultiRow: TcxDBMultiEditorRow;
begin
  i:=aGrid.Rows.Count;
  

  for I := 0 to aGrid.Rows.Count -1 do
  begin
    // ------------------------------------------
    if aGrid.Rows[i] is TcxDBMultiEditorRow then
    begin
      oMultiRow:=aGrid.Rows[i] as TcxDBMultiEditorRow;
    end else


    // ------------------------------------------
    if aGrid.Rows[i] is TcxDBEditorRow then
    begin
      oRow:=aGrid.Rows[i] as TcxDBEditorRow;

      sFieldName:=oRow.Properties.DataBinding.FieldName;

      for j := 0 to (Length(aValues) div 2)-1 do
        if Eq(aValues[j*2], sFieldName) then
        begin
          oRow.Properties.Caption:=aValues[j*2+1];

        end;
      
      
      //='' then
       // ShowMessage('procedure cx_DBVGrid(Sender: TcxDBVerticalGrid);  '+oRow.Name);
    end;

  
   // sFieldName:=aGrid.Rows[i].DataBinding.FieldName;

//    
//    if sFieldName<>'' then
//      for j := 0 to High(aFieldNames) do
//        if Eq(sFieldName, aFieldNames[j][0]) then
//         aGrid.Columns[i].Caption := aFieldNames[j][1]
         
  end;
end;



//----------------------------------------------------------------------------
procedure cx_DBVerticalGrid_SetCaptions_FromIniFile(aGrid: TcxDBVerticalGrid;
    aFileName, aSection: string);
//----------------------------------------------------------------------------
var I,j: Integer;
  sFieldName: string;

  oRow: TcxDBEditorRow;  
  oMultiRow: TcxDBMultiEditorRow;
  
  oIni: TIniFile;
  sCaption: string;

begin
  i:=aGrid.Rows.Count;
  

  Assert(aFileName<>'');
  Assert(aSection<>'');  

  oIni:=TIniFile.Create(aFileName);
       
  
  for I := 0 to aGrid.Rows.Count -1 do
  begin
    // ------------------------------------------
    if aGrid.Rows[i] is TcxDBMultiEditorRow then
    begin
      oMultiRow:=aGrid.Rows[i] as TcxDBMultiEditorRow;
    end else


    // ------------------------------------------
    if aGrid.Rows[i] is TcxDBEditorRow then
    begin
      oRow:=aGrid.Rows[i] as TcxDBEditorRow;

      sFieldName:=oRow.Properties.DataBinding.FieldName;


      sCaption:=oIni.ReadString(aSection,sFieldName,'');
      if sCaption<>'' then
          oRow.Properties.Caption:=sCaption;

      
      //='' then
       // ShowMessage('procedure cx_DBVGrid(Sender: TcxDBVerticalGrid);  '+oRow.Name);
    end;

  
   // sFieldName:=aGrid.Rows[i].DataBinding.FieldName;

//    
//    if sFieldName<>'' then
//      for j := 0 to High(aFieldNames) do
//        if Eq(sFieldName, aFieldNames[j][0]) then
//         aGrid.Columns[i].Caption := aFieldNames[j][1]
         
  end;

  
  FreeAndNil(oIni);
  
end;



end.


