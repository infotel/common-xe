unit u_cx_VGrid_manager;

interface
uses
 System.Classes,  

  Xml.XMLIntf,  Forms, SysUtils, Variants, XMLDoc,  Dialogs,

//  u_xml1,
   
  
 // u_XML_new,
 
  
   cxDropDownEdit, cxVGrid
   ;


type
  TcxVGridManager = class(TObject)
  private
    cxVerticalGrid1: TcxVerticalGrid;
  public
    constructor Create(acxVerticalGrid: TcxVerticalGrid);

    procedure SetValue(aName: string; aValue: Variant);

    
    function AddCategory(aParentRow: TcxCustomRow; aCaption: string; aName: string = ''): TcxCustomRow;

    function AddRow(aCaption: string; aName: string = ''): TcxEditorRow; overload;

    function AddRow(aParentRow: TcxCustomRow; aCaption: string; aName: string = ''): TcxEditorRow; overload;

    function AddComboBox(aCaption: string): TcxEditorRow;
    procedure AssignComboBoxItems(aRow: TcxEditorRow; aItems: tstrings);

    procedure ClearRows;
    procedure ClearRows1;
//    procedure LoadFromXmlFile(aFileName, aTag: string);
  end;


implementation



constructor TcxVGridManager.Create(acxVerticalGrid: TcxVerticalGrid);
begin
//  inherited;

  cxVerticalGrid1:=acxVerticalGrid;
end;

// ---------------------------------------------------------------
function TcxVGridManager.AddCategory(aParentRow: TcxCustomRow; aCaption:
    string; aName: string = ''): TcxCustomRow;
// ---------------------------------------------------------------
begin
  

  if Assigned(aParentRow) then
    Result:=cxVerticalGrid1.AddChild(aParentRow, TcxCategoryRow)
  else
    Result:=cxVerticalGrid1.Add(TcxCategoryRow);

  if aName<>'' then
    Result.Name:=aName;
    
  (Result as TcxCategoryRow).Properties.Caption:=aCaption;

end;

function TcxVGridManager.AddRow(aCaption: string; aName: string = ''):
    TcxEditorRow;
begin
  Result := AddRow(nil, aCaption, aName);
end;

// ---------------------------------------------------------------
function TcxVGridManager.AddRow(aParentRow: TcxCustomRow; aCaption: string;
    aName: string = ''): TcxEditorRow;
// ---------------------------------------------------------------
var
  oEditRow: TcxEditorRow;
begin
  if Assigned(aParentRow) then
     Result:=cxVerticalGrid1.AddChild(aParentRow, TcxEditorRow) as TcxEditorRow
  else
     Result:=cxVerticalGrid1.Add(TcxEditorRow) as TcxEditorRow;


  oEditRow:=Result as TcxEditorRow;

  if aName<>'' then
    oEditRow.Name:=aName;

  
  oEditRow.Properties.Caption               :=aCaption;// + ' / '+ aFieldInfo.FieldName;
//  oEditRow.Properties.DataBinding.FieldName :=aFieldName;


//  oEditRow:=Result as TcxEditorRow;


end;


procedure TcxVGridManager.AssignComboBoxItems(aRow: TcxEditorRow; aItems: tstrings);
begin
  TcxComboBoxProperties(aRow.Properties.EditProperties).Items.Assign (aItems);

 // cxVerticalGrid1.ClearRows;

//  cxVerticalGrid1: TcxDBVerticalGrid;
end;


// ---------------------------------------------------------------
function TcxVGridManager.AddComboBox(aCaption: string): TcxEditorRow;
// ---------------------------------------------------------------
var
  oEditRow: TcxEditorRow;
begin
  Result := AddRow(nil, aCaption) as TcxEditorRow;

  oEditRow:=Result;// as TcxEditorRow;
  oEditRow.Properties.EditPropertiesClass:=TcxComboBoxProperties;

  with TcxComboBoxProperties(oEditRow.Properties.EditProperties) do
  begin
  //  OnCloseUp     := DoOnPickCloseUp_CX;
    UseMouseWheel := False;
    DropDownRows  :=20;
    DropDownListStyle:=lsFixedList;
  end;

end;

procedure TcxVGridManager.ClearRows;
begin
  cxVerticalGrid1.ClearRows;

//  cxVerticalGrid1: TcxDBVerticalGrid;
end;

procedure TcxVGridManager.ClearRows1;
begin
  // TODO -cMM: TcxVGridManager.ClearRows1 default body inserted
end;



procedure TcxVGridManager.SetValue(aName: string; aValue: Variant);
var
  oEditRow: TcxEditorRow;
  oRow: TcxCustomRow;
begin
  oRow:=cxVerticalGrid1.RowByName(aName);

  Assert(Assigned(oRow));
  
  oEditRow:=oRow as TcxEditorRow;
  oEditRow.Properties.Value:=aValue;
  
  
  
//  oRow.
  
end;





end.

{

// ---------------------------------------------------------------
procedure TLib.LoadFromXmlFile(aFileName: string);
// ---------------------------------------------------------------
var
  I: Integer;
  oXMLDoc: TXMLDoc;

  vGroup,vReport,vDocument,vNode: IXMLNode;
  
begin
  Assert(FileExists(aFileName), aFileName);

  oXMLDoc:=TXMLDoc.Create;
//  oXMLDoc.LoadFromFile(aFileName);

  vDocument := oXMLDoc.DocumentElement;

 // vGroup:=vDocument.AddChild('Sites');
     
  vGroup := XMLFindNode(vDocument, 'Sites');

  if Assigned(vGroup) then
    Sites.LoadFromXML(vGroup);

//    for I := 0 to vGroup.ChildNodes.Count-1 do
//      Sites.AddItem.LoadFromXML(vGroup.ChildNodes[i]);
               
                     
  
  FreeAndNil(oXMLDoc);
  
  
end;



// ---------------------------------------------------------------
procedure TcxVGridManager.LoadFromXmlFile(aFileName, aTag: string);
// ---------------------------------------------------------------

//
//  // ---------------------------------------------------------------
//  procedure DoInsertItem(  oCategoryRow: TcxCustomRow);
//  // ---------------------------------------------------------------
//
//
//
//  if Assigned(vGroup) then
//  begin           
//    for I := 0 to vGroup.ChildNodes.Count-1 do
//    begin
//      vNode:=vGroup.ChildNodes[i];
//    
//      sName   :=XMLAttribute(vNode,  'name');
//
//      sCaption:=XMLAttribute(vNode,  'caption');
//      sComment:=XMLAttribute(vNode,  'comment');
//                  
//      oCategoryRow:=AddCategory(nil,  sCaption+sComment, sName);  
//      
//      
//    end;
//      
//


var
  I: Integer;
  iItem: Integer;
  k: Integer;
  oXMLDoc: TXMLDoc;

  vNode1,vGroup,vReport,vDocument,vNode: IXMLNode;

  oCategoryRow: TcxCustomRow;
  oRow: TcxEditorRow;
  
  sCaption: string;
  sComment: string;
  sName: string;

begin
  Assert(FileExists(aFileName), aFileName);

  oXMLDoc:=TXMLDoc.Create;
  oXMLDoc.LoadFromFile(aFileName);

  vDocument := oXMLDoc.DocumentElement;

 // vGroup:=vDocument.AddChild('Sites');
     
  cxVerticalGrid1.ClearRows;
   
     
  k:=vDocument.ChildNodes.Count;

     
  //aTag
  vGroup := XMLFindNode(vDocument, 'link');

  if Assigned(vGroup) then
  begin           
    for I := 0 to vGroup.ChildNodes.Count-1 do
    begin
      vNode:=vGroup.ChildNodes[i];
    
      sName   :=XMLAttribute(vNode,  'name');

      sCaption:=XMLAttribute(vNode,  'caption');
      sComment:=XMLAttribute(vNode,  'comment');
                  
      oCategoryRow:=AddCategory(nil,  sCaption+sComment, sName);  
      

      for iItem := 0 to vNode.ChildNodes.Count-1 do
      begin
        vNode1:=vNode.ChildNodes[iItem];
    
        sName   :=XMLAttribute(vNode1,  'name');

        sCaption:=XMLAttribute(vNode1,  'caption');
        sComment:=XMLAttribute(vNode1,  'comment');
                  
       // oCategoryRow:=
        AddRow(oCategoryRow,  sCaption+sComment, sName);  
      
      
      end;
            

      
    end;
      

  
  
  //  oCategoryRow:=AddCategory(nil,  aCaption: string; aName: string

    
  end;


  
//    Sites.LoadFromXML(vGroup);

//    for I := 0 to vGroup.ChildNodes.Count-1 do
//      Sites.AddItem.LoadFromXML(vGroup.ChildNodes[i]);
               
                     
  
  FreeAndNil(oXMLDoc);
  
  
end;


