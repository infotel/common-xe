unit u_task;

interface

uses           
  Dialogs, Forms, System.SysUtils, 

  CodeSiteLogging,

  d_Progress
  ;
  
//  
//  
//  u_panorama_classes,
//
//  u_panorama_map_loader,
//
//  d_Progress,
//
////  u_Progress_,
//
//  dm_Main_kpt,  
//
//  u_kpt_const,
//  dm_DB_Manager,
//                                   
//  u_Panorama_geo,  
//
//  u_db
//  ;

type
  TTask = class
  protected
    FTerminated: boolean;

    procedure Log(aMsg: string);
                                 
    procedure SetProgress1(aProgress, aTotal: integer);
    procedure SetProgress2(aProgress, aTotal: integer);


    procedure SetProgressMsg1(aMsg: string);
    procedure SetProgressMsg2(aMsg: string);     
    
    procedure Execute; virtual;
    
  end;


implementation
            

procedure TTask.Execute;
begin
  FTerminated:=False;
end;

procedure TTask.Log(aMsg: string);
begin
  CodeSite.Send(aMsg);

//  Progress_Log(aMsg);

end;

procedure TTask.SetProgress1(aProgress, aTotal: integer);
begin
  Progress_SetProgress1(aProgress,aTotal, FTerminated);
end;

procedure TTask.SetProgress2(aProgress, aTotal: integer);
begin
  Progress_SetProgress2(aProgress,aTotal, FTerminated);
end;

procedure TTask.SetProgressMsg1(aMsg: string);
begin
  Progress_SetProgressMsg1(aMsg);
end;

procedure TTask.SetProgressMsg2(aMsg: string);
begin
  Progress_SetProgressMsg2(aMsg);

end;


            

end.



{

function Progress_ExecDlg(aCaption: string; aOnStartEvent: TNotifyEvent): Boolean;
function Progress_ExecDlg_proc(aOnStartEvent: TProcedureEvent; aCaption: string = ''):
    Boolean;

procedure Progress_SetProgress1(aProgress,aTotal: integer; var aTerminated: boolean);
procedure Progress_SetProgress2(aProgress,aTotal: integer; var aTerminated: boolean);

procedure Progress_SetProgressMsg1(aMsg: string);
procedure Progress_SetProgressMsg2(aMsg: string);

}