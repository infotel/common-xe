object dlg_Map_Label_Setup: Tdlg_Map_Label_Setup
  Left = 1035
  Top = 331
  BorderStyle = bsDialog
  BorderWidth = 5
  Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072' '#1087#1086#1076#1087#1080#1089#1077#1081
  ClientHeight = 453
  ClientWidth = 684
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = True
  Position = poDefault
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object cxVerticalGrid1: TcxVerticalGrid
    Left = 0
    Top = 0
    Width = 684
    Height = 281
    BorderStyle = cxcbsNone
    Align = alTop
    LookAndFeel.Kind = lfUltraFlat
    LookAndFeel.NativeStyle = False
    LookAndFeel.SkinName = ''
    OptionsView.CellTextMaxLineCount = 3
    OptionsView.AutoScaleBands = False
    OptionsView.PaintStyle = psDelphi
    OptionsView.GridLineColor = clBtnShadow
    OptionsView.RowHeaderMinWidth = 30
    OptionsView.RowHeaderWidth = 388
    OptionsView.ValueWidth = 40
    TabOrder = 0
    Version = 1
    object cxVerticalGrid1EditorRow1: TcxEditorRow
      Options.TabStop = False
      Properties.Caption = #1057#1074#1086#1081#1089#1090#1074#1072' '#1087#1086#1076#1087#1080#1089#1080
      Properties.DataBinding.ValueType = 'String'
      Properties.Options.Editing = False
      Properties.Value = Null
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object row_Overlap: TcxEditorRow
      Expanded = False
      Properties.Caption = #1055#1077#1088#1077#1082#1088#1099#1074#1072#1090#1100' '#1087#1086#1076#1087#1080#1089#1080' ('#1088#1080#1089#1086#1074#1072#1090#1100' '#1074#1089#1077')'
      Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
      Properties.EditProperties.Alignment = taLeftJustify
      Properties.EditProperties.NullStyle = nssUnchecked
      Properties.EditProperties.ReadOnly = False
      Properties.EditProperties.UseAlignmentWhenInplace = True
      Properties.EditProperties.ValueChecked = 'True'
      Properties.EditProperties.ValueGrayed = ''
      Properties.EditProperties.ValueUnchecked = 'False'
      Properties.DataBinding.ValueType = 'Variant'
      Properties.Value = Null
      ID = 1
      ParentID = 0
      Index = 0
      Version = 1
    end
    object row_DisplayWithinRange1: TcxEditorRow
      Properties.Caption = #1054#1090#1086#1073#1088#1072#1078#1072#1090#1100' '#1090#1086#1083#1100#1082#1086' '#1087#1088#1080' '#1084#1072#1089#1096#1090#1072#1073#1077
      Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
      Properties.EditProperties.Alignment = taLeftJustify
      Properties.EditProperties.NullStyle = nssUnchecked
      Properties.EditProperties.ReadOnly = False
      Properties.EditProperties.ValueChecked = 'True'
      Properties.EditProperties.ValueGrayed = ''
      Properties.EditProperties.ValueUnchecked = 'False'
      Properties.DataBinding.ValueType = 'Variant'
      Properties.Value = Null
      Visible = False
      ID = 2
      ParentID = 0
      Index = 1
      Version = 1
    end
    object row_HigherThan1: TcxEditorRow
      Expanded = False
      Properties.Caption = #1041#1086#1083#1100#1096#1077' '#1095#1077#1084' ('#1082#1084')'
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.Alignment.Horz = taLeftJustify
      Properties.EditProperties.MaxLength = 0
      Properties.EditProperties.ReadOnly = False
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 3
      ParentID = 2
      Index = 0
      Version = 1
    end
    object row_LowerThan1: TcxEditorRow
      Expanded = False
      Properties.Caption = #1052#1077#1085#1100#1096#1077' '#1095#1077#1084' ('#1082#1084')'
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.Alignment.Horz = taLeftJustify
      Properties.EditProperties.MaxLength = 0
      Properties.EditProperties.ReadOnly = False
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 4
      ParentID = 2
      Index = 1
      Version = 1
    end
    object cxVerticalGrid1EditorRow6: TcxEditorRow
      Expanded = False
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.Alignment.Horz = taLeftJustify
      Properties.EditProperties.MaxLength = 0
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 5
      ParentID = -1
      Index = 1
      Version = 1
    end
    object row_BackgroundType: TcxEditorRow
      Properties.Caption = #1060#1086#1085' '#1087#1086#1076#1087#1080#1089#1080
      Properties.EditPropertiesClassName = 'TcxComboBoxProperties'
      Properties.EditProperties.DropDownListStyle = lsFixedList
      Properties.EditProperties.Items.Strings = (
        #1053#1077#1090' '#1092#1086#1085#1072
        #1055#1088#1103#1084#1086#1091#1075#1086#1083#1100#1085#1080#1082#1086#1084
        #1054#1088#1077#1086#1083#1086#1084)
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 6
      ParentID = -1
      Index = 2
      Version = 1
    end
    object row_Background_Color: TcxEditorRow
      Expanded = False
      Properties.Caption = #1062#1074#1077#1090' '#1092#1086#1085#1072
      Properties.EditPropertiesClassName = 'TcxColorComboBoxProperties'
      Properties.EditProperties.ColorDialogType = cxcdtCustom
      Properties.EditProperties.CustomColors = <>
      Properties.EditProperties.ShowDescriptions = False
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 7
      ParentID = 6
      Index = 0
      Version = 1
    end
    object cxVerticalGrid1EditorRow9: TcxEditorRow
      Expanded = False
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.Alignment.Horz = taLeftJustify
      Properties.EditProperties.MaxLength = 0
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 8
      ParentID = -1
      Index = 3
      Version = 1
    end
    object row_FontName: TcxEditorRow
      Properties.Caption = #1064#1088#1080#1092#1090' '#1087#1086#1076#1087#1080#1089#1080
      Properties.EditPropertiesClassName = 'TcxFontNameComboBoxProperties'
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 9
      ParentID = -1
      Index = 4
      Version = 1
    end
    object row_FontSize: TcxEditorRow
      Expanded = False
      Properties.Caption = #1056#1072#1079#1084#1077#1088
      Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
      Properties.EditProperties.MinValue = 5.000000000000000000
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 10
      ParentID = 9
      Index = 0
      Version = 1
    end
    object row_Color: TcxEditorRow
      Expanded = False
      Properties.Caption = #1062#1074#1077#1090
      Properties.EditPropertiesClassName = 'TcxColorComboBoxProperties'
      Properties.EditProperties.ColorDialogType = cxcdtCustom
      Properties.EditProperties.CustomColors = <>
      Properties.EditProperties.ShowDescriptions = False
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 11
      ParentID = 9
      Index = 1
      Version = 1
    end
    object row_Effects1: TcxEditorRow
      Properties.Caption = #1069#1092#1092#1077#1082#1090#1099
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.Alignment.Horz = taLeftJustify
      Properties.EditProperties.MaxLength = 0
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      Visible = False
      ID = 12
      ParentID = 9
      Index = 2
      Version = 1
    end
    object row_DoubleInterval1: TcxEditorRow
      Expanded = False
      Properties.Caption = #1044#1074#1086#1081#1085#1086#1081' '#1080#1085#1090#1077#1088#1074#1072#1083
      Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
      Properties.EditProperties.Alignment = taLeftJustify
      Properties.EditProperties.NullStyle = nssUnchecked
      Properties.EditProperties.ReadOnly = False
      Properties.EditProperties.ValueChecked = 'True'
      Properties.EditProperties.ValueGrayed = ''
      Properties.EditProperties.ValueUnchecked = 'False'
      Properties.DataBinding.ValueType = 'Variant'
      Properties.Value = Null
      ID = 13
      ParentID = 12
      Index = 0
      Version = 1
    end
    object row_AllCaps1: TcxEditorRow
      Expanded = False
      Properties.Caption = #1042#1089#1077' '#1079#1072#1075#1083#1072#1074#1085#1099#1077
      Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
      Properties.EditProperties.Alignment = taLeftJustify
      Properties.EditProperties.NullStyle = nssUnchecked
      Properties.EditProperties.ReadOnly = False
      Properties.EditProperties.ValueChecked = 'True'
      Properties.EditProperties.ValueGrayed = ''
      Properties.EditProperties.ValueUnchecked = 'False'
      Properties.DataBinding.ValueType = 'Variant'
      Properties.Value = Null
      ID = 14
      ParentID = 12
      Index = 1
      Version = 1
    end
    object row_FontIsBold1: TcxEditorRow
      Expanded = False
      Properties.Caption = #1055#1086#1083#1091#1078#1080#1088#1085#1099#1081
      Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
      Properties.EditProperties.Alignment = taLeftJustify
      Properties.EditProperties.NullStyle = nssUnchecked
      Properties.EditProperties.ReadOnly = False
      Properties.EditProperties.ValueChecked = 'True'
      Properties.EditProperties.ValueGrayed = ''
      Properties.EditProperties.ValueUnchecked = 'False'
      Properties.DataBinding.ValueType = 'Variant'
      Properties.Value = Null
      ID = 15
      ParentID = 12
      Index = 2
      Version = 1
    end
    object row_FontIsItalic1: TcxEditorRow
      Expanded = False
      Properties.Caption = #1053#1072#1082#1083#1086#1085#1085#1099#1081
      Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
      Properties.EditProperties.Alignment = taLeftJustify
      Properties.EditProperties.NullStyle = nssUnchecked
      Properties.EditProperties.ReadOnly = False
      Properties.EditProperties.ValueChecked = 'True'
      Properties.EditProperties.ValueGrayed = ''
      Properties.EditProperties.ValueUnchecked = 'False'
      Properties.DataBinding.ValueType = 'Variant'
      Properties.Value = Null
      ID = 16
      ParentID = 12
      Index = 3
      Version = 1
    end
    object row_Shadows1: TcxEditorRow
      Expanded = False
      Properties.Caption = #1058#1077#1085#1080
      Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
      Properties.EditProperties.Alignment = taLeftJustify
      Properties.EditProperties.NullStyle = nssUnchecked
      Properties.EditProperties.ReadOnly = False
      Properties.EditProperties.ValueChecked = 'True'
      Properties.EditProperties.ValueGrayed = ''
      Properties.EditProperties.ValueUnchecked = 'False'
      Properties.DataBinding.ValueType = 'Variant'
      Properties.Value = Null
      ID = 17
      ParentID = 12
      Index = 4
      Version = 1
    end
    object cxVerticalGrid1EditorRow19: TcxEditorRow
      Expanded = False
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.Alignment.Horz = taLeftJustify
      Properties.EditProperties.MaxLength = 0
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 18
      ParentID = -1
      Index = 5
      Version = 1
    end
    object cxVerticalGrid1EditorRow20: TcxEditorRow
      Properties.Caption = #1055#1086#1083#1086#1078#1077#1085#1080#1077' '#1087#1086#1076#1087#1080#1089#1080
      Properties.EditPropertiesClassName = 'TcxTextEditProperties'
      Properties.EditProperties.Alignment.Horz = taLeftJustify
      Properties.EditProperties.MaxLength = 0
      Properties.EditProperties.ReadOnly = True
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 19
      ParentID = -1
      Index = 6
      Version = 1
    end
    object row_RotateWithLine: TcxEditorRow
      Expanded = False
      Properties.Caption = #1055#1086#1074#1086#1088#1072#1095#1080#1074#1072#1090#1100' '#1087#1086#1076#1087#1080#1089#1100' '#1074#1084#1077#1089#1090#1077' '#1089' '#1083#1080#1085#1080#1077#1081' ('#1076#1083#1103' '#1083#1080#1085#1077#1081#1085#1099#1093' '#1086#1073#1098#1077#1082#1090#1086#1074')'
      Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
      Properties.EditProperties.Alignment = taLeftJustify
      Properties.EditProperties.NullStyle = nssUnchecked
      Properties.EditProperties.ReadOnly = False
      Properties.EditProperties.UseAlignmentWhenInplace = True
      Properties.EditProperties.ValueChecked = 'True'
      Properties.EditProperties.ValueGrayed = ''
      Properties.EditProperties.ValueUnchecked = 'False'
      Properties.DataBinding.ValueType = 'Variant'
      Properties.Value = Null
      ID = 20
      ParentID = 19
      Index = 0
      Version = 1
    end
    object row_Position: TcxEditorRow
      Properties.Caption = #1056#1072#1089#1087#1086#1083#1086#1075#1072#1090#1100' '#1087#1086#1076#1087#1080#1089#1100
      Properties.EditPropertiesClassName = 'TcxComboBoxProperties'
      Properties.EditProperties.Alignment.Horz = taLeftJustify
      Properties.EditProperties.DropDownRows = 7
      Properties.EditProperties.Items.Strings = (
        #1055#1086' '#1094#1077#1085#1090#1088#1091
        #1057#1083#1077#1074#1072'-'#1089#1074#1077#1088#1093#1091
        #1057#1074#1077#1088#1093#1091
        #1057#1087#1088#1072#1074#1072'-'#1089#1074#1077#1088#1093#1091
        #1057#1083#1077#1074#1072
        #1057#1087#1088#1072#1074#1072
        #1057#1083#1077#1074#1072'-'#1057#1085#1080#1079#1091
        #1057#1085#1080#1079#1091
        #1057#1087#1088#1072#1074#1072'-'#1057#1085#1080#1079#1091)
      Properties.EditProperties.MaxLength = 0
      Properties.EditProperties.ReadOnly = False
      Properties.EditProperties.Revertable = True
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 21
      ParentID = 19
      Index = 1
      Version = 1
    end
    object row_Offset: TcxEditorRow
      Expanded = False
      Properties.Caption = #1085#1072' '#1091#1076#1072#1083#1077#1085#1080#1080' '#1086#1090' '#1094#1077#1085#1090#1088#1072' '#1086#1073#1098#1077#1082#1090#1072
      Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
      Properties.EditProperties.Alignment.Horz = taLeftJustify
      Properties.EditProperties.MaxValue = 30.000000000000000000
      Properties.EditProperties.MinValue = 2.000000000000000000
      Properties.EditProperties.ReadOnly = False
      Properties.DataBinding.ValueType = 'Float'
      Properties.Value = Null
      ID = 22
      ParentID = 21
      Index = 0
      Version = 1
    end
  end
  object pn_Buttons1111111: TPanel
    Left = 0
    Top = 413
    Width = 684
    Height = 40
    Align = alBottom
    BevelOuter = bvNone
    Constraints.MaxHeight = 40
    Constraints.MinHeight = 40
    TabOrder = 1
    DesignSize = (
      684
      40)
    object Bevel1: TBevel
      Left = 0
      Top = 0
      Width = 684
      Height = 2
      Align = alTop
      Shape = bsTopLine
      ExplicitWidth = 634
    end
    object Button3: TButton
      Left = 521
      Top = 10
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
      Default = True
      ModalResult = 1
      TabOrder = 0
    end
    object Button4: TButton
      Left = 602
      Top = 10
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Cancel = True
      Caption = #1047#1072#1082#1088#1099#1090#1100
      ModalResult = 2
      TabOrder = 1
    end
  end
  object ActionList_new: TActionList
    Left = 200
    Top = 324
    object act_Default: TAction
      Caption = #1047#1085#1072#1095#1077#1085#1080#1103' '#1087#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102
      OnExecute = act_DefaultExecute
    end
  end
  object FormStorage1: TFormStorage
    UseRegistry = True
    StoredValues = <>
    Left = 312
    Top = 320
  end
end
