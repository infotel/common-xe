unit d_Mapinfo_Data;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, Grids,
  cxVGrid, cxInplaceContainer, cxSplitter, ExtCtrls, dxBar, dxBarExtItems,
  DBGrids, dxmdaset, StdCtrls, ToolWin, ComCtrls, ActnList,
  cxTextEdit,


  MapXLib_TLB,

  u_MapX_lib,

  u_func,
  u_db,

  Placemnt, rxPlacemnt
  ;


type
  TDlgMapRec    = record
                 FieldName_Code:  string;
                 FieldName_Desrc: string;

                 Fieldname_abs_height :     string;
                 Fieldname_relative_height : string;
               end;


type
  Tdlg_Mapinfo_Data = class(TForm)
    cx_Data: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    mem_Data: TdxMemData;
    ds_Fields: TDataSource;
    mem_Fields: TdxMemData;
    ds_Data: TDataSource;
    ActionList1: TActionList;
    act_ColumnAutoWidth: TAction;
    Panel1: TPanel;
    cxSplitter1: TcxSplitter;
    cxGrid2: TcxGrid;
    cxFields: TcxGridDBTableView;
    cxGridDBColumn1: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    cxSplitter2: TcxSplitter;
    cxVerticalGrid1: TcxVerticalGrid;
    row_Code: TcxEditorRow;
    row_Descr: TcxEditorRow;
    row_Relative_H: TcxEditorRow;
    mem_Dataname: TStringField;
    mem_Fieldsname: TStringField;
    row_Abs_H: TcxEditorRow;
    cxVerticalGrid1CategoryRow1: TcxCategoryRow;
    cxVerticalGrid1CategoryRow2: TcxCategoryRow;
    dxBarManager1: TdxBarManager;
    btn_ColumnAutoWidth: TdxBarButton;
    FormStorage1: TFormStorage;
    dxBarSubItem1: TdxBarSubItem;
    dxBarButton1: TdxBarButton;
    StatusBar1: TStatusBar;
    Panel2: TPanel;
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure act_ColumnAutoWidthExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure row_Abs_HEditPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
  private
    FmiFile: TmiMap;
    FChanged: boolean;

    procedure LoadDBGridColumns;

  public
    destructor Destroy; override;
    procedure Load(aFileName: string);

    class function ExecDlg(aFileName: string; var aRec: TDlgMapRec): Boolean;

  end;


implementation

{$R *.dfm}

var
  dlg_Mapinfo_Data: Tdlg_Mapinfo_Data;


//--------------------------------------------------------------------
class function Tdlg_Mapinfo_Data.ExecDlg(aFileName: string; var aRec: TDlgMapRec): Boolean;
//--------------------------------------------------------------------
begin
  with Tdlg_Mapinfo_Data.Create(Application) do
  try

    row_Code.Properties.Value :=aRec.FieldName_Code;
    row_Descr.Properties.Value:=aRec.FieldName_Desrc;
    row_Abs_H.Properties.Value:=aRec.Fieldname_abs_height;
    row_Relative_H.Properties.Value:=aRec.Fieldname_relative_height;


    Load(aFileName);

    ShowModal;

    if FChanged then
    begin
      aRec.FieldName_Code           :=AsString(row_Code.Properties.Value);
      aRec.FieldName_Desrc          :=AsString(row_Descr.Properties.Value);
      aRec.Fieldname_abs_height     :=AsString(row_Abs_H.Properties.Value);
      aRec.Fieldname_relative_height:=AsString(row_Relative_H.Properties.Value);

      Result := True;
    end else
      Result := False;

  finally
    Free;
  end;

end;


procedure Tdlg_Mapinfo_Data.FormCreate(Sender: TObject);
begin
 // Caption:='Mapinfo_Data';

  FmiFile := TmiMap.Create;

  cxGrid1.Align:=alClient;
  cxGrid2.Align:=alClient;

end;


destructor Tdlg_Mapinfo_Data.Destroy;
begin
  FreeAndNil(FmiFile);
  inherited Destroy;
end;


procedure Tdlg_Mapinfo_Data.ActionList1Update(Action: TBasicAction; var
    Handled: Boolean);
begin
  StatusBar1.SimpleText:=IIF(act_ColumnAutoWidth.Checked, 'act_ColumnAutoWidth.Checked','');
end;



procedure Tdlg_Mapinfo_Data.Load(aFileName: string);
var
  I: Integer;

  vSelection: CMapXSelection;
  vFeature: CMapXFeature;
  iCount: Integer;
  vRowValues: CMapXRowValues;
  j: Integer;
begin

//  mapx_CreateMemFromMapFields (aFileName, mem_Data);

 // cx_Data.
  //RestoreDefaults;
  db_Clear(mem_Fields);


  if FmiFile.OpenFile (aFileName) then
  begin
    FmiFile.CreateMemFromMapFields (mem_Data);

    LoadDBGridColumns();

    for j:=0 to High(FmiFile.Fields) do
      db_AddRecord(mem_Fields, [db_Par(FLD_NAME, FmiFile.Fields[j].Name )]);


    FmiFile.vLayer.Selection.SelectAll (miSelectionNew);

    vSelection:=FmiFile.vLayer.Selection;
    iCount:=vSelection.Count;

    for i := 0 to iCount - 1 do
      if i<100 then

    begin
//      DoProgress2 (i, iCount);

      vFeature:=vSelection.Item[i+1];

      vRowValues:=FmiFile.VDataSet.RowValues[vFeature];

      mem_Data.Append;

      for j:=0 to High(FmiFile.Fields) do
        mem_Data[FmiFile.Fields[j].Name]:=vRowValues.Item[FmiFile.Fields[j].Name].Value;

      mem_Data.Post;

    end;

  end;


end;


//-------------------------------------------------------------------
procedure Tdlg_Mapinfo_Data.LoadDBGridColumns;
//-------------------------------------------------------------------
var
  I: integer;
  oCXGridDBColumn:TcxGridDBColumn;
begin
  cx_Data.BeginUpdate;

  for I := cx_Data.ColumnCount - 1 downto 0 do
    cx_Data.Columns[i].Free;

  for i:=0 to High(FmiFile.Fields) do
  begin
    oCXGridDBColumn:=cx_Data.CreateColumn;
    oCXGridDBColumn.DataBinding.FieldName := FmiFile.Fields[i].Name; //Result.FieldName;
    oCXGridDBColumn.PropertiesClass:=TcxTextEditProperties;
  end;

  cx_Data.EndUpdate;
end;



procedure Tdlg_Mapinfo_Data.row_Abs_HEditPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  FChanged:=True;
  //sdgvdfg ShowMessage('Validate');
end;


procedure Tdlg_Mapinfo_Data.act_ColumnAutoWidthExecute(Sender: TObject);
begin
  if Sender=act_ColumnAutoWidth then
    cx_Data.OptionsView.ColumnAutoWidth:=act_ColumnAutoWidth.Checked;
end;



end.


