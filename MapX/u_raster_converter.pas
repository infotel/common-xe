unit u_raster_converter;

interface
uses
  SysUtils,

  u_mapinfo_TAB_file_new,
  u_ozi_explorer;


procedure OZI_to_TAB(aMapFileName, aTabFileName: String);


implementation

procedure OZI_to_TAB(aMapFileName, aTabFileName: String);
var
  I: Integer;
  oOZI: TOziExplorerFile;
  oTab: TMapinfoRasterTabFile;
begin
  oOZI:=TOziExplorerFile.Create;
  oTab:=TMapinfoRasterTabFile.Create;

  oOZI.LoadFromiFile(aMapFileName);


  oTab.RasterFileName := oOZI.RasterFileName;

  oTab.GK_Zone := oOZI.GK_Zone;
  oTab.PointCount := oOZI.PointCount;
  oTab.Units := utDegree;

  for I := 0 to oOZI.PointCount - 1 do
  begin
    oTab.Points[i].x := oOZI.Points[i].x;
    oTab.Points[i].y := oOZI.Points[i].y;

    oTab.Points[i].b := oOZI.Points[i].b;
    oTab.Points[i].l := oOZI.Points[i].l;

    oTab.Points[i].img_x := oOZI.Points[i].img_x;
    oTab.Points[i].img_y := oOZI.Points[i].img_y;

  end;


  oTab.SaveToFile(aTabFileName);

  FreeAndNil(oOZI);
  FreeAndNil(oTab);


end;



begin
  //OZI_to_TAB('S:\_1\2.map', 'S:\_1\2_.tab');



end.
