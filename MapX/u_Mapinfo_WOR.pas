unit u_Mapinfo_WOR;

interface
uses
  SysUtils, classes, Graphics, Variants,

  MapXLib_TLB,

  u_mapX,

  u_files,
  u_func
  ;

  procedure mapx_SaveToWOR(aMap: TMap; aWorFileName: string);


implementation


  //--------------------------------------------------------
  procedure ColorTo_R_G_B(Value: TColor; var R,G,B: byte);
  //--------------------------------------------------------
  var cl: packed record case b:byte of
            0: (int: TColor);   1: (RGB: array[0..4] of byte);
          end;
  begin
    cl.int:=Value;
    R:=cl.rgb[0];  G:=cl.rgb[1];  B:=cl.rgb[2];
  end;


  function MakeMapinfoColor (aColor: integer): integer;
  var r,g,b: byte;
  begin
    ColorTo_R_G_B (aColor, r,g,b);
    Result:=(r*65536) + (g * 256) + b;
  end;



//-------------------------------------------------------------------
procedure mapx_SaveToWOR(aMap: TMap; aWorFileName: string);
//-------------------------------------------------------------------

     //-------------------------------------------------------------------
     function GetMapinfoShortFileName(aFileName: string): string;
     //-------------------------------------------------------------------
     var
       s: string;
     begin
      aFileName:=ExtractFileNameNoExt(aFileName) ;
      aFileName:=ReplaceStr(aFileName,' ','_');
      aFileName:=ReplaceStr(aFileName,'.','_');
      aFileName:=ReplaceStr(aFileName,'-','_');
      aFileName:=ReplaceStr(aFileName,',','_');

      s:=Copy(aFileName, 1,1);
      if Copy(aFileName, 1,1)[1] in ['0'..'9'] then
        aFileName:='_'+ aFileName;

      Result := aFileName;
    end;


    function MiPositionToStr(aValue: Integer): string;
    begin
      case aValue of
        miPositionCC : Result := 'Center';
        miPositionTL : Result := 'Above RIGHT';
        miPositionTC : Result := 'Above Left';
        miPositionTR : Result := 'Above Left';
        miPositionCL : Result := 'LEFT';
        miPositionCR : Result := 'RIGHT';
        miPositionBL : Result := 'Below Left';
        miPositionBC : Result := 'Below';
        miPositionBR : Result := 'Below RIGHT';
      end;

    end;


    function StrToMiPosition(aValue: string): Integer;
    begin
     (* case aValue of
        miPositionCC : Result := 'Center';
        miPositionTL : Result := 'Above RIGHT';
        miPositionTC : Result := 'Above Left';
        miPositionTR : Result := 'Above Left';
        miPositionCL : Result := 'LEFT';
        miPositionCR : Result := 'RIGHT';
        miPositionBL : Result := 'Below Left';
        miPositionBC : Result := 'Below';
        miPositionBR : Result := 'Below RIGHT';
      end;
*)
    end;



const
  CRLF = #13+#10;

var
  v: Variant;
  vField : CMapXField;

  sKeyField: string;
  st: string;
  bAuto: Boolean;
  sPos: string;
  iColorBack: integer;
  iSize: integer;
  sFont: string;
  sName: string;
  sFilename: string;
  i: integer;
  S,sNames: string;
 // v: Variant;

  vLayer: CMapxLayer;


  oSList: TStringList;
  s1: string;
begin
  oSList:=TStringList.Create;


(*  oSList.Add('!Workspace');
  oSList.Add('!Version 600');
  oSList.Add('!Charset WindowsCyrillic');

*)
  s1:= '!Workspace' + CRLF +
      '!Version 600' + CRLF +
      '!Charset WindowsCyrillic';// + CRLF;


  oSList.Add(s1);


  sNames := '';

  for i := 0 to aMap.Layers.Count-1 do
  begin
    //u_mapX_tools
    vLayer:=mapx_GetLayerByIndex_from_0(aMap,i);

//    sFilename:=aMap.Layers.Item[i].FileSpec;
    sFilename:=vLayer.FileSpec;

  //  st:=aMap.Layers.Item[i].KeyField;


    sName:=GetMapinfoShortFileName(sFilename) ;
  //  map1.Layers.Item[i].Name;

    s1:=Format('Open Table "%s" As %s Interactive', [sFilename,sName]);
    oSList.Add(s1);

    s:=s+ Format('Open Table "%s" As %s Interactive', [sFilename,sName]) + CRLF;

    sNames:=sNames+ sName + IIF(i<aMap.Layers.Count-1,',','');

  end;

  //Map From pmp_site,property,aqua,pmp_sector_ant
  s:=s+ Format('Map From %s', [sNames]) + CRLF ;

  s1:=Format('Map From %s', [sNames]);
  oSList.Add(s1);


  s:=s+ 'Set Map ' + CRLF;
  oSList.Add('Set Map ');

  oSList.Add('  CoordSys Earth Projection 1, 1001 ');

  s1:=Format('  Center (%1.8f,%1.8f)  ',  [aMap.CenterX, aMap.CenterY]);
  oSList.Add(s1);

  s1:=Format('  Zoom %1.6f Units "km" ', [aMap.Zoom]);
  oSList.Add(s1);

     // s+
  s1:= '  CoordSys Earth Projection 1, 1001 '+ CRLF +
       Format('  Center (%1.8f,%1.8f)  ',  [aMap.CenterX, aMap.CenterY]) + CRLF +
       Format('  Zoom %1.6f Units "km" ', [aMap.Zoom]); // + CRLF ;

  oSList.Add(s1);

  s:=s+ 'Set Map '+ CRLF;

  oSList.Add('Set Map ');

  for i := 0 to aMap.Layers.Count-1   do
  begin

//    vLayer:=aMap.Layers.Item[i];
    vLayer:=mapx_GetLayerByIndex_from_0(aMap,i);

  //  vLayer.LabelProperties.Visible

    s:=s+ Format('  Layer %d      ', [i+1])+ CRLF;

    s1:=Format('  Layer %d      ', [i+1]);
    oSList.Add(s1);


    case vLayer.Type_ of

        miLayerTypeRaster:
        begin
          s1:= '  Display Graphic  ' + CRLF +
               '  Selectable Off   ' + CRLF ;
          s:=s+ s1;

          oSList.Add(s1);
        end;


        miLayerTypeNormal:
        begin
           // v := vLayer.LabelProperties.Visible;
           // sName:=AsString(v);
            sName     :=OleVariant(vLayer.LabelProperties.Style.TextFont).Name;
            iSize     :=OleVariant(vLayer.LabelProperties.Style.TextFont).Size;
            iColorBack:=vLayer.LabelProperties.Style.TextFontBackColor;
            sPos      :=MiPositionToStr(vLayer.LabelProperties.Position);

        //    v:=vLayer.LabelProperties.DataField;

            if Pos('cyr',sName)=0 then
              sName:=sName + ' cyr';


            sFont:= Format('("%s",0,%d,0,%d)',
                      [sName,
                       iSize,
                       MakeMapinfoColor(iColorBack)
                      ]);


           if (vLayer.Datasets.Count = 0) then
             aMap.Datasets.Add (miDataSetLayer, vLayer, EmptyParam, EmptyParam,
                                EmptyParam, EmptyParam, EmptyParam, EmptyParam) ;


            vField:=vLayer.LabelProperties.DataField as CMapXField;
            if Assigned(vField) then
              sKeyField:=vField.Name;

            bAuto:=vLayer.AutoLabel;
            sKeyField:=vLayer.KeyField;



            s1:= Format('    Label Line Arrow Position %s Font %s Pen (1,2,0)', [sPos,sFont])+ CRLF +
       IIF(sKeyField<>'','    With '+ sKeyField+ CRLF,'') +
//                  Format('    With %s       ', [sName])+ CRLF +
                  Format('    Parallel On Auto %s ', [IIF(bAuto,'On','Off') ])+
                             'Overlap Off Duplicates On Offset 2'+ CRLF +
                         '    Visibility On '; // + CRLF;



            s:=s+ s1;
            oSList.Add(s1);


{            Format('    Label Line Arrow Position %s Font %s Pen (1,2,0)', [sPos,sFont])+ CRLF +
       IIF(sKeyField<>'','    With '+ sKeyField+ CRLF,'') +
//                  Format('    With %s       ', [sName])+ CRLF +
                  Format('    Parallel On Auto %s ', [IIF(bAuto,'On','Off') ])+
                             'Overlap Off Duplicates On Offset 2'+ CRLF +
                         '    Visibility On ' + CRLF;
}

        end;
    //  end;
    end;
  end;

//  StrToTxtFile (s, aWorFileName);

  ForceDirByFileName(aWorFileName);

  oSList.SaveToFile (aWorFileName);
  FreeAndNil(oSList);

end;


end.


