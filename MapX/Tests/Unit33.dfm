object Form33: TForm33
  Left = 309
  Top = 320
  Caption = 'Form33'
  ClientHeight = 561
  ClientWidth = 1418
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Map1: TMap
    Left = 0
    Top = 0
    Width = 568
    Height = 561
    ParentColor = False
    Align = alLeft
    TabOrder = 0
    OnMouseUp = Map1MouseUp
    OnMouseMove = Map1MouseMove
    OnToolUsed = Map1ToolUsed
    OnMapViewChanged = Map1MapViewChanged
    ExplicitHeight = 333
    ControlData = {
      2CA10700B43A0000FB390000010000000F0000FFFFFEFF0D470065006F004400
      69006300740069006F006E0061007200790000FFFEFF3745006D007000740079
      002000470065006F007300650074004E0061006D00650020007B003900410039
      00410043003200460034002D0038003300370035002D0034003400640031002D
      0042004300450042002D00340037003600410045003900380036004600310039
      0030007D00FFFEFF001E00FFFEFF000600000A000000000000FFFEFF00500001
      0100000001F4010000050000800C0000000014000000000002000E00E8030005
      00000100000000D4D0C8000000000000000001370000000000D4D0C800000000
      0000000352E30B918FCE119DE300AA004BB85101CC00009001DC7C0100054172
      69616C000352E30B918FCE119DE300AA004BB851010200009001A42C02000B4D
      61702053796D626F6C730000000000000001000100FFFFFF000200D4D0C80000
      0000000001000000010000FFFEFF3245006D0070007400790020005400690074
      006C00650020007B00300031004100390035003000340042002D004300450031
      0033002D0034003400310035002D0041003500410030002D0035003100440038
      00430032004600310035003200300034007D0000000000FFFFFF000100000000
      000000000000000000000000000000000000000352E30B918FCE119DE300AA00
      4BB85101CC00009001348C030005417269616C000352E30B918FCE119DE300AA
      004BB85101CC00009001348C030005417269616C000000000000000000000000
      00000000000000000000000000000000000000000000000000DCA54000000000
      00007C40010000010000180100000100000090E322001C000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000020000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000008076C000000000008056C0000000000080764000000000008056400000
      000001000000180100000100000090E322001C00000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000200
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000080
      76C000000000008056C000000000008076400000000000805640000000000100
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000}
  end
  object Button1: TButton
    Left = 744
    Top = 24
    Width = 75
    Height = 25
    Caption = 'Add dlg'
    TabOrder = 1
    OnClick = Button1Click
  end
  object FilenameEdit1: TFilenameEdit
    Left = 744
    Top = 72
    Width = 521
    Height = 21
    NumGlyphs = 1
    TabOrder = 2
    Text = 'D:\wms1111\~property.tab'
  end
  object Button2: TButton
    Left = 744
    Top = 161
    Width = 75
    Height = 25
    Caption = 'Add'
    TabOrder = 3
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 744
    Top = 224
    Width = 156
    Height = 25
    Caption = 'Property Page'
    TabOrder = 4
    OnClick = Button3Click
  end
  object ListBox2: TListBox
    Left = 744
    Top = 275
    Width = 393
    Height = 108
    ItemHeight = 13
    TabOrder = 5
  end
  object Button4: TButton
    Left = 752
    Top = 496
    Width = 75
    Height = 25
    Caption = 'Button4'
    TabOrder = 6
    OnClick = Button4Click
  end
  object Button5: TButton
    Left = 880
    Top = 496
    Width = 75
    Height = 25
    Caption = 'OSM'
    TabOrder = 7
    OnClick = Button5Click
  end
  object FilenameEdit2: TFilenameEdit
    Left = 744
    Top = 97
    Width = 521
    Height = 21
    NumGlyphs = 1
    TabOrder = 8
    Text = 'D:\33333333333\0\Untitled.TAB'
  end
  object FilenameEdit3: TFilenameEdit
    Left = 744
    Top = 134
    Width = 521
    Height = 21
    NumGlyphs = 1
    TabOrder = 9
    Text = 'C:\GIS\piter_and_obl_MIF\urban.tab'
  end
  object Button6: TButton
    Left = 1072
    Top = 496
    Width = 75
    Height = 25
    Caption = 'Set Center'
    TabOrder = 10
    OnClick = Button6Click
  end
  object ComboBox1: TComboBox
    Left = 752
    Top = 527
    Width = 209
    Height = 21
    Style = csDropDownList
    TabOrder = 11
  end
  object Button7: TButton
    Left = 892
    Top = 161
    Width = 129
    Height = 25
    Caption = 'Button7'
    TabOrder = 12
    OnClick = Button7Click
  end
  object Button8: TButton
    Left = 1232
    Top = 344
    Width = 75
    Height = 25
    Caption = 'Button8'
    TabOrder = 13
    OnClick = Button8Click
  end
  object ed_Z: TEdit
    Left = 1212
    Top = 492
    Width = 121
    Height = 21
    TabOrder = 14
    Text = '10'
  end
  object Button9: TButton
    Left = 1212
    Top = 520
    Width = 75
    Height = 24
    Caption = 'Button9'
    TabOrder = 15
    OnClick = Button9Click
  end
  object Button10: TButton
    Left = 1276
    Top = 78
    Width = 74
    Height = 19
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Caption = 'Add'
    TabOrder = 16
    OnClick = Button10Click
  end
  object Button11: TButton
    Left = 1037
    Top = 7
    Width = 58
    Height = 19
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Caption = 'Exec'
    TabOrder = 17
    OnClick = Button11Click
  end
  object Button12: TButton
    Left = 1090
    Top = 6
    Width = 57
    Height = 19
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Caption = 'NN'
    TabOrder = 18
    OnClick = Button12Click
  end
  object Button13: TButton
    Left = 1276
    Top = 115
    Width = 57
    Height = 19
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Caption = 'Button13'
    TabOrder = 19
    OnClick = Button13Click
  end
  object ComboBox_WMS: TComboBox
    Left = 1072
    Top = 208
    Width = 123
    Height = 21
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    TabOrder = 20
    Text = 'YandexMap'
    Items.Strings = (
      'GoogleHybr'
      'YandexSat'
      'YandexMap')
  end
  object Button14: TButton
    Left = 1207
    Top = 208
    Width = 58
    Height = 19
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Caption = 'Button14'
    TabOrder = 21
    OnClick = Button14Click
  end
  object Button15: TButton
    Left = 1152
    Top = 6
    Width = 57
    Height = 19
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Caption = 'Vladiv'
    TabOrder = 22
    OnClick = Button15Click
  end
  object Button16: TButton
    Left = 1214
    Top = 6
    Width = 57
    Height = 19
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Caption = 'Murm'
    TabOrder = 23
    OnClick = Button16Click
  end
  object Button17: TButton
    Left = 997
    Top = 37
    Width = 58
    Height = 19
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Caption = #1057#1072#1085#1082#1090'-'#1055#1077#1090#1077#1088#1100#1091#1088#1075
    TabOrder = 24
    OnClick = Button17Click
  end
  object Button18: TButton
    Left = 1059
    Top = 37
    Width = 57
    Height = 19
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Caption = #1045#1082#1072#1090#1077#1088#1080#1085#1073#1091#1088#1075
    TabOrder = 25
    OnClick = Button18Click
  end
  object Button19: TButton
    Left = 1121
    Top = 37
    Width = 57
    Height = 19
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Caption = 'odessa'
    TabOrder = 26
    OnClick = Button19Click
  end
  object Button20: TButton
    Left = 1183
    Top = 37
    Width = 57
    Height = 19
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Caption = 'Button17'
    TabOrder = 27
    OnClick = Button20Click
  end
  object Button21: TButton
    Left = 1217
    Top = 269
    Width = 58
    Height = 19
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Caption = 'Button21'
    TabOrder = 28
    OnClick = Button21Click
  end
  object Button22: TButton
    Left = 1296
    Top = 224
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'log'
    TabOrder = 29
    OnClick = Button22Click
  end
  object Timer1: TTimer
    Interval = 10
    OnTimer = Timer1Timer
    Left = 1312
    Top = 16
  end
end
